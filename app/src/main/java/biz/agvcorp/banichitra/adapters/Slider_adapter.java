package biz.agvcorp.banichitra.adapters;

import android.view.View;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.HashMap;

public class Slider_adapter {

    private View mView;
    private BaseSliderView.OnSliderClickListener listener;
    private ViewPagerEx.OnPageChangeListener pageChangeListener;
    private SliderLayout mSliderLayout;

    private HashMap<String,String> url_maps = new HashMap<String, String>();
//    private HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
    private HashMap<String,String> file_maps = new HashMap<String, String>();

    public Slider_adapter(){
        mapClear(url_maps);
        mapClear(file_maps);

        load_slide_urls();
//        load_slide_image();
    }

    private void load_slide_urls(){
        this.url_maps.put("Messi", "https://ichef.bbci.co.uk/images/ic/976xn/p06c5dp9.jpg");
        this.url_maps.put("Messi", "https://ichef.bbci.co.uk/images/ic/976xn/p06c5dp9.jpg");

        load_slide_image();
    }

    private void load_slide_image(){
//        this.file_maps.put("Avergers: Infinity War",R.drawable.slide1);
//        this.file_maps.put("Rampage",R.drawable.slide2);
//        this.file_maps.put("Avatar",R.drawable.slide3);
        this.file_maps.put("Messi","https://ichef.bbci.co.uk/images/ic/976xn/p06c5dp9.jpg");
        this.file_maps.put("Messi","https://ichef.bbci.co.uk/images/ic/976xn/p06c5dp9.jpg");
    }

    private void mapClear(HashMap map){
        map.clear();
    }

    public HashMap<String,String> get_urlMap(){
        return this.url_maps;
    }

    public HashMap<String,String> get_file_map(){
        return this.file_maps;
    }

    public void set_url_map(HashMap<String,String> map){
        this.url_maps = map;
    }

    public void set_file_map(HashMap<String,String> map){
        this.file_maps = map;
    }
}
