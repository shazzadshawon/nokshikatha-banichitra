package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class History_videoDuration_adapter {

    private ArrayList<String> thumbs = new ArrayList<>();

    public History_videoDuration_adapter(){

    }

    public History_videoDuration_adapter(String parent){
        switch (parent){
            case "allupload":
                list_maker();
                break;
            case "recentupload":
                list_maker();
                break;
            case "favourite":
                list_maker();
                break;
            case "topview":
                list_maker();
                break;
            default:
                list_maker();
                break;
        }
    }

    private void list_maker(){
        this.thumbs.add("10:20");
        this.thumbs.add("20:30");
        this.thumbs.add("30:10");
        this.thumbs.add("5:20");
        this.thumbs.add("1:10:30");
        this.thumbs.add("1:20:05");
        this.thumbs.add("55:59");
        this.thumbs.add("15:30");
    }

    private void list_randomizer(){
        Collections.shuffle(this.thumbs);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.thumbs;
    }
}
