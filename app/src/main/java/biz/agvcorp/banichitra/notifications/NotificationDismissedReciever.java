package biz.agvcorp.banichitra.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import biz.agvcorp.banichitra.R;
import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created By - Shazzadur Rahaman on 9/10/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
public class NotificationDismissedReciever extends BroadcastReceiver {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();

        int notificationId = intent.getExtras().getInt("com.my.app.notificationId");
        /* Your code to handle the event here */

        int pre = sharedPreferences.getInt(context.getString(R.string.iconBadgeCount), 0);
        int after =  pre-1;
        editor.putInt(context.getString(R.string.iconBadgeCount), after);
        editor.commit();
        ShortcutBadger.applyCount(context, sharedPreferences.getInt(context.getString(R.string.iconBadgeCount), 0));
    }
}
