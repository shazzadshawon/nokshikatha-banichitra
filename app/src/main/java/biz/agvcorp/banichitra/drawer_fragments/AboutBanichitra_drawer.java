package biz.agvcorp.banichitra.drawer_fragments;



import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.recycler_adapters.Recycler_adapter_fb_app_friend;
import de.hdodenhof.circleimageview.CircleImageView;

public class AboutBanichitra_drawer extends Fragment implements ViewPagerEx.OnPageChangeListener {

    private static final String TAG = "AboutBanichitra_drawer";

    private View mView;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private CircleImageView facebook, twitter;

    private OnFragmentInteractionListener mListener;

    private String mParam1;
    private String mParam2;

    private Toolbar toolbar;

    private AccessToken fb_accessToken;
    private Boolean fb_login_sts = false;
    private RelativeLayout mFacebookContainer, mFacebook_login;
    private TextView mFacebook_text_heading;
    private TextView mFacebook_text;

    private ArrayList<String> mThumbnail = new ArrayList<>();
    private RecyclerView recyclerView;
    private Recycler_adapter_fb_app_friend recycler_adapter_fb_app_friend;
    private GridLayoutManager layoutManager_grid;

    private LoginButton mFacebook_login_btn;
    private CallbackManager mFacebook_callback_manager;
    private AccessTokenTracker accessTokenTracker;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        try{
            mListener = (OnFragmentInteractionListener) getActivity();
        } catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFacebook_callback_manager = CallbackManager.Factory.create();
        layoutManager_grid = new GridLayoutManager(getContext(), 6);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.drawer_about_banichitra, container, false);

        mFacebookContainer = mView.findViewById(R.id.facebook_items);
        mFacebook_login = mView.findViewById(R.id.facebook_logoff);
        mFacebook_text_heading = mView.findViewById(R.id.fb_upText);
        mFacebook_text = mView.findViewById(R.id.facebook_text);

        mFacebook_login_btn = mView.findViewById(R.id.facebook_login_button);
        mFacebook_login_btn.registerCallback(mFacebook_callback_manager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (getFacebookLogin()){
                    getFacebookFriends();
                } else {
                    hide_fb_item_container();
                    show_fb_login_btn();
                }
            }

            @Override
            public void onCancel() {
                hide_fb_item_container();
                show_fb_login_btn();
            }

            @Override
            public void onError(FacebookException error) {
                hide_fb_item_container();
                show_fb_login_btn();
            }
        });

        fb_accessToken = AccessToken.getCurrentAccessToken();
        if (getFacebookLogin()){
            getFacebookFriends();
        } else {
            hide_fb_item_container();
            show_fb_login_btn();
        }

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar = view.findViewById(R.id.about_toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);

        facebook = view.findViewById(R.id.click_facebook);
        twitter = view.findViewById(R.id.click_twitter);

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFacebook(getContext(), "banichitra");
                //goToBrowser("https://m.facebook.com/banichitra");
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToBrowser("https://m.facebook.com/agvbd");
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFacebook_callback_manager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (getFacebookLogin()){
                    getFacebookFriends();
                } else {
                    hide_fb_item_container();
                    show_fb_login_btn();
                }
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (accessTokenTracker != null){
            accessTokenTracker.stopTracking();
        }
    }

















    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public AboutBanichitra_drawer() {
        // Required empty public constructor
    }

    private void gather_data_recycle() {
        clearList(this.mThumbnail);
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/39177581_1967928523270172_8514993155056074752_n.jpg?_nc_cat=0&oh=e5f4802bd94ac0cd96901cf3a72e0df2&oe=5C254DB9");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/36633853_1910325785698585_814057282261221376_n.jpg?_nc_cat=0&oh=a180f09e0480df43b4d7f5d759440524&oe=5C2EEFD8");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/28575611_10214585470846026_8570217038883359703_n.jpg?_nc_cat=0&oh=f88fcd35e2e686ec176c5cc909baed06&oe=5BECFED6");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/32365686_10216386850772551_4738513550819983360_n.jpg?_nc_cat=0&oh=4267db3f49bcc8b7640ca718f180abe9&oe=5C216D37");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/37715743_10216193651611950_7431954371832709120_n.jpg?_nc_cat=0&oh=db3bc2bb69c2f568a4bf109f228a6f1b&oe=5BF15C77");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/30704312_10210203968049557_5556752400355688448_n.jpg?_nc_cat=0&oh=9b8faefd8fab9bff5157fcd03723ce3d&oe=5C31EF52");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/36063325_10215337263078672_943515903029411840_n.jpg?_nc_cat=0&oh=ee5099685a04c22343aa5ad9d9f52255&oe=5C27C4C9");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/1545830_10152063238883285_890295710_n.jpg?_nc_cat=0&oh=00c75dc54fbbe1e4b35959db7f021b94&oe=5C329A16");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/33085405_2064664877143719_6938979756469649408_n.jpg?_nc_cat=0&oh=3d2906a5bf897c2990d3b9f6b9f82f0a&oe=5BF1A514");
        this.mThumbnail.add("https://scontent.fdac24-1.fna.fbcdn.net/v/t1.0-9/20245618_214134115779489_3921381716787095999_n.jpg?_nc_cat=0&oh=a945e82a5e8839387006d131d506fb5d&oe=5C3AAA5B");

        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView = mView.findViewById(R.id.fb_app_user_list);
        if (recyclerView.getVisibility() == View.GONE){
            recyclerView.setVisibility(View.VISIBLE);
        }

        recycler_adapter_fb_app_friend = new Recycler_adapter_fb_app_friend(mThumbnail,getContext());
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setAdapter(recycler_adapter_fb_app_friend);
        recyclerView.setLayoutManager(layoutManager_grid);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
    }

    private void clearList(ArrayList list) {
        list.clear();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public static AboutBanichitra_drawer newInstance(String param1, String param2) {
        AboutBanichitra_drawer fragment = new AboutBanichitra_drawer();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void goToBrowser(String uri){
        if (uri != null){
            Uri ad_uri = Uri.parse(uri);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, ad_uri);

            PackageManager packageManager = getContext().getPackageManager();
            List<ResolveInfo> activities = packageManager.queryIntentActivities(browserIntent, PackageManager.MATCH_DEFAULT_ONLY);
            boolean isIntentSafe = activities.size() > 0;

            if (isIntentSafe){
                String intentTitle = getString(R.string.browserChooserText);
                Intent chooser = Intent.createChooser(browserIntent, intentTitle);
                startActivity(chooser);
            } else {
                //THERE IS NOT ANY APP INSTALLED IN YOUR PHONE TO OPEN THIS LINK
                Toast.makeText(getContext(), getString(R.string.noBrowserText), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void goToFacebook(Context context, String pageName){
        if (context != null && pageName != null && !pageName.equals("")){
            Intent facebookIntent;
            String channel_id = getString(R.string.banichitraYoutubeChannelId);
            try {
                context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
                facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/"+channel_id));
            } catch (PackageManager.NameNotFoundException e) {
                facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/"+pageName));
            }

            Intent chooserIntent = Intent.createChooser(facebookIntent, "Open with");
            startActivity(chooserIntent);
        } else {
            Toast.makeText(getContext(), "Error Occured! Try again after some time", Toast.LENGTH_SHORT).show();
        }
    }

    private void getFacebookFriends(){
        GraphRequest request = GraphRequest.newMeRequest(
                fb_accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (response != null){
//                            mFacebook_text.setText(response.toString());
                            mFacebook_text.setText("Usage Application is being Reviewed by Facebook.");
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link");
        request.setParameters(parameters);
        request.executeAsync();

        hide_fb_login_btn();
        gather_data_recycle();
        show_fb_item_container();
    }

    private Boolean getFacebookLogin(){
        fb_accessToken = AccessToken.getCurrentAccessToken();
        if (fb_accessToken != null && !fb_accessToken.isExpired()){
            fb_login_sts = true;
            return true;
        } else {
            fb_login_sts = false;
            return false;
        }
    }

    private void show_fb_item_container(){
        mFacebookContainer.setVisibility(View.VISIBLE);
        mFacebook_text_heading.setVisibility(View.VISIBLE);
        mFacebook_text.setVisibility(View.VISIBLE);
    }

    private void hide_fb_item_container(){
        mFacebookContainer.setVisibility(View.GONE);
        mFacebook_text_heading.setVisibility(View.GONE);
        mFacebook_text.setVisibility(View.GONE);
    }

    private void hide_fb_login_btn(){
        if (mFacebook_login.getVisibility() == View.VISIBLE){
            mFacebook_login.setVisibility(View.GONE);
        }
    }

    private void show_fb_login_btn(){
        if (mFacebook_login.getVisibility() == View.GONE){
            mFacebook_login.setVisibility(View.VISIBLE);
        }
    }
}
