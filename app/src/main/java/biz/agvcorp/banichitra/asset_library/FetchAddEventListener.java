package biz.agvcorp.banichitra.asset_library;

public interface FetchAddEventListener<T> {
    public void onFetchingAddSuccess(T object);
    public void onFetchingAddFailure(Exception e);
    public void onFetchingStart();
    public void onNoAd();
}
