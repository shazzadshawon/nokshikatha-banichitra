package biz.agvcorp.banichitra.recycler_adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.elyeproj.loaderviewlibrary.LoaderImageView;
import com.elyeproj.loaderviewlibrary.LoaderTextView;

import java.util.ArrayList;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.Video_play;
import biz.agvcorp.banichitra.notifications.NewsDetails;

public class Recycler_adapter_news extends RecyclerView.Adapter<Recycler_adapter_news.ViewHolder> {

    private static final String TAG = "Recycler_adapter_news";

    private ArrayList<String> msgId = new ArrayList<>();
    private ArrayList<String> msgTitle = new ArrayList<>();
    private ArrayList<String> msgBody = new ArrayList<>();
    private ArrayList<String> msgImageUrl = new ArrayList<>();
    private ArrayList<String> msgPriority = new ArrayList<>();
    private ArrayList<String> msgVideoId = new ArrayList<>();
    private Context mContext;

    public Recycler_adapter_news(ArrayList<String> msgId, ArrayList<String> msgTitle, ArrayList<String> msgBody, ArrayList<String> msgImageUrl, ArrayList<String> msgPriority, ArrayList<String> msgVideoId, Context mContext) {
        this.msgId = msgId;
        this.msgTitle = msgTitle;
        this.msgBody = msgBody;
        this.msgImageUrl = msgImageUrl;
        this.msgPriority = msgPriority;
        this.msgVideoId = msgVideoId;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_staggered_news, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    int lastPosition = -1;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final int holder_position = position;
        if (!msgImageUrl.get(position).equals("")){
            Glide.with(mContext)
                    .asBitmap()
                    .load(msgImageUrl.get(position))
                    .into(holder.image);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    news_rerouting(holder_position);
                }
            });
        }
        if (msgTitle.get(position).equals("")){
            holder.news.setText("No Title");
        } else {
            holder.news.setText(msgTitle.get(position));
        }

        holder.recycle_item_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                news_rerouting(holder_position);
            }
        });

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    @Override
    public int getItemCount() {
        return msgId.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LoaderImageView image;
        LoaderTextView news;
        RelativeLayout recycle_item_container;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.newsImageView);
            news = itemView.findViewById(R.id.news_section);
            recycle_item_container = itemView.findViewById(R.id.recycle_item_container);
        }
    }

    private void news_rerouting(int position){
        String news_video_id = msgVideoId.get(position);
        if (news_video_id.equals("")){
            go_to_news_page(position);
        } else {
            go_to_video_play_page(position, news_video_id);
        }
    }

    private void go_to_news_page(int position){
        Intent newsDetailsIntent = new Intent(mContext, NewsDetails.class);

        String news_id = msgId.get(position);
        String news_image_url = msgImageUrl.get(position);
        String news_heading = msgTitle.get(position);
        String news_body = msgBody.get(position);

        newsDetailsIntent.putExtra(mContext.getString(R.string.newsidTag), news_id);
        newsDetailsIntent.putExtra(mContext.getString(R.string.newsheadingTag), news_heading);
        newsDetailsIntent.putExtra(mContext.getString(R.string.newsDetailstag), news_body);
        newsDetailsIntent.putExtra(mContext.getString(R.string.intentFlagTag), false);

        if (news_image_url.equals("")){
            newsDetailsIntent.putExtra(mContext.getString(R.string.newsimagetag), "noimage");
        } else {
            newsDetailsIntent.putExtra(mContext.getString(R.string.newsimagetag), news_image_url);
        }

        mContext.startActivity(newsDetailsIntent);
    }

    private void go_to_video_play_page(int position, String vid){
        Intent videoPlayIntent = new Intent(mContext, Video_play.class);

        String news_id = msgId.get(position);
        String news_heading = msgTitle.get(position);

        videoPlayIntent.putExtra(mContext.getString(R.string.newsidTag), news_id);
        videoPlayIntent.putExtra(mContext.getString(R.string.passvid), vid);
        videoPlayIntent.putExtra(mContext.getString(R.string.passTitle), news_heading);
        videoPlayIntent.putExtra(mContext.getString(R.string.videoplayIntentSourceFlag), false);

        mContext.startActivity(videoPlayIntent);
    }

    private void clearAll(){
        this.msgId.clear();
        this.msgTitle.clear();
        this.msgBody.clear();
        this.msgImageUrl.clear();
        this.msgPriority.clear();
    }

}
