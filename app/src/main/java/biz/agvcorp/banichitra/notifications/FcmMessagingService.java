package biz.agvcorp.banichitra.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import biz.agvcorp.banichitra.MainActivity;
import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.Video_play;
import biz.agvcorp.banichitra.database_classes.AppDatabase;
import biz.agvcorp.banichitra.database_classes.App_notification;
import me.leolin.shortcutbadger.ShortcutBadger;

public class FcmMessagingService extends FirebaseMessagingService{
    private static final String TAG = "FcmMessagingService";
    private NotificationManager notificationManager;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private Boolean activeStatus, vibrationStatus;
    private String priorityStatus;

    private String msgId, msgTitle, msgBody, imageUrl, priority, video_id;
    private int notificationId = 0, requestId = 0;

    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String NOTIFICATION_TITLE = "notificationTitle";
    private static final String NOTIFICATION_DESCRIPTION = "notifitaionDescription";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID ="admin_channel";

    private int notification_count = 0;

    public static AppDatabase appDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

        appDatabase = AppDatabase.getAppDatabse(this);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String recentToken = s;

        Log.d(TAG, "onTokenRefresh: "+recentToken);

        SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.FCM_TOKEN), recentToken);
        editor.apply();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        activeStatus = sharedPreferences.getBoolean(getString(R.string.pref_notification_key), true);
        vibrationStatus = sharedPreferences.getBoolean(getString(R.string.pref_notification_vibrate_key), true);
        priorityStatus = sharedPreferences.getString(getString(R.string.notification_priority_key), "normalPriority");

        notificationId = new Random().nextInt(60000);
        requestId = (int) System.currentTimeMillis();

        if (remoteMessage.getData().size() > 0){
            msgId = remoteMessage.getMessageId();
            msgTitle = remoteMessage.getData().get("title");
            msgBody = remoteMessage.getData().get("message");
            imageUrl = remoteMessage.getData().get("imageUrl");
            priority = remoteMessage.getData().get("priority");
            video_id = remoteMessage.getData().get("videoId");

            appDatabase.appDatabaseobject().addnotification(make_noti_pack_with_image());
        }

        if (remoteMessage.getNotification() != null){
            msgId = remoteMessage.getMessageId();
            msgTitle = remoteMessage.getNotification().getTitle();
            msgBody = remoteMessage.getNotification().getBody();
            video_id = remoteMessage.getData().get("videoId");

            appDatabase.appDatabaseobject().addnotification(make_noti_pack_without_image());
        }

        if (activeStatus) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            showNotification(remoteMessage);
        }

        super.onMessageReceived(remoteMessage);
    }

    @Override
    public void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    private void showNotification(RemoteMessage remoteMessage) {
        Intent notificationIntent;
        if (video_id != null){
            notificationIntent = new Intent(this, Video_play.class);
            notificationIntent.putExtra(getString(R.string.passvid), video_id);
            notificationIntent.putExtra(getString(R.string.passTitle), msgTitle);
            notificationIntent.putExtra(getString(R.string.videoplayIntentSourceFlag), true);
        } else {
            notificationIntent = new Intent(this, NewsDetails.class);
            notificationIntent.putExtra(getString(R.string.newsidTag), msgId);
            notificationIntent.putExtra(getString(R.string.newsidPhoneTag), notificationId);
            notificationIntent.putExtra(getString(R.string.newsheadingTag), msgTitle);
            notificationIntent.putExtra(getString(R.string.newsimagetag), imageUrl);
            notificationIntent.putExtra(getString(R.string.newsDetailstag), msgBody);
            notificationIntent.putExtra(getString(R.string.intentFlagTag), true);
        }

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notificationId, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap bitmap = getBitmapfromUrl(imageUrl);

        Intent likeIntent = new Intent(this,LikeService.class);
        likeIntent.putExtra("action", "makefavourite");
        likeIntent.putExtra(NOTIFICATION_ID_EXTRA,notificationId);
        likeIntent.putExtra(NOTIFICATION_TITLE, msgTitle);
        likeIntent.putExtra(NOTIFICATION_DESCRIPTION, msgBody);
        likeIntent.putExtra(IMAGE_URL_EXTRA,imageUrl);
        PendingIntent likePendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notificationId,likeIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels();
        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        notificationBuilder.setSmallIcon(R.drawable.banichitraapplogonotification);
        notificationBuilder.setContentText(msgBody);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setContentTitle(msgTitle);
        notificationBuilder.setSound(defaultSoundUri);
        notificationBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
        notificationBuilder.setDeleteIntent(createOnDismissedIntent(getApplicationContext(), notificationId));

        if (imageUrl == null){
            notificationBuilder.setContentIntent(pendingIntent);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgBody) );
        } else {
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                            .setSummaryText(msgBody)
                            .bigPicture(bitmap)
                    );
            /*notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgBody));
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgTitle));*/
            notificationBuilder.addAction(R.drawable.ic_favourite, getString(R.string.notification_add_to_cart_button),likePendingIntent);
            notificationBuilder.setContentIntent(pendingIntent);
        }

        Log.d("priority", String.valueOf(priority));
        if (priorityStatus.equals("highPriority")){
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        } else if (priorityStatus.equals("normalPriority")){
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
        }
//        if (priority != null){
//            if (priority.equals("high")){
//                notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
//            } else if (priority.equals("normal")){
//                notificationBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
//            }
//        } else {
//            notificationBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
//        }
        if (vibrationStatus){
            notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        }
        notificationManager.notify(notificationId, notificationBuilder.build());
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        updateNotificationCount();
        ShortcutBadger.applyCount(FcmMessagingService.this, getNotificationCount());
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(){
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        adminChannel.setShowBadge(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    private PendingIntent createOnDismissedIntent(Context context, int notificationId) {
        Intent intent = new Intent(context, NotificationDismissedReciever.class);
        intent.putExtra("com.my.app.notificationId", notificationId);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), notificationId, intent, PendingIntent.FLAG_ONE_SHOT);
        return pendingIntent;
    }

    public void updateNotificationCount(){
        int p = sharedPreferences.getInt(getString(R.string.iconBadgeCount), 0);
        int f = p+1;
        editor.putInt(getString(R.string.iconBadgeCount), f);
        editor.commit();
    }
    public int getNotificationCount(){
        return sharedPreferences.getInt(getString(R.string.iconBadgeCount), 0);
    }
    public void resetNotificationCount(){
        editor.putInt(getString(R.string.iconBadgeCount), 0);
        editor.commit();
    }

    private App_notification make_noti_pack_with_image() {
        App_notification notification = new App_notification();
        notification.setNotificationid(this.msgId);
        notification.setNotificationBody(this.msgBody);
        notification.setNotificationImageUrl(this.imageUrl);
        notification.setNotificationTitle(this.msgTitle);
        notification.setNotificationPriority(this.priority);

        if (this.video_id != null){
            notification.setVideoId(this.video_id);
        }

        return notification;
    }
    private App_notification make_noti_pack_without_image(){
        App_notification notification = new App_notification();
        notification.setNotificationid(this.msgId);
        notification.setNotificationBody(this.msgBody);
        notification.setNotificationTitle(this.msgTitle);

        if (this.video_id != null){
            notification.setVideoId(this.video_id);
        }

        return notification;
    }
}