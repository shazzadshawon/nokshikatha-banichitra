package biz.agvcorp.banichitra.database_classes;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DatabaseAccessInterface {

    @Insert
    public void addVideo(Video video);

    @Query("Select * from videos")
    public List<Video> readVideo();

    @Query("Select * from videos where userid like :uid and vid like :vid")
    public abstract List<Video> findVideobyUserid(String uid, String vid);

    @Update
    public void updateVideo(Video video);

    @Delete
    public void deleteVideo(Video video);


    @Insert
    public void addnotification(App_notification notification);

    @Query("Select * from app_notification")
    public List<App_notification> readNotification();

    @Query("Select * from app_notification where notificationid like :nid")
    public abstract List<App_notification> findNotificationbyNid(String nid);

    @Update
    public void updatenotification(App_notification notification);

    @Delete
    public void deletenotification(App_notification notification);
}
