package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class Watchlater_videotitle_adapter {

    private ArrayList<String> thumbs = new ArrayList<>();

    public Watchlater_videotitle_adapter(){

    }

    public Watchlater_videotitle_adapter(String parent){
        switch (parent){
            case "Tab_allupload":
                list_maker();
                break;
            case "Tab_recentupload":
                list_maker();
                break;
            case "Tab_favourite":
                list_maker();
                break;
            case "Tab_topview":
                list_maker();
                break;
            default:
                list_maker();
                break;
        }
    }

    private void list_maker(){
        this.thumbs.add("Wonder Woman");
        this.thumbs.add("Man of Steel");
        this.thumbs.add("Batman Begins");
        this.thumbs.add("Silence of the Lambs");
        this.thumbs.add("God of War");
        this.thumbs.add("Edge of Tomorrow");
        this.thumbs.add("Spiderman");
        this.thumbs.add("Infinity War");
    }

    private void list_randomizer(){
        Collections.shuffle(this.thumbs);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.thumbs;
    }
}
