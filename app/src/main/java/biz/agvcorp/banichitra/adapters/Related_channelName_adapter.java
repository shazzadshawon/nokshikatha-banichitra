package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class Related_channelName_adapter {

    private ArrayList<String> thumbs = new ArrayList<>();

    public Related_channelName_adapter(){
        list_maker();
    }

    private void list_maker(){
        this.thumbs.add("Banichitra Production");
        this.thumbs.add("Banichitra Production");
        this.thumbs.add("Banichitra Production");
        this.thumbs.add("Banichitra Production");
        this.thumbs.add("Banichitra Production");
        this.thumbs.add("Banichitra Production");
        this.thumbs.add("Banichitra Production");
        this.thumbs.add("Banichitra Production");
    }

    private void list_randomizer(){
        Collections.shuffle(this.thumbs);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.thumbs;
    }
}
