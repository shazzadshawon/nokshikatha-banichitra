package biz.agvcorp.banichitra;

/**
 * Created By - Shazzadur Rahaman on 8/29/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.people.v1.PeopleService;
import com.google.api.services.people.v1.model.Person;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.resource.Resource;

import java.io.IOException;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.droidsonroids.gif.GifImageView;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class Login extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        EasyPermissions.PermissionCallbacks,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "Login";

    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;
    private GoogleSignInAccount googleSignInAccount;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebase_listener;

    private String profilepicurl, coverPhoto, displayname, googleid, useremail;

    private ImageView mImageView;

    private RelativeLayout relativeLayout, login_btn_container;
    private SignInButton mSignInButton;
    private Button apiSignInButton;
    private Snackbar internetSnackbar;

    private Vibrator vibe;
    private ProgressDialog mProgress;

    public Boolean isloginSystemOauth = true;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    final int RC_API_CHECK = 100;
    public static final int SIGN_IN_CODE = 1000;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private MakeAuthority curr_make_authority = null;
    private PeopleAsync curr_people_async = null;

    private ProgressBar mLoginProgress;

    private Button mFacebook_login_btn;
    private CallbackManager mFacebook_callback_manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*FACEBOOK LOGIN PROCESS*/
//        AccessToken fb_accessToken = AccessToken.getCurrentAccessToken();
//        boolean isLoggedIn = fb_accessToken != null && !fb_accessToken.isExpired();
//
//        mFacebook_login_btn = findViewById(R.id.facebook_login_loginpage);
//        .
//        LoginManager.getInstance().registerCallback(mFacebook_callback_manager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Toast.makeText(Login.this, "Logged In With Facebook", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onCancel() {
//                Toast.makeText(Login.this, "User Canceled login process with Facebook", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Toast.makeText(Login.this, "Facebook Login Error", Toast.LENGTH_SHORT).show();
//            }
//        });
//        mFacebook_login_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("public_profile", "user_friends"));
//            }
//        });

        sharedPreferences = getBaseContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        mProgress = new ProgressDialog(this);
        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.PeopleApiClientId))
                .requestServerAuthCode(getString(R.string.PeopleApiClientId))
                .requestEmail()
                .requestProfile()
                .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.PLUS_ME))
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mImageView = findViewById(R.id.imageView2);

        mLoginProgress = findViewById(R.id.loginProgressBar);
        mLoginProgress.setProgressDrawable(getResources().getDrawable(R.drawable.my_progress));
        mLoginProgress.setMax(100);
        mLoginProgress.setVisibility(View.GONE);

        relativeLayout = findViewById(R.id.activity_log_in);
        login_btn_container = findViewById(R.id.linearLayout_buttons);
        mSignInButton = findViewById(R.id.loginWithGoogle);
        apiSignInButton = findViewById(R.id.withoutloginButton);

        internetSnackbar = Snackbar.make(relativeLayout, "No Internet Connection!", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        retryNetwork();
                    }
                });


        firebaseAuth = FirebaseAuth.getInstance();
        firebase_listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    setuserData_firebase(user);
                    goMainScreen();
                }
            }
        };

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isloginSystemOauth = true;
                setLoginState();
                initiateLogin();
            }
        });
        apiSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoginProgress(mLoginProgress.getProgress() + 1);
                toggle_login_btns(false);
                toggle_logo_image(true);

                isloginSystemOauth = false;
                setLoginState();
                initiateLogin();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebase_listener);
    }

    /**
     * Respond to requests for permissions at runtime for API 23 and above.
     * @param requestCode The request code passed in
     *     requestPermissions(android.app.Activity, String, int, String[])
     * @param permissions The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *     which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //mFacebook_callback_manager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null){
            switch (requestCode){
                case REQUEST_GOOGLE_PLAY_SERVICES:
                    if (resultCode != RESULT_OK){
                        getSnackbar(getString(R.string.googlePlayRequiredText), Snackbar.LENGTH_LONG).show();
                    } else {
                        initiateLogin();
                    }
                    break;
                case SIGN_IN_CODE:
                    if (resultCode == RESULT_OK && data != null && data.getExtras() != null){

                        //showProgress( getString(R.string.appsigninMsg) );
                        showLoginProgress(25);
                        toggle_login_btns(false);
                        toggle_logo_image(true);

                        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                        handleSignInResult(result);
                    } else {
                        getSnackbar(getString(R.string.signinRequiredText), Snackbar.LENGTH_LONG)
                                .setAction("Try Again", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        initiateLogin();
                                    }
                                }).show();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebase_listener != null){
            firebaseAuth.removeAuthStateListener(firebase_listener);
        }
        if (mProgress.isShowing()){
            mProgress.dismiss();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("connection", "msg: " + connectionResult.getErrorMessage());
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();
    }




















    /*
    * ALL METHODS ..................................................................................
    * */
    private void initiateLogin(){
        if (isloginSystemOauth){        // OAuth Login System
            if ( !isGooglePlayServicesAvailable() ){
                acquireGooglePlayServices();
            } else if ( !isDeviceOnline() ){
                internetSnackbar.show();
                View sbView = internetSnackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.RED);
                textView.setAllCaps(true);
            } else {
                internetSnackbar.dismiss();
                chooseAccount();
            }
        } else {    // API Key Login system
            //showProgress(getString(R.string.login_withoutLogin_text));
            loginWithoutGoogle();
        }
    }

    private void retryNetwork() {
        initiateLogin();
    }

    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount(){
        if (EasyPermissions.hasPermissions(this, android.Manifest.permission.GET_ACCOUNTS)){
            accountChooseIntent();
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.getAccountReasonText),
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    private void accountChooseIntent(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, SIGN_IN_CODE);
    }

    private Snackbar getSnackbar(String message, int durationCode){
        return Snackbar.make(relativeLayout, message, durationCode);
    }

    private String serverAuthenticationCode, googleauthenticationtoken;
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()){
            hideProgress();
            googleSignInAccount = result.getSignInAccount();
            if (googleSignInAccount != null){

                serverAuthenticationCode = result.getSignInAccount().getServerAuthCode();
                editor.putString(getString(R.string.serverAuthenticationCode), serverAuthenticationCode);
                editor.apply();

                MakeAuthority makeAuthority = new MakeAuthority(serverAuthenticationCode);
                curr_make_authority = makeAuthority;
                curr_make_authority.m_context = Login.this;
                curr_make_authority.execute();


                //new PeopleAsync(result).execute(account.getServerAuthCode());
            } else {
                hideLoginProgress();
                toggle_login_btns(true);
                toggle_logo_image(false);
                getSnackbar( getString(R.string.googlesignInErrMsg), Snackbar.LENGTH_LONG ).setAction(R.string.googlesigninretry, new LoginretryListener()).show();
            }
            //firebaseAuthWithGoogle(result.getSignInAccount());
        } else {
            hideLoginProgress();
            toggle_login_btns(true);
            toggle_logo_image(false);
            getSnackbar( getString(R.string.not_log_in), Snackbar.LENGTH_LONG ).setAction(R.string.googlesigninretry, new LoginretryListener()).show();
        }
    }

    private void firebaseAuthWithGoogle() {
        //showProgress( getString(R.string.signingFirebaseMsg) );
        showLoginProgress(mLoginProgress.getProgress()+25);

        AuthCredential credential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()){
                    hideProgress();
                    hideLoginProgress();
                    toggle_login_btns(true);
                    toggle_logo_image(false);
                    getSnackbar(getString(R.string.firebaseAuthFailedMsg), Snackbar.LENGTH_LONG).setAction("OK Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            firebaseAuthWithGoogle();
                        }
                    }).show();
                }
            }
        });
    }

    private void goMainScreen() {
        showLoginProgress(100);

        hideProgress();
        Intent intent = new Intent(this, MainActivity.class);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void showMessage(int msg) {
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     * @return true if Google Play Services is available and up to
     *     date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     * @param connectionStatusCode code describing the presence (or lack of)
     *     Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) { GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                Login.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    /**
     * Checks whether the device currently has a network connection.
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isDeviceInternetAvailable(){
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.equals("");
        } catch (Exception e) {
            return false;
        }
    }

    private void showProgress(String msg){
        mProgress.setMessage(msg);
        mProgress.show();
    }
    private void hideProgress(){
        mProgress.setMessage("");
        mProgress.dismiss();
    }

    private Boolean getLoginStatus(){
        return sharedPreferences.getBoolean(getString(R.string.loginStatus), true);
    }

    private String getFcmToken(){
        return sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
    }

    private String getDeviceID(){
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public void setRowId(int id){
        editor.putInt( getString(R.string.rowid),  id);
        editor.commit();
    }
    private void setLoginState(){
        editor.putBoolean( getString(R.string.loginStatus), isloginSystemOauth);
        editor.commit();
    }
    public int gerRowId(){
        return sharedPreferences.getInt(getString(R.string.rowid), 0);
    }

    private void setuserData_firebase(FirebaseUser user){
        displayname = user.getDisplayName();
        googleid = user.getUid();
        profilepicurl = String.valueOf(user.getPhotoUrl());
        useremail = user.getEmail();
    }

    private void saveLoginDataSP(){
        editor.putString("username", displayname);
        editor.putString("profilePic", profilepicurl);
        editor.putString("email", useremail);
        editor.putString("googleId", googleid);
        editor.putString("coverPhoto", coverPhoto);
        editor.commit();
    }

    private void loginWithoutGoogle(){
        showLoginProgress(mLoginProgress.getProgress() + 25);
        String uri = getString(R.string.host_url)+"/"+getString(R.string.app_login_without_google)+"/"+getDeviceID();
        RequestQueue queue = Volley.newRequestQueue(Login.this);
        JsonObjectRequest getRequest = new JsonObjectRequest(JsonRequest.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        hideProgress();
                        showLoginProgress(100);
                        goMainScreen();
                    } else {
                        hideProgress();
                        mLoginProgress.setProgress(0);
                        hideLoginProgress();
                        toggle_login_btns(true);
                        toggle_logo_image(false);

                        getSnackbar("Network Failour or Server is currently offline. ", Snackbar.LENGTH_LONG)
                                .setAction("Try Again", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        initiateLogin();
                                    }
                                }).show();
                    }
                } catch (JSONException e) {
                    hideProgress();
                    hideLoginProgress();
                    toggle_login_btns(true);
                    toggle_logo_image(false);

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress();
                hideLoginProgress();
                mLoginProgress.setProgress(0);
                toggle_login_btns(true);
                toggle_logo_image(false);
                getSnackbar("Network Failour or Server is currently offline. ", Snackbar.LENGTH_LONG)
                        .setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                initiateLogin();
                            }
                        }).show();
            }
        });
        queue.add(getRequest);
    }

    private void showLoginProgress(int progress){
        if (mLoginProgress.getVisibility() == View.GONE){
            mLoginProgress.setVisibility(View.VISIBLE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mLoginProgress.setProgress(progress, true);
        } else {
            mLoginProgress.setProgress(progress);
        }
    }

    private void hideLoginProgress(){
        if (mLoginProgress != null){
            if (mLoginProgress.getVisibility() == View.VISIBLE){
                mLoginProgress.setVisibility(View.GONE);
            }
        }
    }

    private void toggle_login_btns(Boolean flag){
        if (flag){
            if (login_btn_container != null){
                if (login_btn_container.getVisibility() == View.GONE){
                    login_btn_container.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (login_btn_container != null){
                if (login_btn_container.getVisibility() == View.VISIBLE){
                    login_btn_container.setVisibility(View.GONE);
                }
            }
        }
    }

    private void toggle_logo_image(Boolean flag){
//        if (flag){
//            if (mImageView != null && mAnimatedimage != null){
//                if (mImageView.getVisibility() == View.VISIBLE){
//                    mImageView.setVisibility(View.GONE);
//                    mAnimatedimage.setVisibility(View.VISIBLE);
//                }
//            }
//        } else {
//            if (mImageView != null && mAnimatedimage != null){
//                if (mImageView.getVisibility() == View.GONE){
//                    mImageView.setVisibility(View.VISIBLE);
//                    mAnimatedimage.setVisibility(View.GONE);
//                }
//            }
//        }
    }




















    /*
    * ALL CLASSES .........................................................................................
    * */
    private static class MakeAuthority extends AsyncTask<Void, Void, Void>{
        private GoogleCredential credential;
        private String serverAuthenticationCode;
        private String accessToken;
        public Login m_context;

        public MakeAuthority(String code){
            this.serverAuthenticationCode = code;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(
                        new NetHttpTransport(),
                        JacksonFactory.getDefaultInstance(),
                        "https://www.googleapis.com/oauth2/v4/token",
                        "409734570725-7n33m0acfmj4t7mq608ph5g19shjkgpm.apps.googleusercontent.com",
                        "Tdd7p7RUvb1oPpSlp0WEgEeY",
                        serverAuthenticationCode,
                        "http://shobarjonnoweb.com/banichitra/php/comment_handling.php")
                        .execute();

                accessToken = tokenResponse.getAccessToken();
                credential = new GoogleCredential().setAccessToken(accessToken);

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (credential != null){
                if (accessToken != null){
                    m_context.googleauthenticationtoken = accessToken;
                    m_context.editor.putString(m_context.getString(R.string.GoogleAccessToken), accessToken);
                    m_context.editor.apply();
                }

                PeopleAsync peopleAsync = new PeopleAsync(credential);
                m_context.curr_people_async = peopleAsync;
                m_context.curr_people_async.m_contxt = m_context;
                m_context.curr_people_async.execute(serverAuthenticationCode);
            }
        }
    }


    private static class PeopleAsync extends AsyncTask<String, Void, Person> {
        private GoogleSignInResult result;
        private GoogleCredential credential;
        private String birthdayStr, addressStr, genderStr, localeStr, occupationStr, relationStatusStr, phoneNumberStr, agestr, coverPhotoUrl;
        private Date birthdaydate;
        private Exception mLastError = null;

        public Login m_contxt = null;

        PeopleAsync(GoogleSignInResult result){
            this.result = result;
        }

        PeopleAsync(GoogleCredential credential){
            this.credential = credential;
        }

        private Years getAge(String date){
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
            LocalDate bDate = formatter.parseLocalDate(date);
            Years age = Years.yearsBetween(bDate, LocalDate.now());
            return age;
        }

        private Date ConvertToDate(String dateString){
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date convertedDate = new Date();
            try {
                convertedDate = format.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return convertedDate;
        }

        private String parseJsontoString(Person person, String param, String params){
            JSONArray temp;
            String res = "";
            try {
                switch (params){
                    case "gender":
                        temp = new JSONArray(person.getGenders().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "address":
                        temp = new JSONArray(person.getAddresses().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "locale":
                        temp = new JSONArray(person.getLocales().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "occupation":
                        temp = new JSONArray(person.getOccupations().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "relationStatus":
                        temp = new JSONArray(person.getRelationshipStatuses().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "phonenumber":
                        temp = new JSONArray(person.getPhoneNumbers().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "coverPhoto":
                        temp = new JSONArray(person.getCoverPhotos().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return res;
        }

        private void sendtoPHPServer(){
            //m_contxt.showProgress( m_contxt.getString(R.string.phpsendinginitMsg) );
            m_contxt.showLoginProgress(m_contxt.mLoginProgress.getProgress()+25);

            String uri = m_contxt.getString(R.string.host_url)+"/"+m_contxt.getString(R.string.app_login);

            RequestQueue queue = Volley.newRequestQueue(m_contxt);

            Map<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("profilePicUrl",m_contxt.profilepicurl);
            jsonParams.put("displayName", m_contxt.displayname);
            jsonParams.put("emailAddress",m_contxt.useremail);
            jsonParams.put("googleId", m_contxt.googleid);
            jsonParams.put("phoneNumber",phoneNumberStr);
            jsonParams.put("birthday", birthdayStr);
            jsonParams.put("age", agestr);
            jsonParams.put("locale", localeStr);
            jsonParams.put("occupation", occupationStr);
            jsonParams.put("gender", genderStr);
            jsonParams.put("address", addressStr);
            jsonParams.put("deviceId", m_contxt.getDeviceID());
            jsonParams.put("fcmToken",m_contxt.getFcmToken());
            jsonParams.put("google_access_token", m_contxt.googleauthenticationtoken);
            jsonParams.put("serverauthenticatiotoken", m_contxt.serverAuthenticationCode);

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(JSONObject response) {
                    m_contxt.hideProgress();
                    try {
                        if (response.getBoolean("status")){
                            m_contxt.setRowId( Integer.parseInt(response.getString("userId")) );
                            //getSnackbar(getString(R.string.phpserversuccessMsg), Snackbar.LENGTH_SHORT).show();
                            if (credential != null){
                                m_contxt.firebaseAuthWithGoogle();
                            }
                        } else {
                            m_contxt.mLoginProgress.setProgress(0);
                            m_contxt.hideLoginProgress();
                            m_contxt.toggle_login_btns(true);
                            m_contxt.toggle_logo_image(false);
                            m_contxt.getSnackbar(m_contxt.getString(R.string.phpservererrorMsg), Snackbar.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        m_contxt.mLoginProgress.setProgress(0);
                        m_contxt.hideLoginProgress();
                        m_contxt.toggle_login_btns(true);
                        m_contxt.toggle_logo_image(false);
                        m_contxt.getSnackbar(m_contxt.getString(R.string.phpservererrorMsg), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    m_contxt.hideProgress();
                    m_contxt.mLoginProgress.setProgress(0);
                    m_contxt.hideLoginProgress();
                    m_contxt.toggle_login_btns(true);
                    m_contxt.toggle_logo_image(false);
                    m_contxt.getSnackbar(m_contxt.getString(R.string.phpservererrorMsg), Snackbar.LENGTH_SHORT).show();
                }
            });
            queue.add(postRequest);
        }

        @Override
        protected Person doInBackground(String... params) {
            Person profile = new Person();

            try {
                /*PeopleService peopleService = PeopleHelper.setUp(Login.this, params[0]);
                profile = peopleService.people().get("people/me")
                        .setPersonFields("names,photos,emailAddresses,ageRanges,addresses,birthdays,coverPhotos,phoneNumbers,genders,locales,occupations,relationshipStatuses")
                        .execute();*/

                PeopleService peopleService = new PeopleService.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(), credential)
                                                .setApplicationName(m_contxt.getString(R.string.app_name))
                                                .build();
                profile = peopleService.people().get("people/me")
                        .setPersonFields("names,photos,emailAddresses,ageRanges,addresses,birthdays,coverPhotos,phoneNumbers,genders,locales,occupations,relationshipStatuses")
                        .execute();

                Log.d(TAG, "profileResponse: "+profile);

                if (profile == null){
                    m_contxt.getSnackbar(m_contxt.getString(R.string.googleLoginError), Snackbar.LENGTH_SHORT);
                } else {

                    if (profile.getNames() != null){
                        m_contxt.displayname = profile.getNames().get(0).getDisplayName();
                        m_contxt.googleid = profile.getNames().get(0).getMetadata().getSource().getId();
                    } else {
                        m_contxt.displayname = m_contxt.getString(R.string.defaultDisplayName);
                        m_contxt.googleid = m_contxt.getString(R.string.defaultGoogleId);
                    }

                    if (profile.getPhotos() != null){
                        m_contxt.profilepicurl = profile.getPhotos().get(0).getUrl();
                    } else {
                        m_contxt.profilepicurl = "";
                    }

                    if (profile.getEmailAddresses() != null){
                        m_contxt.useremail = profile.getEmailAddresses().get(0).getValue();
                    } else {
                        m_contxt.useremail = m_contxt.getString(R.string.defaultEmailAddress);
                    }

                    if (profile.getAddresses() != null){
                        addressStr = parseJsontoString(profile, "formattedValue", "address");
                    } else {
                        addressStr = m_contxt.getString(R.string.noaddressText);
                    }

                    if (profile.getBirthdays() != null){
                        try {
                            JSONArray birthday = new JSONArray(profile.getBirthdays().toString());
                            JSONObject date = (JSONObject) birthday.getJSONObject(0).get("date");
                            String dateStr = date.get("day").toString()+"-"+date.get("month").toString()+"-"+date.get("year").toString();

                            birthdayStr = dateStr;
                            birthdaydate = ConvertToDate(dateStr);

                            agestr = String.valueOf(getAge(birthdayStr).getYears());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            birthdayStr = m_contxt.getString(R.string.noBirthdayText);
                            agestr = m_contxt.getString(R.string.noageText);
                        }
                    } else {
                        birthdayStr = m_contxt.getString(R.string.noBirthdayText);
                        agestr = m_contxt.getString(R.string.noageText);
                    }

                    if (profile.getGenders() != null){
                        genderStr = parseJsontoString(profile, "formattedValue", "gender");
                    } else {
                        genderStr = m_contxt.getString(R.string.noGenderText);
                    }

                    if (profile.getLocales() != null){
                        localeStr = parseJsontoString(profile, "value", "locale");
                    } else {
                        localeStr = m_contxt.getString(R.string.noLocatText);
                    }

                    if (profile.getOccupations() != null){
                        occupationStr = parseJsontoString(profile, "value", "occupation");
                    } else {
                        occupationStr = m_contxt.getString(R.string.nooccupationText);
                    }

                    if (profile.getRelationshipStatuses() != null){
                        relationStatusStr = parseJsontoString(profile, "formattedValue", "relationStatus");
                    } else {
                        relationStatusStr = m_contxt.getString(R.string.relationStsText);
                    }

                    if (profile.getPhoneNumbers() != null){
                        phoneNumberStr = parseJsontoString(profile, "value", "phonenumber");
                    } else {
                        phoneNumberStr = m_contxt.getString(R.string.noPhoneText);
                    }

                    if (profile.getCoverPhotos() != null){
                        coverPhotoUrl = parseJsontoString(profile, "url", "coverPhoto");
                        m_contxt.coverPhoto = coverPhotoUrl;
                    }
                }

            } catch (IOException e) {
                mLastError = e;
                cancel(true);
                e.printStackTrace();
                return null;
            }
            return profile;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //m_contxt.showProgress( m_contxt.getString(R.string.fetchProfileDataMsg) );
            m_contxt.showLoginProgress(m_contxt.mLoginProgress.getProgress()+10);
        }

        @Override
        protected void onPostExecute(Person profile) {
            super.onPostExecute(profile);
            m_contxt.hideProgress();
            m_contxt.saveLoginDataSP();
//            Glide.with(getApplicationContext())
//                    .load(coverPhotoUrl)
//                    .into(new SimpleTarget<Drawable>() {
//                        @Override
//                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
//                            nav_header.setBackground(resource);
//                        }
//                    });

//            addressTextView.setText("Address - "+addressStr);
//            birthdayTextView.setText("Birthday - "+birthdayStr);
//            ageTextView.setText("Age - "+agestr+" years");
//            genderTextView.setText("Gender - "+genderStr);
//            localsTextView.setText("Locality code - "+localeStr);
//            occupationTextView.setText("Occupation - "+occupationStr);
//            relationshipstausTextView.setText("Status - "+relationStatusStr);
//            phoneTextView.setText("Phone no - "+phoneNumberStr);

            sendtoPHPServer();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            m_contxt.hideProgress();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    m_contxt.showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    m_contxt.startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            MainActivity.REQUEST_AUTHORIZATION);
                } else {
                    //getSnackbar("The following error occurred:\n" + mLastError.getMessage(), Snackbar.LENGTH_INDEFINITE).show();
                }
            } else {
                //getSnackbar("Request cancelled.", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private class LoginretryListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            initiateLogin();
        }
    }
}
