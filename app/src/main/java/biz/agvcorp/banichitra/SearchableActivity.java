package biz.agvcorp.banichitra;

/**
 * Created By - Shazzadur Rahaman on 8/29/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
import android.accounts.Account;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import biz.agvcorp.banichitra.drawer_fragments.Profilepage_drawer;
import biz.agvcorp.banichitra.drawer_fragments.Settings_drawer;
import biz.agvcorp.banichitra.recycler_adapters.Recycler_adapter;

public class SearchableActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{
    private static final String TAG = "SearchableActivity";

    public static GoogleApiClient googleApiClient;
    public static GoogleSignInOptions gso;

    private TextView mNoresult, mAction_text_search;
    private String sq;
    private ProgressDialog mLoaderProgress;

    private Toolbar toolbar;
    private Account mAccount;
    private GoogleAccountCredential credential;
    private com.google.api.services.youtube.YouTube mYoutube = null;
    private static HttpTransport transport;
    private JacksonFactory jasonfactory;

    private ArrayList<String> mVideoId = new ArrayList<>();
    private ArrayList<String> mThumbnail = new ArrayList<>();
    private ArrayList<String> mTitle = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();
    private ArrayList<String> mDuration = new ArrayList<>();

    private RecyclerView recyclerView;
    private Recycler_adapter recycler_adapter;

    private LinearLayoutManager layoutManager_list;
    private GridLayoutManager layoutManager_grid;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private final static int NUM_COLUM = 2;

    private SharedPreferences sharedPreferences, sharedPreferences_settings;

    private String YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY, YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY, LAYOUT_SETTING_PREV, LAYOUT_SETTING_CURR;

    final int RC_API_CHECK = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        mLoaderProgress = new ProgressDialog(this);
        mLoaderProgress.setMessage(getString(R.string.youtubeSearchString));
        mLoaderProgress.show();

        sharedPreferences = getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        sharedPreferences_settings = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(SearchableActivity.this));

        get_previous_settings();
        settings_equalizer();

        if (LAYOUT_SETTING_CURR.equals("list")){
            layoutManager_list = new LinearLayoutManager(SearchableActivity.this);
        } else if (LAYOUT_SETTING_CURR.equals("grid")){
            layoutManager_grid = new GridLayoutManager(SearchableActivity.this, 2);
        } else if (LAYOUT_SETTING_CURR.equals("staggered")){
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUM, LinearLayoutManager.VERTICAL);
        } else if (LAYOUT_SETTING_CURR.equals("onelineList")){
            layoutManager_list = new LinearLayoutManager(SearchableActivity.this);
        }

        toolbar = findViewById(R.id.sceondery_toolbar);
        mNoresult = findViewById(R.id.search_no_result);

        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.PeopleApiClientId))
                .requestEmail()
                .requestProfile()
                .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.PLUS_ME))
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        googleApiClient.connect();

        Intent intent = getIntent();
        //initSilentLogin(intent);
        handleIntent(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


        //initSilentLogin(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSubmitButtonEnabled(true);

        if (getLoginStatus()) {
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_profile).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            menu.findItem(R.id.action_signout).setVisible(false);
            menu.findItem(R.id.action_signin).setVisible(false);
        } else {
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_profile).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            menu.findItem(R.id.action_signout).setVisible(false);
            menu.findItem(R.id.action_signin).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final Fragment profile_fragment = new Profilepage_drawer();
        final Fragment settings_fragment = new Settings_drawer();

        switch (item.getItemId()){
            case R.id.action_settings:
                Intent settingIntent = new Intent(this, SettingsActivity.class);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                startActivity(settingIntent);
                break;

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("connection", "msg: " + connectionResult.getErrorMessage());
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        googleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        get_current_settings();

        /*COMPARING YOUTUBE THUMBNAIL QUALITY SETTINGS*/
        if (YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY != null && YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY != null){
            if (!YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals(YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY)){
                Toast.makeText(SearchableActivity.this, "Settings changed. Please Swipe To Refresh.", Toast.LENGTH_LONG).show();
            }
        }

        /*COMPARING LAYOUT CHANGE SETTTINGS*/
        if (LAYOUT_SETTING_CURR != null && LAYOUT_SETTING_PREV != null){
            if (!LAYOUT_SETTING_CURR.equals(LAYOUT_SETTING_PREV)){
                makeAppRestartDialog();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        googleApiClient.disconnect();
    }








    private void initSilentLogin(final Intent intent){
        if (getLoginStatus()){
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
            if (opr.isDone()){
                GoogleSignInResult result = opr.get();
                handleSignInResult(result, intent);
            } else {
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                        handleSignInResult(googleSignInResult, intent);
                    }
                });
            }
        } else {
            //Logged in without Google
            //mProgressDialog.setMessage("Fetching data from YouTube server . . .");
            //mProgressDialog.show();
            //new Tab_allupload.Slider_async().execute("makeSlider");
        }
    }

    private void handleSignInResult(GoogleSignInResult result, Intent intent){
        if (result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            if (account != null){
                mAccount = account.getAccount();

                credential = GoogleAccountCredential.usingOAuth2(this, Collections.singleton( getString(R.string.YOUTUBE_SCOPE) ));
                credential.setSelectedAccount(mAccount);

                transport = AndroidHttp.newCompatibleTransport();
                jasonfactory = JacksonFactory.getDefaultInstance();
                mYoutube = new com.google.api.services.youtube.YouTube.Builder(transport, jasonfactory, credential)
                        .setApplicationName(getString(R.string.youtubeAppname))
                        .build();

                handleIntent(intent);
            }
        }
    }

    private void handleIntent(Intent intent){
        if (Intent.ACTION_SEARCH.equals(intent.getAction())){
            String query = intent.getStringExtra(SearchManager.QUERY);

            /*Saving the recent query to SearchRecentSuggestingPovider*/
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);

            doBanichitraSearch(query);
        }
    }

    private void doBanichitraSearch(String query){
        Toast.makeText(this, "Search Query: "+query, Toast.LENGTH_SHORT).show();
        sq = query;
        hideNoResult();
        //new SearchYouTube(query).execute();
        String[] parts = query.toLowerCase().split(" ");
        String contatedParts = "";
        for (int i = 0; i < parts.length; i++) {
            contatedParts += parts[i];
        }
        //fetchVideoList(contatedParts);
        fetchVideoList(query);
        //new SearchYouTube(contatedParts).execute();
    }

    private void gather_data_recycle(SearchListResponse response){
        clearList(this.mVideoId);
        clearList(this.mTitle);
        clearList(this.mThumbnail);
        clearList(this.mDescription);
        clearList(this.mDuration);
        List<SearchResult> items = response.getItems();
        for (int i = 0; i < items.size(); i++) {
            String vid = items.get(i).getId().getVideoId();
            String vTitle = items.get(i).getSnippet().getTitle();
            String vDescription = items.get(i).getSnippet().getDescription();

            String vThumbnail = getString(R.string.youtubeThumbnailDefaultRes);
            switch (YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY){
                case "default":
                    vThumbnail = items.get(i).getSnippet().getThumbnails().getDefault().getUrl();
                    break;
                case "medium":
                    vThumbnail = items.get(i).getSnippet().getThumbnails().getMedium().getUrl();
                    break;
                case "high":
                    vThumbnail = items.get(i).getSnippet().getThumbnails().getHigh().getUrl();
                    break;
                case "standard":
                    vThumbnail = items.get(i).getSnippet().getThumbnails().getStandard().getUrl();
                    break;
                case "maxres":
                    vThumbnail = items.get(i).getSnippet().getThumbnails().getMaxres().getUrl();
                    break;
            }

            String vDuration = items.get(i).getSnippet().getChannelTitle();

            mVideoId.add( vid );
            mTitle.add( vTitle );
            mDescription.add( vDescription );
            mThumbnail.add( vThumbnail );
            mDuration.add(vDuration);
        }
        initRecyclerView();
    }

    private void gather_data_recycle_nologin(JSONObject response) {
        clearList(this.mVideoId);
        clearList(this.mTitle);
        clearList(this.mThumbnail);
        clearList(this.mDescription);
        clearList(this.mDuration);

        if (!response.isNull("items")){
            try {
                JSONArray results = response.getJSONArray("items");
                if (results.length() <= 0){
                    //Nothing found on Search
                    if (mLoaderProgress.isShowing()){
                        mLoaderProgress.dismiss();
                    }
                    showNoResult(this.sq);
                } else {
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject item = results.getJSONObject(i);

                        if ( item.getJSONObject("id").has("videoId") ){
                            mVideoId.add( item.getJSONObject("id").getString("videoId") );
                            mTitle.add( item.getJSONObject("snippet").getString("title") );
                            mDescription.add( item.getJSONObject("snippet").getString("description") );

                            if(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals("maxres")){
                                mThumbnail.add( item.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url") );
                            } else {
                                mThumbnail.add( item.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY).getString("url") );
                            }

                            mDuration.add( item.getJSONObject("snippet").getString("channelTitle") );
                        }
                    }

                    initRecyclerView();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                if (mLoaderProgress.isShowing()){
                    mLoaderProgress.dismiss();
                }
                //showNoResult(this.sq);
                showNoResult(e.toString());
            }
        } else {
            //Nothing found on Search
            if (mLoaderProgress.isShowing()){
                mLoaderProgress.dismiss();
            }
            showNoResult(this.sq);
        }
    }

    private void responseTest(JSONObject response) {
        clearList(this.mVideoId);
        clearList(this.mTitle);
        clearList(this.mThumbnail);
        clearList(this.mDescription);
        clearList(this.mDuration);

        if (!response.isNull("items")){
            try {
                JSONArray results = response.getJSONArray("items");
                if (results.length() <= 0){
                    //Nothing found on Search
                    if (mLoaderProgress.isShowing()){
                        mLoaderProgress.dismiss();
                    }
                    showNoResult(this.sq);
                } else {
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject item = results.getJSONObject(i);

                        mVideoId.add( item.getJSONObject("id").getString("videoId") );
                        mTitle.add( item.getJSONObject("snippet").getString("title") );
                        mDescription.add( item.getJSONObject("snippet").getString("description") );

                        if(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals("maxres")){
                            mThumbnail.add( item.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url") );
                        } else {
                            mThumbnail.add( item.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY).getString("url") );
                        }

                        mDuration.add( item.getJSONObject("snippet").getString("channelTitle") );
                    }

                    initRecyclerView();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                if (mLoaderProgress.isShowing()){
                    mLoaderProgress.dismiss();
                }
                showNoResult(this.sq);
            }
        } else {
            //Nothing found on Search
            if (mLoaderProgress.isShowing()){
                mLoaderProgress.dismiss();
            }
            showNoResult(this.sq);
        }
    }

    private void initRecyclerView(){
        if (mLoaderProgress.isShowing()){
            mLoaderProgress.dismiss();
        }
        recyclerView = findViewById(R.id.recyclerView_search_res);
        recycler_adapter = new Recycler_adapter(LAYOUT_SETTING_CURR, this, mVideoId, mThumbnail, mTitle, mDescription, mDuration);
        recycler_adapter.notifyDataSetChanged();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(recycler_adapter);

        /*LAYOUT MANAGER IS THE SWITCH TO SWITCH BETWEEN LINEAR LAYOUT MANAGER AND GRID LAYOUT MANAGER*/
        switch (LAYOUT_SETTING_CURR){
            case "list":
                recyclerView.setLayoutManager(layoutManager_list);
                break;
            case "grid":
                recyclerView.setLayoutManager(layoutManager_grid);
                break;
            case "staggered":
                recyclerView.setLayoutManager(staggeredGridLayoutManager);
                break;
            case "onelineList":
                recyclerView.setLayoutManager(layoutManager_list);
                break;
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void showNoResult(String query){
        mNoresult.setVisibility(View.VISIBLE);
        mNoresult.setText("No result found for your search: "+query+" in this channel");
    }

    private void hideNoResult(){
        mNoresult.setText("");
        mNoresult.setVisibility(View.GONE);
    }

    private Boolean getLoginStatus(){
        return sharedPreferences.getBoolean(getString(R.string.loginStatus), true);
    }

    private void clearList(ArrayList list){
        list.clear();
    }

    private void fetchVideoList(String query){
        String uri = getString(R.string.youtube_base_address)+
                "/"+getString(R.string.yt_search)+
                "?part=snippet&maxResults="+
                getString(R.string.searchMaxResuls)+
                "&channelId="+getString(R.string.MASTER_CHANNEL_ID)+
                "&type=video"+
                "&q="+query+
                "&key="+getString(R.string.YOUTUBE_API_KEY_WEB);

        RequestQueue yt_queue = Volley.newRequestQueue(this);
        JsonObjectRequest yt_pl_item_request = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null){
                    gather_data_recycle_nologin(response);
                } else {
                    if (mLoaderProgress.isShowing()){
                        mLoaderProgress.dismiss();
                    }
                    showNoResult(sq);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                if (mLoaderProgress.isShowing()){
                    mLoaderProgress.dismiss();
                }
                showNoResult(sq);
            }
        });
        yt_queue.add(yt_pl_item_request);
    }

    private void get_current_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            LAYOUT_SETTING_CURR = sharedPreferences_settings.getString(getString(R.string.recycler_layout_key), "list");
        } else {
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            LAYOUT_SETTING_CURR = "list";
        }
    }

    private void get_previous_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            LAYOUT_SETTING_PREV = sharedPreferences_settings.getString(getString(R.string.recycler_layout_key), "list");
        } else {
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            LAYOUT_SETTING_PREV = "list";
        }
    }

    private void settings_equalizer(){
        YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY;
        LAYOUT_SETTING_CURR = LAYOUT_SETTING_PREV;
    }

    private void makeAppRestartDialog(){
        AlertDialog.Builder nonCancelableDialog = new AlertDialog.Builder(SearchableActivity.this);
        //nonCancelableDialog.setCancelable(false);
        nonCancelableDialog.setTitle(getString(R.string.restartAppdialogTitle));
        nonCancelableDialog.setMessage(getString(R.string.restartAppdialogMessage));
        nonCancelableDialog.setPositiveButton("Restart Application", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Restart the app
                Intent restartIntent = getPackageManager().getLaunchIntentForPackage( getPackageName() );
                assert restartIntent != null;
                restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(restartIntent);
            }
        });
        nonCancelableDialog.setNegativeButton("Restart Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Dissmiss the dialog
            }
        });

        AlertDialog dialog = nonCancelableDialog.create();
        dialog.show();
    }














    private class SearchYouTube extends AsyncTask<Void, Void, SearchListResponse>{
        private final String query;
        private Exception exception;
        SearchYouTube(String q){
            this.query = q;
        }

        @Override
        protected SearchListResponse doInBackground(Void... voids) {
            SearchListResponse response = new SearchListResponse();
            try {
                response = searchyoutube();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        private SearchListResponse searchyoutube() throws IOException {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet");
            parameters.put("maxResults", getString(R.string.searchMaxResuls));
            parameters.put("q", this.query);
            parameters.put("type", "");
            parameters.put("channelId", getString(R.string.MASTER_CHANNEL_ID));

            YouTube.Search.List searchListByKeywordRequest = mYoutube.search().list(parameters.get("part".toString()));
            if (parameters.containsKey("maxResults")){
                searchListByKeywordRequest.setMaxResults(Long.parseLong(parameters.get("maxResults").toString()));
            }
            if (parameters.containsKey("q") && parameters.get("q") != "") {
                searchListByKeywordRequest.setQ(parameters.get("q").toString());
            }
            if (parameters.containsKey("type") && parameters.get("type") != "") {
                searchListByKeywordRequest.setType(parameters.get("type").toString());
            }
            if (parameters.containsKey("channelId") && parameters.get("channelId") != ""){
                searchListByKeywordRequest.setChannelId(parameters.get("channelId").toString());
            }

            SearchListResponse response = searchListByKeywordRequest.execute();
            return response;
        }

        @Override
        protected void onPostExecute(SearchListResponse response) {
            super.onPostExecute(response);
            gather_data_recycle(response);
            Log.d(TAG, "onPostExecute: "+response);
        }
    }
}
