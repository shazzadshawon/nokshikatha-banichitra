package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class Root_videoDescription_adapter {

    private ArrayList<String> thumbs = new ArrayList<>();

    public Root_videoDescription_adapter(){

    }

    public Root_videoDescription_adapter(String parent){
        switch (parent){
            case "allupload":
                list_maker();
                break;
            case "recentupload":
                list_maker();
                break;
            case "favourite":
                list_maker();
                break;
            case "topview":
                list_maker();
                break;
            default:
                list_maker();
                break;
        }
    }

    private void list_maker(){
        this.thumbs.add("Chris Stuckmann reviews Adrift, starring Shailene Woodley, Sam Claflin. Directed by Baltasar Kormákur.");
        this.thumbs.add("A 24/7 stream of relaxing and calm piano music you can use as study music, focus or concentration music, background music for any mind and creative work, to think, read, code, daydream or reflect, or simply to enjoy.");
        this.thumbs.add("Beautiful piano music for studying and sleeping (no loop, see tracklist below). This relaxing music is composed by me, Peder B. Helland, and can be described as study music, sleep music and relaxation music. ");
        this.thumbs.add("A tribute in honor of Chester Bennington.  Linkin Park was a huge inspiration for me when I was young and this news has been devastating.  Rest in peace, Chester.");
        this.thumbs.add("La grande sonata al chiaro di luna.");
        this.thumbs.add("Perhaps one of Beethoven's most popular pieces-\"Fur Elise\" (translated into For Elise). The picture is one of Beethoven when he was a middle-age man");
        this.thumbs.add("Wolfgang Amadeus Mozart's final Masterpiece was commissioned in mid 1791 by the Austrian ");
        this.thumbs.add("This is the first movement of Beethoven's 5th symphony.  Composed between 1804 and 1808.");
    }

    private void list_randomizer(){
        Collections.shuffle(this.thumbs);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.thumbs;
    }
}
