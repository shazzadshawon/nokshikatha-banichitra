package biz.agvcorp.banichitra.notifications;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elyeproj.loaderviewlibrary.LoaderImageView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import biz.agvcorp.banichitra.MainActivity;
import biz.agvcorp.banichitra.R;
import me.leolin.shortcutbadger.ShortcutBadger;

public class NewsDetails extends AppCompatActivity {
    private Toolbar toolbar;
    private LoaderImageView mNotiImageView;
    private TextView mNotiHeading;
    private HtmlTextView mNotiText;

    private String n_id;
    private int notification_id;
    private Boolean intentFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        toolbar = findViewById(R.id.news_toolbar);
        AppCompatActivity appCompatActivity = this;
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(true);

        mNotiImageView = findViewById(R.id.news_details_image);
        mNotiHeading = findViewById(R.id.news_details_heading);
        mNotiText = findViewById(R.id.news_details_text);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent ownIntent = getIntent();
        n_id = ownIntent.getStringExtra(getString(R.string.newsidTag));
        String heading = ownIntent.getStringExtra(getString(R.string.newsheadingTag));
        String img_url = ownIntent.getStringExtra(getString(R.string.newsimagetag));
        String body = ownIntent.getStringExtra(getString(R.string.newsDetailstag));
        intentFlag = ownIntent.getBooleanExtra(getString(R.string.intentFlagTag), false);

        if (ownIntent.hasExtra(getString(R.string.newsidPhoneTag))){
            notification_id = ownIntent.getIntExtra(getString(R.string.newsidPhoneTag), 0);
        }
        clearNotification();
        ShortcutBadger.applyCount(NewsDetails.this, 0);

        mNotiHeading.setText(heading);
        mNotiText.setText(body);

        if (img_url.equals("noimage")){
            if (mNotiImageView.getVisibility() == View.VISIBLE){
                mNotiImageView.setVisibility(View.GONE);
            }
        } else {
            if (mNotiImageView.getVisibility() == View.GONE){
                mNotiImageView.setVisibility(View.VISIBLE);
            }
            if (!img_url.equals("")){
                Glide.with(getApplicationContext())
                        .asBitmap()
                        .load(img_url)
                        .into(mNotiImageView);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (intentFlag){
            Intent mainIntent = new Intent(this, MainActivity.class);
            mainIntent.putExtra("fromNewsDetailPage", n_id);
            startActivity(mainIntent);
            finish();
        } else {
            onBackPressed();
        }
        return super.onSupportNavigateUp();
    }

    private void clearNotification(){
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notification_id);
    }
}
