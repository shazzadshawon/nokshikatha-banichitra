package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class Related_videoView_adapter {

    private ArrayList<String> thumbs = new ArrayList<>();

    public Related_videoView_adapter(){
        list_maker();
    }

    private void list_maker(){
        this.thumbs.add("1.1 M Views");
        this.thumbs.add("1.2 M Views");
        this.thumbs.add("2.1 M Views");
        this.thumbs.add("3.1 M Views");
        this.thumbs.add("4.1 M Views");
        this.thumbs.add("1 K Views");
        this.thumbs.add("1 K Views");
        this.thumbs.add("6 K Views");
    }

    private void list_randomizer(){
        Collections.shuffle(this.thumbs);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.thumbs;
    }
}
