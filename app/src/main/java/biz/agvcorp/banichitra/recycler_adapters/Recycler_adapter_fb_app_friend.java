package biz.agvcorp.banichitra.recycler_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.elyeproj.loaderviewlibrary.LoaderImageView;

import java.util.ArrayList;

import biz.agvcorp.banichitra.R;

/**
 * Created By - Shazzadur Rahaman on 9/6/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
public class Recycler_adapter_fb_app_friend extends RecyclerView.Adapter<Recycler_adapter_fb_app_friend.ViewHolder>{
    private static final String TAG = "Recycler_adapter_fb_app";

    private ArrayList<String> mThumbnail = new ArrayList<>();
    private Context mContext;

    public Recycler_adapter_fb_app_friend(ArrayList<String> mThumbnail, Context mContext) {
        clearAll();
        this.mThumbnail = mThumbnail;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_fb_friends, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext)
                .asBitmap()
                .load(mThumbnail.get(position))
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return this.mThumbnail.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        LoaderImageView thumbnail;
        RelativeLayout recycle_item_container;

        public ViewHolder(View itemView) {
            super(itemView);

            thumbnail = itemView.findViewById(R.id.fb_recycle_friend);
            recycle_item_container = itemView.findViewById(R.id.fb_frlistrecycle_item_container);
        }
    }

    private void clearAll(){
        this.mThumbnail.clear();
    }
}
