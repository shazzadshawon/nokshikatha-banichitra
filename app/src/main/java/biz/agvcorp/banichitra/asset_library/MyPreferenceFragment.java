package biz.agvcorp.banichitra.asset_library;

import android.os.Bundle;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.asset_library.AppPreferenceFragment;

/**
 * Created By - Shazzadur Rahaman on 9/3/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
public class MyPreferenceFragment extends AppPreferenceFragment {
    public static final String FRAGMENT_TAG = "my_preference_fragment";

    public MyPreferenceFragment() {
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.app_prefferences, rootKey);
    }
}
