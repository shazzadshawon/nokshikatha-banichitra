package biz.agvcorp.banichitra.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SectionPageAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "SectionPageAdapter";

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    //private HashMap<Integer, FragmentDipsplayPageContent> cachedFragmentHashMap;

    public SectionPageAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
        /*
        * This Workaround is doing the trick. It is called only when notifyDataSetChanged() is called.
        * When a fragment is deleted, and a new fragment is added, adapter does not instantanize the
        * fragment immediately. POSITION_NONE force all the fragment to instantanize always. This is
        * harmful for memory and performance. I must need to find another solution later on when
        * optimising. But for now this is the workaround.
        * This is also the reason for crashing the app after couple of tab changing with the slider
        * API calling.
        *
        * Possible fix will be here ---  https://stackoverflow.com/questions/10849552/update-viewpager-dynamically/17855730#17855730
        * */
        //return FragmentStatePagerAdapter.POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public int getPositionbyName(String name){
        int pos = -1;
        for (int i = 0; i < mFragmentTitleList.size(); i++){
            if (mFragmentTitleList.get(i).equals(name)){
                pos = i;
            }
        }
        return pos;
    }

    public void removePage(int position){
        if (position <= getCount()){
            mFragmentTitleList.remove(position);
            mFragmentList.remove(position);
        }
        notifyDataSetChanged();
    }

    public void removeAllPage(){
        for (int i = 0; i < mFragmentTitleList.size(); i++) {
            removePage(i);
        }
    }



}
