package biz.agvcorp.banichitra;

/**
 * Created By - Shazzadur Rahaman on 8/29/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import pl.droidsonroids.gif.GifImageView;

public class Splash extends AppCompatActivity {

    private static int TIME_OUT = 5000;
//    private static GifImageView gifSplash;
//    private static ImageView gifSplash;
//    private SimpleDraweeView gifSplash;

    private Handler animePlayerHandeler;
    private MediaPlayer mSplashSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);

        mSplashSound = MediaPlayer.create(this, R.raw.ting_splash);
        mSplashSound.start();

        setContentView(R.layout.activity_splash);

//        gifSplash = findViewById(R.id.gifsplash);
//        gifSplash.setImageResource(R.drawable.banichitralogo_new1000);

        /*gifSplash = findViewById(R.id.gifsplash);
        Glide.with(this)
                .asGif()
//                .load(R.drawable.banichitralogoanimationofficialbig)  //my made middle res
//                .load(R.drawable.banichitralogoanimationofficial2)    //working small res
                .load(R.drawable.banichitralogo_new500)      //sceond time provided mid res
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .listener(new RequestListener<GifDrawable>() {

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                        onGifFinished();//do your stuff
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                        resource.setLoopCount(1);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                while(true) {
                                    if(!resource.isRunning()) {
                                        break;
                                    }
                                }
                            }
                        }).start();
                        return false;
                    }
        }).into(gifSplash);*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Splash.this, Login.class);
                startActivity(intent);
                finish();
            }
        }, TIME_OUT);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSplashSound.release();
        finish();
    }

    private void onGifFinished(){
        Toast.makeText(this, "Gif Finished", Toast.LENGTH_SHORT).show();
    }
}
