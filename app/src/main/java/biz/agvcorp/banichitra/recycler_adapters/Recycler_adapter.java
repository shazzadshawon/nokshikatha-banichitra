package biz.agvcorp.banichitra.recycler_adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.elyeproj.loaderviewlibrary.LoaderImageView;
import com.elyeproj.loaderviewlibrary.LoaderTextView;

import java.util.ArrayList;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.Video_play;

public class Recycler_adapter extends RecyclerView.Adapter<Recycler_adapter.ViewHolder> {

    private static final String TAG = "Recycler_adapter";

    private ArrayList<String> mVideos = new ArrayList<>();
    private ArrayList<String> mThumblains = new ArrayList<>();
    private ArrayList<String> mTitle = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();
    private ArrayList<String> mDuration = new ArrayList<>();
    private Context mContext;
    public String layoutFlag;

    public Recycler_adapter(String layoutFlag, Context mContext, ArrayList<String> mVidios, ArrayList<String> mThumblains, ArrayList<String> mTitle, ArrayList<String> mDescription, ArrayList<String> mDuration) {
        //clearAll();
        this.layoutFlag = layoutFlag;
        this.mVideos = mVidios;
        this.mThumblains = mThumblains;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mDuration = mDuration;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (layoutFlag){
            case "list":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item, parent, false);
                break;
            case "grid":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_grid, parent, false);
                break;
            case "staggered":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_staggered, parent, false);
                break;
            case "onelineList":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_onelinelist, parent, false);
                break;
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    int lastPosition = -1;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final int holder_position = position;
        Log.d(TAG, "onBindViewHolder: Called");
        Log.d(TAG, "onBindViewHolder: "+ mThumblains);

        if (mThumblains != null && mTitle != null && mDescription != null){
            Glide.with(mContext)
                    .asBitmap()
                    .load(mThumblains.get(position))
                    .into(holder.thumbnail);
            holder.videoTitle.setText(mTitle.get(position));
            holder.videoDesc.setText(mDescription.get(position));

            if (!mDuration.isEmpty()){
                if (mDuration.get(position) != null){
                    holder.videoDuration.setText(mDuration.get(position));
                }
            }
            holder.thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    go_to_video_page(holder_position);
                }
            });
            holder.recycle_item_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    go_to_video_page(holder_position);
                }
            });

            Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return mThumblains.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        //ImageView thumbnail;
        LoaderImageView thumbnail;
        LoaderTextView videoTitle;
        LoaderTextView videoDesc;
        LoaderTextView videoDuration;
        RelativeLayout recycle_item_container;

        public ViewHolder(View itemView) {
            super(itemView);

            thumbnail = itemView.findViewById(R.id.imageView);
            videoTitle = itemView.findViewById(R.id.videoTitle);
            videoDesc = itemView.findViewById(R.id.videoDescription);
            videoDuration = itemView.findViewById(R.id.videoDuration);
            recycle_item_container = itemView.findViewById(R.id.recycle_item_container);
        }
    }

    private void go_to_video_page(int position){
        String vid = mVideos.get(position);
        Log.d(TAG, "go_to_video_page: VideoID = " + vid);

        //Tab_allupload tab_allupload = new Tab_allupload();
        //tab_allupload.save_current_vid(vid);

        Intent videoIntent = new Intent(mContext, Video_play.class);
        videoIntent.putExtra(mContext.getString(R.string.passvid), vid);
        videoIntent.putExtra(mContext.getString(R.string.passTitle), mTitle.get(position));
        //videoIntent.putExtra(mContext.getString(R.string.passDesc), mDescription.get(position));
        mContext.startActivity(videoIntent);

        //Toast.makeText(mContext, "Clicked on the video named: "+mTitle.get(position), Toast.LENGTH_SHORT).show();
    }

    private void clearAll(){
        this.mVideos.clear();
        this.mThumblains.clear();
        this.mTitle.clear();
        this.mDuration.clear();
        this.mDescription.clear();
    }

    public void removeItem(int position){
        mVideos.remove(position);
        mThumblains.remove(position);
        mTitle.remove(position);
        mDescription.remove(position);
        mDuration.remove(position);

        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mVideos.size());
    }

    public void restoreItem(int position, String vid, String thumb, String title, String description, String durr){
        mVideos.add(position, vid);
        mThumblains.add(position, thumb);
        mTitle.add(position, title);
        mDescription.add(position, description);
        mDuration.add(position, durr);

        notifyItemInserted(position);
    }

}
