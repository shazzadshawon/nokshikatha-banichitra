package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class Related_videotitle_adapter {

    private ArrayList<String> thumbs = new ArrayList<>();

    public Related_videotitle_adapter(){
        list_maker();
    }

    private void list_maker(){
        this.thumbs.add("Wonder Woman");
        this.thumbs.add("Man of Steel");
        this.thumbs.add("Batman Begins");
        this.thumbs.add("Silence of the Lambs");
        this.thumbs.add("God of War");
        this.thumbs.add("Edge of Tomorrow");
        this.thumbs.add("Spiderman");
        this.thumbs.add("Infinity War");
    }

    private void list_randomizer(){
        Collections.shuffle(this.thumbs);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.thumbs;
    }
}
