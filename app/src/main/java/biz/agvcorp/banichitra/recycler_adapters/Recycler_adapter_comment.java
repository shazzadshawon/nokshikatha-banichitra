package biz.agvcorp.banichitra.recycler_adapters;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import biz.agvcorp.banichitra.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class Recycler_adapter_comment extends RecyclerView.Adapter<Recycler_adapter_comment.ViewHolder>{
    private static final String TAG = "Recycler_adapter_commen";

    private ArrayList<String> mVideoId = new ArrayList<>();
    private ArrayList<String> mMyCommentId = new ArrayList<>();
    private ArrayList<String> mCommentId = new ArrayList<>();
    private ArrayList<String> mAuthorName = new ArrayList<>();
    private ArrayList<String> mAuthorChannelUrl = new ArrayList<>();
    private ArrayList<String> mauthorProfileImage = new ArrayList<>();
    private ArrayList<String> mText = new ArrayList<>();
    private ArrayList<String> mRatting = new ArrayList<>();
    private ArrayList<String> mLikeCount = new ArrayList<>();
    private ArrayList<String> mDateTime = new ArrayList<>();

    private ArrayList<Boolean> mCanRate = new ArrayList<>();
    private Context mContext;

    private CommentRElatedCallback commentRElatedCallback;

    public Recycler_adapter_comment(Context mContext, ArrayList<String> mVideoId, ArrayList<String> mCommentId, ArrayList<String> mAuthorName, ArrayList<String> mAuthorChannelUrl, ArrayList<String> mauthorProfileImage, ArrayList<String> mText, ArrayList<String> mRatting, ArrayList<String> mLikeCount, ArrayList<String> mDateTime, ArrayList<Boolean>canRate, ArrayList<String> myCommentId, CommentRElatedCallback commentRElatedCallback) {
        clearAll();
        this.mVideoId = mVideoId;
        this.mCommentId = mCommentId;
        this.mAuthorName = mAuthorName;
        this.mAuthorChannelUrl = mAuthorChannelUrl;
        this.mauthorProfileImage = mauthorProfileImage;
        this.mText = mText;
        this.mRatting = mRatting;
        this.mLikeCount = mLikeCount;
        this.mDateTime = mDateTime;
        this.mContext = mContext;
        this.mCanRate = canRate;
        this.mMyCommentId = myCommentId;
        this.commentRElatedCallback = commentRElatedCallback;

        Log.d(TAG, "Recycler_adapter_comment: " + mMyCommentId);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_comment, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int holder_position = position;
        if (mVideoId != null &&
            mCommentId != null &&
            mAuthorName != null &&
            mAuthorChannelUrl != null &&
            mauthorProfileImage != null &&
            mText != null &&
            mRatting != null &&
            mLikeCount != null &&
            mDateTime != null){

            Glide.with(mContext)
                    //.asBitmap()
                    .load(mauthorProfileImage.get(position))
                    .into(holder.mProfile);
            holder.mText.setText(mText.get(position));
            holder.mCommenter.setText(mAuthorName.get(position));
            holder.mComment_like_count.setText(mLikeCount.get(position));
            holder.mComment_datetime.setText(mDateTime.get(position));

            if (mCanRate.get(position)){
                holder.mComment_like_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }

            if (!mAuthorChannelUrl.get(position).equals("")){
                holder.mGotoAuthorChannel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goToBrowser(mAuthorChannelUrl.get(holder_position));
                    }
                });
            } else {
                Toast.makeText(this.mContext, this.mContext.getString(R.string.comm_cuthor_channel_url_no_msg), Toast.LENGTH_SHORT).show();
            }

            holder.m_overflow_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu comment_popup = new PopupMenu(mContext, holder.m_overflow_btn);
                    comment_popup.inflate(R.menu.comment_option_menu);

                    comment_popup.getMenu().findItem(R.id.comment_edit).setVisible(false);
                    comment_popup.getMenu().findItem(R.id.comment_delete).setVisible(false);
                    comment_popup.getMenu().findItem(R.id.comment_menu_report).setVisible(true);
                    comment_popup.getMenu().findItem(R.id.comment_menu_ban).setVisible(true);
                    comment_popup.getMenu().findItem(R.id.comment_menu_somtinh).setVisible(true);

                    if (!mMyCommentId.isEmpty()){
                        for (int i = 0; i < mMyCommentId.size(); i++) {
                            if ( mCommentId.get(holder_position).equals(mMyCommentId.get(i)) ){
                                // THIS IS MY COMMENT
                                comment_popup.getMenu().findItem(R.id.comment_edit).setVisible(true);
                                comment_popup.getMenu().findItem(R.id.comment_delete).setVisible(true);
                                comment_popup.getMenu().findItem(R.id.comment_menu_report).setVisible(false);
                                comment_popup.getMenu().findItem(R.id.comment_menu_ban).setVisible(false);
                                comment_popup.getMenu().findItem(R.id.comment_menu_somtinh).setVisible(true);
                                break;
                            }
                        }
                    }

                    comment_popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.comment_edit:
                                    //Toast.makeText(mContext, "Editing My Comment", Toast.LENGTH_SHORT).show();
                                    commentRElatedCallback.onCommentEdit(mCommentId.get(holder_position), holder_position);
                                    return true;
                                case R.id.comment_delete:
                                    commentRElatedCallback.onCommentDelet(mCommentId.get(holder_position), holder_position);
                                    return true;
                                case R.id.comment_menu_report:
                                    commentRElatedCallback.onCommentReporting(mCommentId.get(holder_position), holder_position);
                                    return true;
                                case R.id.comment_menu_ban:
                                    commentRElatedCallback.onCommentBanning(mCommentId.get(holder_position), holder_position);
                                    return true;
                                case R.id.comment_menu_somtinh:
                                    commentRElatedCallback.onCommentSpam(mCommentId.get(holder_position), holder_position);
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    comment_popup.show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mCommentId.size();
    }


    private void goToBrowser(String uri){
        if (uri != null){
            Uri ad_uri = Uri.parse(uri);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, ad_uri);

            PackageManager packageManager = this.mContext.getPackageManager();
            List<ResolveInfo> activities = packageManager.queryIntentActivities(browserIntent, PackageManager.MATCH_DEFAULT_ONLY);
            boolean isIntentSafe = activities.size() > 0;

            if (isIntentSafe){
                String intentTitle = this.mContext.getString(R.string.browserChooserText);
                Intent chooser = Intent.createChooser(browserIntent, intentTitle);
                this.mContext.startActivity(chooser);
            } else {
                //THERE IS NOT ANY APP INSTALLED IN YOUR PHONE TO OPEN THIS LINK
                Toast.makeText(this.mContext, this.mContext.getString(R.string.noBrowserText), Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        CircleImageView mProfile;
        TextView mText, mCommenter, mCommenterChannel, mComment_like_count, mComment_dislike_count, mComment_datetime;
        Button mComment_like_btn, mComment_dislike_btn, mGotoAuthorChannel_btn;
        TextView m_overflow_btn;


        public ViewHolder(View itemView) {
            super(itemView);

            mProfile = itemView.findViewById(R.id.comment_item_image);
            mText = itemView.findViewById(R.id.comment_item_text);
            mCommenter = itemView.findViewById(R.id.commenter_name);
            mComment_like_btn = itemView.findViewById(R.id.comment_like);
            mComment_like_count = itemView.findViewById(R.id.comment_like_count);
            mGotoAuthorChannel_btn = itemView.findViewById(R.id.commenter_channel_go_btn);
            m_overflow_btn = itemView.findViewById(R.id.commentbox_menu_btn);
            //mComment_dislike_btn = itemView.findViewById(R.id.comment_dislike);
            //mComment_dislike_count = itemView.findViewById(R.id.comment_dislike_count);
            mComment_datetime = itemView.findViewById(R.id.comment_datetime);
        }
    }

    private void clearAll(){
        this.mVideoId.clear();
        this.mCommentId.clear();
        this.mAuthorName.clear();
        this.mauthorProfileImage.clear();
        this.mAuthorChannelUrl.clear();
        this.mText.clear();
        this.mRatting.clear();
        this.mLikeCount.clear();
        this.mDateTime.clear();
    }

    public interface CommentRElatedCallback {
        void onCommentEdit(String commentId, int position);
        void onCommentDelet(String commentId, int position);
        void onCommentReporting(String commentId, int position);
        void onCommentBanning(String commentID, int position);
        void onCommentSpam(String commentID, int position);
    }
}
