package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class Root_videoid_adapter {

    private ArrayList<String> vid = new ArrayList<>();

    public Root_videoid_adapter(){

    }

    public Root_videoid_adapter(String parent){
        switch (parent){
            case "allupload":
                list_maker();
                break;
            case "recentupload":
                list_maker();
                break;
            case "favourite":
                list_maker();
                break;
            case "topview":
                list_maker();
                break;
            default:
                list_maker();
                break;
        }
    }

    private void list_maker(){
        this.vid.add("kQQTo1WxQk4");
        this.vid.add("TG796An9kNk");
        this.vid.add("P-_zhCAv4aY");
        this.vid.add("D3zzfdb0gZU");
        this.vid.add("MYtbnTGVoQY");
        this.vid.add("0il7xdpUPB8");
        this.vid.add("6hoVDburJ-I");
        this.vid.add("SKtJY6soCds");
    }

    private void list_randomizer(){
        Collections.shuffle(this.vid);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.vid;
    }
}
