package biz.agvcorp.banichitra.asset_library;

import android.content.Context;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.people.v1.PeopleService;
import com.google.api.services.people.v1.PeopleServiceScopes;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import biz.agvcorp.banichitra.R;

public class PeopleHelper {
    private static final String TAG = "PeopleHelper";
    private static final String APPLICATION_NAME = "Banichitra";

    private static final List<String> SCOPES = Arrays.asList(PeopleServiceScopes.CONTACTS_READONLY);
    private static final String CREDENTIALS_FOLDER = "credentials";

    public static PeopleService setUp(Context context, String serverAuthCode) throws IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        JacksonFactory jsonFactory = JacksonFactory.getDefaultInstance();

        // Redirect URL for web based applications.
        // Can be empty too.
        String redirectUrl = "urn:ietf:wg:oauth:2.0:oob";
        //System.out.println(serverAuthCode);
        // Exchange auth code for access token
        GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(
                httpTransport,
                jsonFactory,
                context.getString(R.string.PeopleApiClientId),
                context.getString(R.string.PeopleApiclientSecret),
                serverAuthCode,
                redirectUrl).execute();

        // Then, create a GoogleCredential object using the tokens from GoogleTokenResponse
        GoogleCredential credential = new GoogleCredential.Builder()
                .setClientSecrets(context.getString(R.string.PeopleApiClientId), context.getString(R.string.PeopleApiclientSecret))
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .build();


//        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
//                httpTransport,
//                jsonFactory,
//                context.getString(R.string.clientId),
//                context.getString(R.string.clientSecret),
//                SCOPES)
//                .setAccessType("offline")
//                .build();
//        String accessToken  = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user").getAccessToken();
//        credential.setAccessToken(accessToken);

        credential.setFromTokenResponse(tokenResponse);

        // credential can then be used to access Google services
        return new PeopleService.Builder(httpTransport, jsonFactory, credential).build();
    }
}
