package biz.agvcorp.banichitra.asset_library;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.adapters.Slider_adapter;

public class Fetch_slider_list extends AsyncTask<String, Void, String> {
    private Slider_adapter slider_adapter;
    private ProgressDialog dialog;
    private TextSliderView textSliderView;
    private Context mContext;
    private Activity mActivity;
    private ArrayList<TextSliderView> slides = new ArrayList<>();
    private String pageId;


    private HashMap<String,String> urls = new HashMap<String, String>();
    private HashMap<String,String> files = new HashMap<String, String>();

    public Fetch_slider_list(Context context){

    }

    private Boolean checkforActivity(){
        Activity activity = mActivity;
        if (activity != null){
            return true;
        } return false;
    }

    private void parseSliderData(JSONArray data){
        for (int i = 0; i < data.length(); i++) {
            try {
                JSONObject obj = data.getJSONObject(i);
                String slide_title = obj.getString("title");
                String slide_video_id = obj.getString("url");
                String slide_name = mContext.getString(R.string.host_url_slider_image)+obj.getString("image");

                this.urls.put(slide_title, slide_video_id);
                this.files.put(slide_title, slide_name);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void start_slider(){
        this.slider_adapter = new Slider_adapter();

        for(String name : this.files.keySet()){
            textSliderView = new TextSliderView(mContext);
            textSliderView.description(name)
                    .image(this.files.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra",name);
            slides.add(textSliderView);
        }
    }

    private void fetchSliderDataPHP(){
        String uri = mContext.getString(R.string.host_url)+"/"+mContext.getString(R.string.slider_api)+"/"+pageId;
        RequestQueue queue = Volley.newRequestQueue(mContext);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                if ( checkforActivity() ){
                    try {
                        if (response.getBoolean("status")){
                            parseSliderData(response.getJSONArray("sliders"));
                            start_slider();
                            //add_slide_into_view();

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(postRequest);
    }

    @Override
    protected String doInBackground(String... strings) {
        return null;
    }
}
