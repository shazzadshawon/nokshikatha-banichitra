package biz.agvcorp.banichitra.notifications;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class LikeService extends BroadcastReceiver {

    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String NOTIFICATION_TITLE = "notificationTitle";
    private static final String NOTIFICATION_DESCRIPTION = "notifitaionDescription";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID ="admin_channel";

    private String action, image_url, notification_id, notification_title, notification_description, notification_channel_id;
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        String action = intent.getStringExtra("action");
        String image_url = intent.getStringExtra(IMAGE_URL_EXTRA);
        String notification_id = intent.getStringExtra(NOTIFICATION_ID_EXTRA);
        String notification_title = intent.getStringExtra(NOTIFICATION_TITLE);
        String notification_description = intent.getStringExtra(NOTIFICATION_DESCRIPTION);
        String notification_channel_id = intent.getStringExtra(ADMIN_CHANNEL_ID);

        if (action.equals("makefavourite") && mContext != null){
            performMakeFavourite();
        } else {

        }

        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);
    }

    public void performMakeFavourite(){
        Toast.makeText(mContext, "Added to Favourite", Toast.LENGTH_SHORT).show();
    }
}
