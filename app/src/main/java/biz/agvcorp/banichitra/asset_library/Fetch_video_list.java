package biz.agvcorp.banichitra.asset_library;

import android.content.Context;
import android.os.AsyncTask;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistItemListResponse;

import java.io.IOException;
import java.util.HashMap;

public class Fetch_video_list extends AsyncTask<Void, Void, PlaylistItemListResponse> {
    public FetchVideoEventListener<PlaylistItemListResponse> mCallback;
    public YouTube mYoutube;
    public String playlistId, max_results, pageToken;
    public Context mContext;
    public Exception mException;

    public Fetch_video_list(){

    }

    public Fetch_video_list(Context context, String playlistID, String max_results, YouTube youTube, FetchVideoEventListener callback){
        this.mContext = context;
        this.playlistId = playlistID;
        this.max_results = max_results;
        this.mYoutube = youTube;
        this.mCallback = callback;
    }

    public void dataNao(Context context, String playlistID, String max_results, YouTube youTube, String pageToken, FetchVideoEventListener callback){
        this.mContext = context;
        this.playlistId = playlistID;
        this.max_results = max_results;
        this.mYoutube = youTube;
        this.pageToken = pageToken;
        this.mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mCallback != null){
            if (!isCancelled()){
                mCallback.onFetchingStart();
            }
        }
    }

    @Override
    protected PlaylistItemListResponse doInBackground(Void... voids) {
        PlaylistItemListResponse response = new PlaylistItemListResponse();
        try {
            if (!isCancelled()){
                response = fetchAllVideos();
            } else {
                return null;
            }
        } catch (IOException e) {
            mException = e;
        }
        return response;
    }

    private PlaylistItemListResponse fetchAllVideos() throws IOException {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("part", "snippet,contentDetails");
        parameters.put("maxResults", this.max_results);
        parameters.put("playlistId", this.playlistId);
        parameters.put("pageToken", this.pageToken);

        YouTube.PlaylistItems.List playlistItemsListByPlaylistIdRequest = mYoutube.playlistItems().list(parameters.get("part").toString());
        PlaylistItemListResponse response = new PlaylistItemListResponse();

        try {
            if (playlistItemsListByPlaylistIdRequest != null){
                if (parameters.containsKey("maxResults") && !isCancelled()) {
                    playlistItemsListByPlaylistIdRequest.setMaxResults(Long.parseLong(parameters.get("maxResults").toString()));
                }

                if (parameters.containsKey("playlistId") && parameters.get("playlistId") != "" && !isCancelled()) {
                    playlistItemsListByPlaylistIdRequest.setPlaylistId(parameters.get("playlistId").toString());
                }

                if (parameters.containsKey("pageToken") && parameters.get("pageToken") != "" && !isCancelled() ){
                    playlistItemsListByPlaylistIdRequest.setPageToken(parameters.get("pageToken").toString());
                }

                if (!isCancelled()){
                    response = playlistItemsListByPlaylistIdRequest.execute();
                } else {
                    return null;
                }
            }
        } catch (NullPointerException ne) {

        }

        return response;
    }

    @Override
    protected void onPostExecute(PlaylistItemListResponse response) {
        super.onPostExecute(response);
        if (mCallback != null){
            if (!isCancelled()){
                if (mException == null){
                    mCallback.onSuccess(response);
                } else {
                    mCallback.onFailure(mException);
                }
            }
        }

    }
}
