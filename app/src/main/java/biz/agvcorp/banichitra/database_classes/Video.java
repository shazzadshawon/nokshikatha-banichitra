package biz.agvcorp.banichitra.database_classes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "videos")
public class Video {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "userid")
    private String userId;

    @ColumnInfo(name = "vid")
    private String vid;

    @ColumnInfo(name = "videotitle")
    private String videoTitle;

    @ColumnInfo(name = "videodescription")
    private String videoDescription;

    @ColumnInfo(name = "videothumbnail")
    private String videoThumbnile;





    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoThumbnile() {
        return videoThumbnile;
    }

    public void setVideoThumbnile(String videoThumbnile) {
        this.videoThumbnile = videoThumbnile;
    }
}
