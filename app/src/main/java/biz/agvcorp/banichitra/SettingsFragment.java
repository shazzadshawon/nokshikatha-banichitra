package biz.agvcorp.banichitra;

/**
 * Created By - Shazzadur Rahaman on 8/29/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;
import java.util.Objects;

import biz.agvcorp.banichitra.asset_library.MyPreferenceFragment;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener, PreferenceFragmentCompat.OnPreferenceStartScreenCallback {
    private static final String TAG = SettingsFragment.class.getSimpleName();

    private SharedPreferences sharedPreferences;
    private View mView;

    private AccessToken fb_accessToken;
    private AccessTokenTracker fb_access_token_tracker;
    private CallbackManager mFacebook_callback_manager;
    private Preference facebookConnectPreference;
    private Boolean fb_login_sts = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        facebookConnectPreference = findPreference(getString(R.string.facebook_conect_key));
        fb_accessToken = AccessToken.getCurrentAccessToken();
        facebook_pref_status_login();

        mFacebook_callback_manager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mFacebook_callback_manager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                facebook_pref_status_login();
                if (getFacebookLogin()){
                    fb_login_sts = true;
                    //Logged In
                } else {
                    fb_login_sts = false;
                    //Logged Out
                }
            }

            @Override
            public void onCancel() {
                Toast.makeText(getContext(), "User Canceled login process with Facebook", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getContext(), "Facebook Login Error", Toast.LENGTH_SHORT).show();
            }
        });

        facebookConnectPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (!getFacebookLogin()){
                    LoginManager.getInstance().logInWithReadPermissions(SettingsFragment.this, Arrays.asList("public_profile"));
                } else {
                    LoginManager.getInstance().logOut();
                }
                return false;
            }
        });

        fb_access_token_tracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null){
                    if (fb_login_sts){
                        facebook_pref_status_login();
                    }
                }
            }
        };

        Preference suggestionClearPreference = findPreference(getString(R.string.searchHistoryClearKey));
        suggestionClearPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.searchSuggestionDelete));
                builder.setMessage(getString(R.string.searchSuggestionDeleteMsg));
                DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getContext(), SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
                                suggestions.clearHistory();
                                Snackbar.make(mView, "All your search suggestions have deleted", Snackbar.LENGTH_LONG).show();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                            case DialogInterface.BUTTON_NEUTRAL:
                                break;
                        }
                    }
                };
                builder.setPositiveButton("Yes", clickListener);
                builder.setNegativeButton("No", clickListener);
                AlertDialog dialog = builder.create();
                dialog.show();

                return false;
            }
        });
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.app_prefferences);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.pref_notification_key));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.pref_notification_vibrate_key));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.notification_priority_key));
    }

    @Override
    public Fragment getCallbackFragment() {
        return this;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);
        if (preference instanceof ListPreference) {
            ListPreference listPreference = (ListPreference) preference;
            int prefIndex = listPreference.findIndexOfValue(sharedPreferences.getString(key, ""));
            if (prefIndex >= 0) {
                preference.setSummary(listPreference.getEntries()[prefIndex]);
            }
        } else {
            //preference.setSummary(sharedPreferences.getString(key, ""));
        }
    }

    @Override
    public void onPause() {
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public boolean onPreferenceStartScreen(PreferenceFragmentCompat caller, PreferenceScreen pref) {
//        caller.setPreferenceScreen(pref);
//        return true;

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        MyPreferenceFragment fragment = new MyPreferenceFragment();
        Bundle args = new Bundle();
        args.putString(PreferenceFragmentCompat.ARG_PREFERENCE_ROOT, pref.getKey());
        fragment.setArguments(args);
        fragmentTransaction.add(R.id.settingsContent, fragment, pref.getKey());
        fragmentTransaction.addToBackStack(pref.getKey());
        fragmentTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
        fragmentTransaction.commit();
        return true;
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFacebook_callback_manager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }




    private Boolean getFacebookLogin(){
        fb_accessToken = AccessToken.getCurrentAccessToken();
        if (fb_accessToken != null && !fb_accessToken.isExpired()){
            fb_login_sts = true;
            return true;
        } else {
            fb_login_sts = false;
            return false;
        }
    }
    private void facebook_pref_status_login(){
        if (facebookConnectPreference != null){
            if (getFacebookLogin()){
                facebookConnectPreference.setTitle("Facebook is Connected");
                facebookConnectPreference.setSummary("Tap to disconnect");
                facebookConnectPreference.setIcon(R.drawable.ic_facebook);
            } else {
                facebookConnectPreference.setTitle("Connect to Facebook");
                facebookConnectPreference.setSummary("Tap to connect");
                facebookConnectPreference.setIcon(R.drawable.ic_facebook_no);
            }
        }
    }




    private class SuggestionClearListener implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getContext(), SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
            suggestions.clearHistory();
            Snackbar.make(mView, "All your search suggestions have deleted", Snackbar.LENGTH_LONG).show();
        }
    }
}
