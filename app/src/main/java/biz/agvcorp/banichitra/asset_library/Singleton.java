package biz.agvcorp.banichitra.asset_library;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Singleton {
    private static Singleton mInstance;
    private static Context mctx;
    private static RequestQueue tokenQueue;

    private Singleton(Context context){
        mctx = context;
        tokenQueue = getRequestQueue();
    }

    private RequestQueue getRequestQueue(){
        if (tokenQueue==null){
            tokenQueue = Volley.newRequestQueue(mctx.getApplicationContext());
        }
        return tokenQueue;
    }

    public static synchronized Singleton getmInstance(Context context){
        if (mInstance==null){
            mInstance = new Singleton(context);
        }
        return mInstance;
    }

    public<T> void addtoRequestQueue(Request<T> request){
        getRequestQueue().add(request);
    }
}
