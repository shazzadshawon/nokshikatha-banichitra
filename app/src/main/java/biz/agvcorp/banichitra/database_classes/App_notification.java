package biz.agvcorp.banichitra.database_classes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "app_notification")
public class App_notification {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "notificationid")
    private String notificationid;

    @ColumnInfo(name = "notificationTitle")
    private String notificationTitle;

    @ColumnInfo(name = "notificationBody")
    private String notificationBody;

    @ColumnInfo(name = "notificationImageUrl")
    private String notificationImageUrl;

    @ColumnInfo(name = "notificationPriority")
    private String notificationPriority;

    @ColumnInfo(name = "videoId")
    private String videoId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotificationid() {
        return notificationid;
    }

    public void setNotificationid(String notificationid) {
        this.notificationid = notificationid;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationBody() {
        return notificationBody;
    }

    public void setNotificationBody(String notificationBody) {
        this.notificationBody = notificationBody;
    }

    public String getNotificationImageUrl() {
        return notificationImageUrl;
    }

    public void setNotificationImageUrl(String notificationImageUrl) {
        this.notificationImageUrl = notificationImageUrl;
    }

    public String getNotificationPriority() {
        return notificationPriority;
    }

    public void setNotificationPriority(String notificationPriority) {
        this.notificationPriority = notificationPriority;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
