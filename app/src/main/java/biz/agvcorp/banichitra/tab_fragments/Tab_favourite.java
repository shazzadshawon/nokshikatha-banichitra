package biz.agvcorp.banichitra.tab_fragments;

/**
 * Created By - Shazzadur Rahaman on 8/29/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
import android.accounts.Account;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.elyeproj.loaderviewlibrary.LoaderImageView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.model.PlaylistItemListResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import biz.agvcorp.banichitra.asset_library.FetchVideoEventListener;
import biz.agvcorp.banichitra.asset_library.Fetch_video_list;
import biz.agvcorp.banichitra.MainActivity;
import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.adapters.Root_thumbnail_adapter;
import biz.agvcorp.banichitra.Video_play;
import biz.agvcorp.banichitra.recycler_adapters.Recycler_adapter;
import biz.agvcorp.banichitra.adapters.Root_videoDescription_adapter;
import biz.agvcorp.banichitra.adapters.Root_videoDuration_adapter;
import biz.agvcorp.banichitra.adapters.Root_videoid_adapter;
import biz.agvcorp.banichitra.adapters.Root_videotitle_adapter;
import biz.agvcorp.banichitra.adapters.Slider_adapter;
import pl.droidsonroids.gif.GifImageView;

import static android.app.Activity.RESULT_OK;
import static biz.agvcorp.banichitra.MainActivity.REQUEST_AUTHORIZATION;

public class Tab_favourite extends Fragment implements
        BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = "Tab_favourite";

    private static View mView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private Account mAccount;
    final int RC_API_CHECK = 100;

    private GoogleAccountCredential credential;
    private com.google.api.services.youtube.YouTube mYoutube = null;
    private HttpTransport transport;
    private JacksonFactory jasonfactory;

    private static SliderLayout mSliderLayout;
    private NestedScrollView mNestedScrollView;
    private RecyclerView recyclerView;
    private Recycler_adapter recycler_adapter;
    private RelativeLayout recycle_container;

    private Root_thumbnail_adapter thumb_adapter;
    private Root_videoid_adapter videoid_adapter;
    private Root_videotitle_adapter videoTitle_adapter;
    private Root_videoDescription_adapter videoDescription_adapter;
    private Root_videoDuration_adapter videoDuration_adapter;

    private ArrayList<String> mVideoId = new ArrayList<>();
    private ArrayList<String> mThumbnail = new ArrayList<>();
    private ArrayList<String> mTitle = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();
    private ArrayList<String> mDuration = new ArrayList<>();

    public static ProgressBar mRecyclerProgressbar;
    private LoaderImageView mSliderPlaceholder;
    public static ArrayList<TextSliderView> slides = new ArrayList<>();

    private SharedPreferences sharedPreferences, sharedPreferences_settings;
    private SharedPreferences.Editor editor;

    private Snackbar authorizeSnackbar;
    private ProgressDialog mProgressDialog, mProgressSliderDialog;

    private Slider_async m_curr_slider_async = null;
    private Fetch_video_list m_curr_fetch_video_list = null;

    private Integer recycler_total_result_count, recycler_results_per_page_count, curr_recycler_visible_item_count, first_visible_item_position;
    private String recycler_next_page_token = "";
    private LinearLayoutManager layoutManager_list;
    private GridLayoutManager layoutManager_grid;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private final static int NUM_COLUM = 2;
    private Boolean isLoadingMoreVideos = false;
    private GifImageView mRecyclerLoader;

    private String YOUTUBE_RECYCLER_MAX_LIMIT, YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY, YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY, LAYOUT_SETTING_PREV, LAYOUT_SETTING_CURR;
    private Boolean SLIDER_IS_CURR_HIDDEN, SLIDER_IS_PREV_HIDDEN;

    private AlertDialog settingsChangedialog, appRestartDialog;

    /*
    * ALL OVERRIDES ..........................................................................................
    * */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
         * Possible Fix for the not attached to activity
         * Need to Look out
         * */
        sharedPreferences = getContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        sharedPreferences_settings = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()));
        editor = sharedPreferences.edit();

        get_previous_settings();
        settings_equalizer();

        mProgressDialog = new ProgressDialog(getContext());
        mProgressSliderDialog = new ProgressDialog(getContext());

        initSilentLogin();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.tab_favourite, container, false);
        swipeRefreshLayout = mView.findViewById(R.id.swiperefreshmain);
        swipeRefreshLayout.setOnRefreshListener(this);

        videoid_adapter = new Root_videoid_adapter(TAG);
        thumb_adapter = new Root_thumbnail_adapter(TAG);
        videoTitle_adapter = new Root_videotitle_adapter(TAG);
        videoDescription_adapter = new Root_videoDescription_adapter(TAG);
        videoDuration_adapter = new Root_videoDuration_adapter(TAG);

        Log.d(TAG, "onCreateView: It is running now");

        //gather_data_recycle();
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSliderPlaceholder = view.findViewById(R.id.slider_placeholder);
        mRecyclerProgressbar = view.findViewById(R.id.recyclerprogressBar);

        if (LAYOUT_SETTING_CURR.equals("list")){
            layoutManager_list = new LinearLayoutManager(mView.getContext());
        } else if (LAYOUT_SETTING_CURR.equals("grid")){
            layoutManager_grid = new GridLayoutManager(mView.getContext(), 2);
        } else if (LAYOUT_SETTING_CURR.equals("staggered")){
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUM, LinearLayoutManager.VERTICAL);
        } else if (LAYOUT_SETTING_CURR.equals("onelineList")){
            layoutManager_list = new LinearLayoutManager(mView.getContext());
        }

        mNestedScrollView = view.findViewById(R.id.nested_scroll_view);
        //mRecyclerLoader = view.findViewById(R.id.recycler_loader);

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null){
                    if ( scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight()) && scrollY > oldScrollY ){

                        switch (LAYOUT_SETTING_CURR){
                            case "list":
                                curr_recycler_visible_item_count = layoutManager_list.getChildCount();
                                first_visible_item_position = layoutManager_list.findFirstVisibleItemPosition();
                                break;
                            case "grid":
                                curr_recycler_visible_item_count = layoutManager_grid.getChildCount();
                                first_visible_item_position = layoutManager_grid.findFirstVisibleItemPosition();
                                break;
                            case "staggered":
                                curr_recycler_visible_item_count = staggeredGridLayoutManager.getChildCount();
                                int[] firstVisibleItems = null;
                                firstVisibleItems = staggeredGridLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
                                if(firstVisibleItems != null && firstVisibleItems.length > 0) {
                                    first_visible_item_position = firstVisibleItems[0];
                                }
                                break;
                            case "onelineList":
                                curr_recycler_visible_item_count = layoutManager_list.getChildCount();
                                first_visible_item_position = layoutManager_list.findFirstVisibleItemPosition();
                                break;
                        }

                        if ( (curr_recycler_visible_item_count + first_visible_item_position) < recycler_total_result_count && first_visible_item_position >= 0 ){
                            loadMoreVideos();
                        }
                    }
                }
            }
        });

        mSliderLayout = view.findViewById(R.id.slider);
        slider_viability_check();
        recycle_container = view.findViewById(R.id.recycle_container);

        //new Slider_async().execute("makeSlider");
//        Slider_async m_slider_async = new Slider_async();
//        m_curr_slider_async = m_slider_async;
//        m_curr_slider_async.m_fragment = this;
//        m_curr_slider_async.execute("makeSlider");
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        if (slider != null && checkActivityAlive()){
            String clicked_vid = slider.getBundle().get("vid").toString();
            String clicked_title = slider.getBundle().get("extra").toString();
            String clicked_url = slider.getBundle().get("full_url").toString();

            if (clicked_vid.equals("undefined")){
                if (clicked_url != null && !clicked_url.equals("")){
                    goToBrowser(clicked_url);
                }
            } else {
                gotoVideoPage(clicked_vid, clicked_title);
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //Log.d(TAG, "onPageScrolled: Scrolled to "+position);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStop() {
        recycler_next_page_token = "";
        mSliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recycler_next_page_token = "";
        ((MainActivity)getActivity()).clearBackStackInclusive(TAG);
        dismissProgressDialog();
        dissmiss_settingsChangeAlert();
        dissmiss_appRestartAlert();
        if (m_curr_slider_async != null){
            m_curr_slider_async.cancel(true);
        }
        if (getLoginStatus()){
            if (m_curr_fetch_video_list != null){
                m_curr_fetch_video_list.cancel(true);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("connection", "msg: " + connectionResult.getErrorMessage());
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(getActivity(), connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK){
                    makeAuthorizePermissionDialog();
                } else {
                    initSilentLogin();
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        get_current_settings();
        swipeRefreshLayout.setOnRefreshListener(this);

        /*COMPARING THE RECYCLER VIEW MAX LIMIT SETTINGS*/
        if (YOUTUBE_RECYCLER_MAX_LIMIT != null && recycler_results_per_page_count != null){
            if ( Integer.parseInt(YOUTUBE_RECYCLER_MAX_LIMIT) != recycler_results_per_page_count ){
                dissmiss_appRestartAlert();
                dissmiss_settingsChangeAlert();
//                alertforSettingChange("RECYCLER_MAX_LIMIT");
                makeAppRestartDialog();
            }
        }

        /*COMPARING YOUTUBE THUMBNAIL QUALITY SETTINGS*/
        if (YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY != null && YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY != null){
            if (!YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals(YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY)){
                dissmiss_appRestartAlert();
                dissmiss_settingsChangeAlert();
//                alertforSettingChange("THUMBNAIL_QUALITY");
                makeAppRestartDialog();
            }
        }

        /*COMPARING SLIDER HIDABILITY SETTINGS*/
        if (SLIDER_IS_CURR_HIDDEN != null && SLIDER_IS_PREV_HIDDEN != null){
            if (!SLIDER_IS_CURR_HIDDEN.equals(SLIDER_IS_PREV_HIDDEN)){
                dissmiss_appRestartAlert();
                dissmiss_settingsChangeAlert();
//                alertforSettingChange("SLIDER_STATE_CHANGED");
                makeAppRestartDialog();
            }
        }

        /*COMPARING LAYOUT CHANGE SETTTINGS*/
        if (LAYOUT_SETTING_CURR != null && LAYOUT_SETTING_PREV != null){
            if (!LAYOUT_SETTING_CURR.equals(LAYOUT_SETTING_PREV)){
                dissmiss_appRestartAlert();
                dissmiss_settingsChangeAlert();
                makeAppRestartDialog();
            }
        }
    }

    @Override
    public void onPause() {
        recycler_next_page_token = "";
        if (mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
        dissmiss_settingsChangeAlert();
        dissmiss_appRestartAlert();
        super.onPause();
    }

    @Override
    public void onRefresh() {
        updateUI();
    }


















    /*
    * ALL METHODS ....................................................................................
    * */
    private void initSilentLogin(){
        if (getLoginStatus()){
            if (MainActivity.googleApiClient != null){
                OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(MainActivity.googleApiClient);
                if (opr.isDone()){
                    GoogleSignInResult result = opr.get();
                    handleSignInResult(result);
                } else {
                    opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                        @Override
                        public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                            handleSignInResult(googleSignInResult);
                        }
                    });
                }
            } else {
                /*Mainactivity is dead. Re-initialize the Main Activity*/
            }
        } else {
            //Logged in without Google
            mProgressDialog.setMessage("Fetching data from YouTube server . . .");
            mProgressDialog.show();
            fetch_video_list_nologin();
        }
    }

    private Boolean checkActivityAlive(){
        Activity activity = getActivity();
        if (activity != null && isAdded()){
            return true;
        } return false;
    }

    private void handleSignInResult(GoogleSignInResult result){
        if (checkActivityAlive()){
            if (result.isSuccess()){
                GoogleSignInAccount account = result.getSignInAccount();
                if (account != null){
                    mAccount = account.getAccount();

                    credential = GoogleAccountCredential.usingOAuth2(getContext(), Collections.singleton( getString(R.string.YOUTUBE_SCOPE)) );
                    credential.setSelectedAccount(mAccount);

                    transport = AndroidHttp.newCompatibleTransport();
                    jasonfactory = JacksonFactory.getDefaultInstance();
                    mYoutube = new com.google.api.services.youtube.YouTube.Builder(transport, jasonfactory, credential)
                            .setApplicationName(getString(R.string.youtubeAppname))
                            .build();

                    fetch_video_list();
                } else {
                    initSilentLogin();
                }
            } else {
                initSilentLogin();
            }
        }
    }

    private void fetch_video_list(){
        Fetch_video_list fetch_video_list = new Fetch_video_list();
        m_curr_fetch_video_list = fetch_video_list;
        m_curr_fetch_video_list.dataNao(getContext(),
                getString(R.string.FAVOURITE_PLAYLIST_ID),
                YOUTUBE_RECYCLER_MAX_LIMIT,
                mYoutube,
                recycler_next_page_token,
                new FetchVideoEventListener<PlaylistItemListResponse>(){

                    @Override
                    public void onSuccess(PlaylistItemListResponse object) {
                        if (isVisible()){
                            hideProgress(mRecyclerProgressbar);
                            gather_data_recycle(object);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        if (isVisible()){
                            hideProgress(mRecyclerProgressbar);
                        }
                    }

                    @Override
                    public void onFetchingStart() {
                        if (isVisible()){
                            showProgress(mRecyclerProgressbar);
                        }
                    }
                });
        m_curr_fetch_video_list.execute();
    }

    public void showProgress(ProgressBar node){
        node.setVisibility(View.VISIBLE);
        node.showContextMenu();
    }

    public void hideProgress(ProgressBar node){
        node.setVisibility(View.GONE);
    }

    public void add_slide_into_view(){
        for (int i = 0; i < slides.size(); i++) {
            if (isVisible()){
                slides.get(i).setOnSliderClickListener(this);
                mSliderLayout.addSlider(slides.get(i));
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                mSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                mSliderLayout.setCustomAnimation(new DescriptionAnimation());
                mSliderLayout.setDuration(4000);
                mSliderLayout.addOnPageChangeListener(this);
            } else {
                break;
            }
        }
    }

    public void remove_all_slides_from_view(){
        mSliderLayout.removeAllSliders();
        slides.clear();
    }

    private void gather_data_recycle(PlaylistItemListResponse response){
        if (!isLoadingMoreVideos){
            clearList(this.mVideoId);
            clearList(this.mTitle);
            clearList(this.mThumbnail);
            clearList(this.mDescription);
            clearList(this.mDuration);
        }

        recycler_total_result_count = response.getPageInfo().getTotalResults();
        recycler_results_per_page_count = response.getPageInfo().getResultsPerPage();
        recycler_next_page_token = response.getNextPageToken();

        for (int i = 0; i < response.getItems().size(); i++) {
            this.mVideoId.add( response.getItems().get(i).getContentDetails().getVideoId() );
            this.mTitle.add( response.getItems().get(i).getSnippet().getTitle() );
            this.mDescription.add( response.getItems().get(i).getSnippet().getDescription() );

            String vThumbnail = getString(R.string.youtubeThumbnailDefaultRes);
            switch (YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY){
                case "default":
                    vThumbnail = response.getItems().get(i).getSnippet().getThumbnails().getDefault().getUrl();
                    break;
                case "medium":
                    vThumbnail = response.getItems().get(i).getSnippet().getThumbnails().getMedium().getUrl();
                    break;
                case "high":
                    vThumbnail = response.getItems().get(i).getSnippet().getThumbnails().getHigh().getUrl();
                    break;
                case "standard":
                    vThumbnail = response.getItems().get(i).getSnippet().getThumbnails().getStandard().getUrl();
                    break;
                case "maxres":
                    vThumbnail = response.getItems().get(i).getSnippet().getThumbnails().getMaxres().getUrl();
                    break;
            }
            this.mThumbnail.add( vThumbnail );

        }

        initRecyclerView();
    }

    private void clearList(ArrayList list){
        list.clear();
    }

    private void initRecyclerView(){
        hideSwipeLoading();

        recyclerView = mView.findViewById(R.id.recyclerView_favourite);
        recycler_adapter = new Recycler_adapter(LAYOUT_SETTING_CURR, mView.getContext(), this.mVideoId, this.mThumbnail, this.mTitle, this.mDescription, this.mDuration);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(recycler_adapter);

        /*LAYOUT MANAGER IS THE SWITCH TO SWITCH BETWEEN LINEAR LAYOUT MANAGER AND GRID LAYOUT MANAGER*/
        switch (LAYOUT_SETTING_CURR){
            case "list":
                recyclerView.setLayoutManager(layoutManager_list);
                break;
            case "grid":
                recyclerView.setLayoutManager(layoutManager_grid);
                break;
            case "staggered":
                recyclerView.setLayoutManager(staggeredGridLayoutManager);
                break;
            case "onelineList":
                recyclerView.setLayoutManager(layoutManager_list);
                break;
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        if (isLoadingMoreVideos){
            recycler_adapter.notifyItemInserted(mVideoId.size() - 1);
            isLoadingMoreVideos = false;
        }
    }

    private void loadMoreVideos(){
        isLoadingMoreVideos = true;
        if (getLoginStatus()){
            fetch_video_list();
        } else {
            fetch_video_list_nologin();
        }
        //mRecyclerLoader.setVisibility(View.VISIBLE);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void gotoVideoPage(String vid, String title){
        Intent videoIntent = new Intent(getContext(), Video_play.class);
        videoIntent.putExtra(getContext().getString(R.string.passvid), vid);
        videoIntent.putExtra(getContext().getString(R.string.passTitle), title);
        getContext().startActivity(videoIntent);
    }

    private void goToBrowser(String uri){
        if (uri != null){
            Uri ad_uri = Uri.parse(uri);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, ad_uri);

            PackageManager packageManager = getContext().getPackageManager();
            List<ResolveInfo> activities = packageManager.queryIntentActivities(browserIntent, PackageManager.MATCH_DEFAULT_ONLY);
            boolean isIntentSafe = activities.size() > 0;

            if (isIntentSafe){
                String intentTitle = getString(R.string.browserChooserText);
                Intent chooser = Intent.createChooser(browserIntent, intentTitle);
                startActivity(chooser);
            } else {
                //THERE IS NOT ANY APP INSTALLED IN YOUR PHONE TO OPEN THIS LINK
                Toast.makeText(getContext(), getString(R.string.noBrowserText), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void makeAuthorizePermissionDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Your Permission Required");
        alertDialog.setMessage( getString(R.string.requestPermisssion) );
        alertDialog.setButton("Okay.. Bring it ..", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fetch_video_list();
            }
        });
        alertDialog.show();
    }



    private Boolean getLoginStatus(){
        return sharedPreferences.getBoolean(getString(R.string.loginStatus), true);
    }

    private void dismissProgressDialog(){
        if (mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    private void fetch_video_list_nologin(){
        String uri = getString(R.string.youtube_base_address)+
                "/"+getString(R.string.yt_playlistItem)+
                "?part=snippet%2CcontentDetails&maxResults="+
                YOUTUBE_RECYCLER_MAX_LIMIT+
                "&playlistId="+getString(R.string.FAVOURITE_PLAYLIST_ID)+
                "&pageToken="+recycler_next_page_token+
                "&key="+getString(R.string.YOUTUBE_API_KEY_WEB);

        RequestQueue yt_queue = Volley.newRequestQueue(getContext());
        JsonObjectRequest yt_pl_item_request = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null){
                    try {
                        dismissProgressDialog();
                        gather_data_recycle_no_login(response);
                    } catch (JSONException e) {
                        dismissProgressDialog();
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
            }
        });
        yt_queue.add(yt_pl_item_request);
    }

    private void gather_data_recycle_no_login(JSONObject response) throws JSONException {
        if (!isLoadingMoreVideos){
            clearList(this.mVideoId);
            clearList(this.mTitle);
            clearList(this.mThumbnail);
            clearList(this.mDescription);
            clearList(this.mDuration);
        } else {

        }

        recycler_total_result_count = response.getJSONObject("pageInfo").getInt("totalResults");
        recycler_results_per_page_count = response.getJSONObject("pageInfo").getInt("resultsPerPage");
        if (!response.isNull("nextPageToken")){
            recycler_next_page_token = response.getString("nextPageToken");
        }

        JSONArray jsonArray = response.getJSONArray("items");
        if (jsonArray != null){
            for (int i = 0; i < jsonArray.length(); i++) {
                this.mVideoId.add( jsonArray.getJSONObject(i).getJSONObject("contentDetails").getString("videoId") );
                this.mTitle.add( jsonArray.getJSONObject(i).getJSONObject("snippet").getString("title") );
                this.mDescription.add( jsonArray.getJSONObject(i).getJSONObject("snippet").getString("description") );

                if(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals("maxres")){
                    this.mThumbnail.add( jsonArray.getJSONObject(i).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url") );
                } else {
                    this.mThumbnail.add( jsonArray.getJSONObject(i).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY).getString("url") );
                }
            }
        }

        dismissProgressDialog();
        initRecyclerView();
    }

    private void updateUI(){
        recycler_next_page_token = "";

        slider_viability_check();

        if (getLoginStatus()){
            if (MainActivity.googleApiClient != null){
                if (mYoutube != null){
                    fetch_video_list();
                } else {
                    hideSwipeLoading();
                    initSilentLogin();
                }
            } else {
                /*Mainactivity is dead. Re-initialize the Main Activity*/
                hideSwipeLoading();
                return;
            }
        } else {
            hideSwipeLoading();
            fetch_video_list_nologin();
        }

        get_previous_settings();
        settings_equalizer();
    }

    private void hideSwipeLoading(){
        if (swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void get_current_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_RECYCLER_MAX_LIMIT = sharedPreferences_settings.getString(getString(R.string.pref_youtubePageSize_key), getString(R.string.All_upload_max_result));
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            SLIDER_IS_CURR_HIDDEN = sharedPreferences_settings.getBoolean(getString(R.string.sliderHideKey), false);
            LAYOUT_SETTING_CURR = sharedPreferences_settings.getString(getString(R.string.recycler_layout_key), "list");
        } else {
            YOUTUBE_RECYCLER_MAX_LIMIT = getString(R.string.All_upload_max_result);
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            SLIDER_IS_CURR_HIDDEN = false;
            LAYOUT_SETTING_CURR = "list";
        }
    }

    private void get_previous_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_RECYCLER_MAX_LIMIT = sharedPreferences_settings.getString(getString(R.string.pref_youtubePageSize_key), getString(R.string.All_upload_max_result));
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            SLIDER_IS_PREV_HIDDEN = sharedPreferences_settings.getBoolean(getString(R.string.sliderHideKey), false);
            LAYOUT_SETTING_PREV = sharedPreferences_settings.getString(getString(R.string.recycler_layout_key), "list");
        } else {
            YOUTUBE_RECYCLER_MAX_LIMIT = getString(R.string.All_upload_max_result);
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            SLIDER_IS_PREV_HIDDEN = false;
            LAYOUT_SETTING_PREV = "list";
        }
    }

    private void settings_equalizer(){
        YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY;
        SLIDER_IS_CURR_HIDDEN = SLIDER_IS_PREV_HIDDEN;
        LAYOUT_SETTING_CURR = LAYOUT_SETTING_PREV;
    }

    private void slider_viability_check(){
        if (SLIDER_IS_CURR_HIDDEN){
            mSliderLayout.setVisibility(View.GONE);
        } else {
            mSliderLayout.setVisibility(View.VISIBLE);

            Slider_async m_slider_async = new Slider_async();
            m_curr_slider_async = m_slider_async;
            m_curr_slider_async.m_fragment = this;
            m_curr_slider_async.execute("makeSlider");
        }
    }

    private void makeAppRestartDialog(){
        if (getUserVisibleHint()){
            AlertDialog.Builder nonCancelableDialog = new AlertDialog.Builder(getContext());
            //nonCancelableDialog.setCancelable(false);
            nonCancelableDialog.setTitle(getString(R.string.restartAppdialogTitle));
            nonCancelableDialog.setMessage(getString(R.string.restartAppdialogMessage));
            nonCancelableDialog.setPositiveButton("Restart Application", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Restart the app
                    Intent restartIntent = Objects.requireNonNull(getContext()).getPackageManager().getLaunchIntentForPackage( getContext().getPackageName() );
                    assert restartIntent != null;
                    restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(restartIntent);
                }
            });
            nonCancelableDialog.setNegativeButton("Restart Later", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Dissmiss the dialog
                    dismissProgressDialog();
                }
            });

            appRestartDialog = nonCancelableDialog.create();
            appRestartDialog.show();
        }

    }

    private void alertforSettingChange(String setting){
        if (getUserVisibleHint()){
            AlertDialog.Builder settingsChangeAlert = new AlertDialog.Builder(getContext());
            settingsChangeAlert.setTitle( getString(R.string.settingsChangeAlertTitle) );
            switch (setting){
                case "RECYCLER_MAX_LIMIT":
                    settingsChangeAlert.setMessage("Video List Max Limit settings changed. "+getString(R.string.settingsChangeAlertMsg));
                    break;
                case "THUMBNAIL_QUALITY":
                    settingsChangeAlert.setMessage("Thumbnail Quality settings changed. "+getString(R.string.settingsChangeAlertMsg));
                    break;
                case "SLIDER_STATE_CHANGED":
                    settingsChangeAlert.setMessage("Slider visibility settings changed. "+getString(R.string.settingsChangeAlertMsg));
                    break;
            }
            settingsChangeAlert.setPositiveButton("Refresh Application", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    updateUI();
                }
            });
            settingsChangeAlert.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            settingsChangedialog = settingsChangeAlert.create();
            settingsChangedialog.show();
        }
    }

    private void dissmiss_settingsChangeAlert(){
        if (settingsChangedialog != null){
            if (settingsChangedialog.isShowing()){
                settingsChangedialog.cancel();
            }
        }
    }

    private void dissmiss_appRestartAlert(){
        if (appRestartDialog != null){
            if (appRestartDialog.isShowing()){
                appRestartDialog.cancel();
            }
        }
    }



















    /*
    * ALL CLASSES .................................................................................
    * */
    private static class Slider_async extends AsyncTask<String, Void, String>{
        Tab_favourite m_fragment = null;
        private Slider_adapter slider_adapter;
        private ProgressDialog dialog;

        private HashMap<String,String> ids = new HashMap<String, String>();
        private HashMap<String,String> urls = new HashMap<String, String>();
        private HashMap<String,String> full_urls = new HashMap<String, String>();
        private HashMap<String,String> files = new HashMap<String, String>();

        private Boolean checkforActivity(){
            Activity activity = m_fragment.getActivity();
            if (activity != null && m_fragment.isAdded()){
                return true;
            } return false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (checkforActivity()){
                if (!isCancelled()){
                    m_fragment.mSliderPlaceholder.setVisibility(View.VISIBLE);
                    slides.clear();
                }
            }
        }

        private void parseSliderData(JSONArray data){
            for (int i = 0; i < data.length(); i++) {
                try {
                    String slide_video_url = "";
                    JSONObject obj = data.getJSONObject(i);
                    String slide_id = obj.getString("id");
                    String slide_title = obj.getString("title");
                    if (!obj.isNull("full_url")){
                        slide_video_url = obj.getString("full_url");
                    } else {
                        slide_video_url = "";
                    }
                    String slide_video_id = obj.getString("url");
                    String slide_name = m_fragment.getString(R.string.host_url_slider_image)+obj.getString("image");

                    this.ids.put(slide_title, slide_id);
                    this.full_urls.put(slide_title, slide_video_url);
                    this.urls.put(slide_title, slide_video_id);
                    this.files.put(slide_title, slide_name);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (isCancelled()){
                    break;
                }
            }
        }

        private TextSliderView textSliderView;
        private void start_slider(){
            this.slider_adapter = new Slider_adapter();
            m_fragment.remove_all_slides_from_view();

            for(String name : this.files.keySet()){
                if (isCancelled()){
                    break;
                }

                textSliderView = new TextSliderView(mView.getContext());
                textSliderView.description(name)
                        .image(this.files.get(name))
                        .setScaleType(BaseSliderView.ScaleType.Fit);

                textSliderView.bundle(new Bundle());
                textSliderView.getBundle().putString("extra",name);
                textSliderView.getBundle().putString("full_url", this.full_urls.get(name));
                textSliderView.getBundle().putString("vid", this.urls.get(name));
                slides.add(textSliderView);
            }

            if (!isCancelled()){
                m_fragment.add_slide_into_view();
            } else {
                m_fragment.dismissProgressDialog();
                return;
            }
        }

        private void fetchSliderDataPHP(){
            String uri = m_fragment.getString(R.string.host_url)+"/"+m_fragment.getString(R.string.slider_api)+"/"+m_fragment.getString(R.string.favourite);
            RequestQueue queue = Volley.newRequestQueue(m_fragment.getContext());
            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {

                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(JSONObject response) {
                    if (checkforActivity()){
                        try {
                            if (response.getBoolean("status")){
                                if (!isCancelled()){
                                    parseSliderData(response.getJSONArray("sliders"));
                                    start_slider();
                                } else {
                                    return;
                                }

                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (!isCancelled()){
                                if (!isCancelled()){
                                    Toast.makeText(m_fragment.getContext(), m_fragment.getString(R.string.phpservererrorMsg), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (checkforActivity()){
                        Toast.makeText(m_fragment.getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            queue.add(postRequest);
        }

        @Override
        protected String doInBackground(String... strings) {
            String str = strings[0];
            if (checkforActivity()){
                if (!isCancelled()){
                    fetchSliderDataPHP();
                } else {
                    return null;
                }
            }
            return str;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
            m_fragment.hideSwipeLoading();
            if (checkforActivity()){
                m_fragment.mSliderPlaceholder.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            m_fragment.hideSwipeLoading();
            if (s != null){
                if (checkforActivity()){
                    m_fragment.mSliderPlaceholder.setVisibility(View.GONE);
                    //add_slide_into_view();
                }
            }
        }
    }
}
