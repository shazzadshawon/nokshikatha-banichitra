package biz.agvcorp.banichitra.database_classes;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import biz.agvcorp.banichitra.R;

@Database(entities = {Video.class, App_notification.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase{

    private static AppDatabase INSTANCE;

    public abstract DatabaseAccessInterface appDatabaseobject();

    public static AppDatabase getAppDatabse(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, context.getString(R.string.DATABASE_NAME))
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return INSTANCE;
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }
}
