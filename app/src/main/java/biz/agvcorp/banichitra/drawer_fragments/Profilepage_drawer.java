package biz.agvcorp.banichitra.drawer_fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.elyeproj.loaderviewlibrary.LoaderImageView;
import com.elyeproj.loaderviewlibrary.LoaderTextView;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import biz.agvcorp.banichitra.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class Profilepage_drawer extends Fragment implements ViewPagerEx.OnPageChangeListener {
    private static final String TAG = "Profilepage_drawer";

    private View mView;

    private LoaderImageView profile_cover_image;
    private CircleImageView profile_profile_image;
    private LoaderTextView profile_name, profile_email_address, profile_fav_count, profile_history_count, profile_schedule_count;

    private Toolbar toolbar;
    private ProgressDialog mProgress;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public Profilepage_drawer() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.drawer_profilepage, container, false);
        mProgress = new ProgressDialog(getContext());

        sharedPreferences = getContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        profile_cover_image = mView.findViewById(R.id.profile_cover_photo);
        profile_profile_image = mView.findViewById(R.id.profile_profile_photo);
        profile_name = mView.findViewById(R.id.profile_name);
        profile_email_address = mView.findViewById(R.id.profile_email_address);
        profile_fav_count = mView.findViewById(R.id.profile_fav_count);
        profile_history_count = mView.findViewById(R.id.profile_history_count);
        profile_schedule_count = mView.findViewById(R.id.profile_scheduled_count);

        //showProgress("Fetching Your Profile . . .");
        fetch_analytics_php();

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar = view.findViewById(R.id.sceondery_toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }







    private String getCoverPhoto(){
        return sharedPreferences.getString(getString(R.string.coverphoto), "");
    }
    private String getProfilePic(){
        return sharedPreferences.getString(getString(R.string.setProfileImageurl), "");
    }
    private String getUserName(){
        return sharedPreferences.getString(getString(R.string.setUserNaem), "");
    }
    private String getUserEmail(){
        return sharedPreferences.getString(getString(R.string.setUserEmail), "");
    }
    public int getRowId(){
        return sharedPreferences.getInt(getString(R.string.rowid), 0);
    }

    private void showProgress(String msg){
        mProgress.setMessage(msg);
        mProgress.show();
    }
    private void hideProgress(){
        mProgress.setMessage("");
        mProgress.hide();
    }

    private void populateData(JSONObject response) throws JSONException {
        String prof_img_url = getProfilePic();
        String prof_cover_url = getCoverPhoto();
        if (!response.isNull("history_count") && !response.isNull("favourite_count") && !response.isNull("watch_later_count") && prof_cover_url != null && prof_img_url != null ){
            Glide.with(getContext())
                    .asBitmap()
                    .load(prof_cover_url)
                    .into(profile_cover_image);
            Glide.with(getContext())
                    .asBitmap()
                    .load(prof_img_url)
                    .into(profile_profile_image);
            profile_name.setText(getUserName());
            profile_email_address.setText(getUserEmail());
            profile_fav_count.setText(response.getString("favourite_count"));
            profile_history_count.setText(response.getString("history_count"));
            profile_schedule_count.setText(response.getString("watch_later_count"));
        }
    }

    private void fetch_analytics_php(){
        String uri = getString(R.string.host_url)+"/"+getString(R.string.fetch_analytics)+"/"+getRowId();
        RequestQueue queue = Volley.newRequestQueue(getContext());
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                //hideProgress();
                try {
                    if (response.getBoolean("status")){
                        populateData(response);
                    } else {
                        Toast.makeText(getContext(), "Data not Came ..", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //hideProgress();
            }
        });
        queue.add(postRequest);
    }
}
