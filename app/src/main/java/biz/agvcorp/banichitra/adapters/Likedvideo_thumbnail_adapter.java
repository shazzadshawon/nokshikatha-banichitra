package biz.agvcorp.banichitra.adapters;

import java.util.ArrayList;
import java.util.Collections;

public class Likedvideo_thumbnail_adapter {

    private ArrayList<String> thumbs = new ArrayList<>();

    public Likedvideo_thumbnail_adapter(){

    }

    public Likedvideo_thumbnail_adapter(String parent){
        switch (parent){
            case "allupload":
                list_maker();
                break;
            case "recentupload":
                list_maker();
                break;
            case "favourite":
                list_maker();
                break;
            case "topview":
                list_maker();
                break;
            default:
                list_maker();
                break;
        }
    }

    private void list_maker(){
        this.thumbs.add("https://i.ytimg.com/vi/ymLwqYv9_6E/maxresdefault.jpg");
        this.thumbs.add("https://images.fandango.com/ImageRenderer/0/0/redesign/static/img/default_poster.png/0/images/masterrepository/other/hos-best-horror-posters-intro.jpg");
        this.thumbs.add("http://top10tale.com/wp-content/uploads/2017/08/september-2017-bollywood-movies-release.jpg");
        this.thumbs.add("https://www.hdwallpapers.in/thumbs/2016/x_men_apocalypse_banner_poster-t2.jpg");
        this.thumbs.add("https://areyoujustwatching.com/wp-content/uploads//2015/10/WarRoom-2015-movie-poster.jpg");
        this.thumbs.add("https://images.wallpapersden.com/image/download/dunkirk-movie-poster_57817_1280x720.jpg");
        this.thumbs.add("https://www.desktopbackground.org/download/1280x720/2014/01/19/703459_wallpapers-thor-movie-poster-backgrounds-1920x1080_1920x1080_h.jpg");
        this.thumbs.add("http://www.indianbollywoodcinema.com/wp-content/uploads/2017/11/Wazir-Movie-Poster.jpg");
    }

    private void list_randomizer(){
        Collections.shuffle(this.thumbs);
    }

    public ArrayList get_list(){
        list_randomizer();
        return this.thumbs;
    }
}
