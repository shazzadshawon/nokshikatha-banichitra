package biz.agvcorp.banichitra;

/**
 * Created By - Shazzadur Rahaman on 8/29/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */
import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.elyeproj.loaderviewlibrary.LoaderImageView;
import com.elyeproj.loaderviewlibrary.LoaderTextView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.Comment;
import com.google.api.services.youtube.model.CommentSnippet;
import com.google.api.services.youtube.model.CommentThread;
import com.google.api.services.youtube.model.CommentThreadListResponse;
import com.google.api.services.youtube.model.CommentThreadSnippet;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoGetRatingResponse;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.joda.time.Instant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import biz.agvcorp.banichitra.adapters.Related_channelName_adapter;
import biz.agvcorp.banichitra.adapters.Related_thumbnail_adapter;
import biz.agvcorp.banichitra.adapters.Related_videoView_adapter;
import biz.agvcorp.banichitra.adapters.Related_videotitle_adapter;
import biz.agvcorp.banichitra.asset_library.Player_config;
import biz.agvcorp.banichitra.drawer_fragments.Profilepage_drawer;
import biz.agvcorp.banichitra.drawer_fragments.Settings_drawer;
import biz.agvcorp.banichitra.recycler_adapters.Recycler_adapter_comment;
import biz.agvcorp.banichitra.recycler_adapters.Recycler_adapter_related;
import de.hdodenhof.circleimageview.CircleImageView;
import pub.devrel.easypermissions.EasyPermissions;

public class Video_play extends AppCompatActivity implements
        YouTubePlayer.OnInitializedListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        Settings_drawer.OnFragmentInteractionListener,
        SwipeRefreshLayout.OnRefreshListener,
        EasyPermissions.PermissionCallbacks{

    private static final String TAG = "Video_play";

    final int RC_API_CHECK = 100;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    private DateTime publishdate;
    private BigInteger viewCount, likeCount, dislikeCount, favoriteCount, commentCount;
    private String viewCount_nologin, likeCount_nologin, dislikeCount_nologin, favoriteCount_nologin, commentCount_nologin;
    private String vid, description, title, channelId, channelTitle, duration, thumbnail, publishDate_nologin;
    private Toolbar toolbar;
    private CoordinatorLayout activity_main;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NestedScrollView mNestedScrollView;

    private YouTubePlayer mYouTubeplayer;
    private Boolean mYtwasRestored;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;
    public static Account mAccount;
    private GoogleAccountCredential credential, credentialYT;
    private com.google.api.services.youtube.YouTube mYoutube = null;
    private com.google.api.services.youtube.YouTube mYoutube_comment = null;
    private HttpTransport transport;
    private JacksonFactory jasonfactory;
    //private static final String YOUTUBE_SCOPE = "https://www.googleapis.com/auth/youtube";
    //private static final Collection<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/youtube", "https://www.googleapis.com/auth/youtube.force-ssl", "https://www.googleapis.com/auth/youtubepartner");
    private static final String[] SCOPES = { YouTubeScopes.YOUTUBE };
    private static final String[] YOUTUBE_SCOPE = { YouTubeScopes.YOUTUBE_FORCE_SSL, YouTubeScopes.YOUTUBE };

    private Button mVideo_like_btn, mVideo_disLike_btn, mVideo_share_btn;
    private ToggleButton mVideo_watch_later_btn, mVideo_favourite_btn;
    private LoaderTextView mVideoTitle, mVideoViewCount, mChannelname, mVideoPublishDate, mVideoDescription, mVideoCommentCount, mVideoShareCount;
    private LoaderImageView mVideoChannelImage;
    private TextView mVideoLikeCount, mVideoDisLikeCount, mVideoCommentCountCurr;

    private RecyclerView recyclerView, commentrecyclerView;
    private Recycler_adapter_related recycler_adapter;
    private Recycler_adapter_comment comment_recycler_adapter;
    private RelativeLayout recycle_container;
    private Recycler_adapter_comment.CommentRElatedCallback commentRecyclerCallbacks;

    private Related_thumbnail_adapter thumb_adapter;
    private Related_videotitle_adapter videoTitle_adapter;
    private Related_channelName_adapter channelName_adapter;
    private Related_videoView_adapter videoView_adapter;

    private ArrayList<String> mVideo = new ArrayList<>();
    private ArrayList<String> mThumbnail = new ArrayList<>();
    private ArrayList<String> mTitle = new ArrayList<>();
    private ArrayList<String> mChannel = new ArrayList<>();
    private ArrayList<String> mDescripies = new ArrayList<>();

    private YouTubePlayerView youTubePlayerView;
    private Button button;

    private SharedPreferences sharedPreferences, sharedPreferences_settings;
    private SharedPreferences.Editor editor;

    private Boolean isThisVideoRated = false, isVideo_wl = false, isVideo_fav = false, isThisVideoLiked = false, isThisVideoDisliked = false;
    private ProgressDialog mProgress;
    private ProgressBar mCommentProgressbar;

    private ArrayList<String> mCommVideoId = new ArrayList<>();
    private ArrayList<String> mMyCommCommentId = new ArrayList<>();
    private ArrayList<String> mCommentId = new ArrayList<>();
    private ArrayList<String> mCommAuthorName = new ArrayList<>();
    private ArrayList<String> mCommAuthorChannelUrl = new ArrayList<>();
    private ArrayList<String> mCommAuthorProfileImage = new ArrayList<>();
    private ArrayList<String> mCommText = new ArrayList<>();
    private ArrayList<String> mCommRatting = new ArrayList<>();
    private ArrayList<String> mCommLikeCount = new ArrayList<>();
    private ArrayList<String> mCommDateTime = new ArrayList<String>();
    private ArrayList<Boolean> mCommCanRate = new ArrayList<>();
    private String CURR_TOP_LEVEL_COMMENT_TEXT = "";
    private String CURR_TOP_LEVEL_COMMENT_EDITED_TEXT = "";
    private String CURR_TOP_LEVEL_EDITED_COMMENT_ID = "";
    private int CURR_TOP_LEVEL_COMMENT_EDIT_POSITION;

    private CircleImageView mCircleImageComment;
    private EditText mCommentText;
    private LinearLayout mComment_update_section;
    private Button mComment_inserter_btn, mComment_update_btn, mComment_update_cancel_btn;
    private TextView m_firstCommenter;

    private ProcessCommentResponse curr_process_comment_response = null;
    private Publish_public_comment curr_publis_public_comment = null;
    private Fetch_video_ratting_Count curr_fetch_video_rating_count = null;
    private Fetch_video_info curr_fetch_video_info = null;
    private Fetch_related_vides curr_fetch_related_videos = null;
    private PerformRateOperations curr_perform_rate_operation = null;
    private CheckRatingStatus curr_check_rating_status = null;
    private Fetch_comments curr_fetch_comments = null;
    private Fetch_my_comments curr_fetch_my_comments = null;
    private Edit_comment curr_edit_comments = null;
    private Delete_comments curr_delete_comment = null;
    private Mark_Spam_comments curr_mark_As_spam = null;

    private Boolean COMMENT_FETCH_PERMISSION_FLAG = false;

    private AlertDialog show_login_required_alertDialog;
    private AlertDialog net_connection_alertDialog;
    private AlertDialog invalid_id_alertDialog;
    private AlertDialog comment_without_login, goolePlusRequired;
    private LoginRequiredListener loginRequiredListener;
    private NetConnectListener netConnectListener;
    private InvalidVIDListener invalidVIDListener;

    private Integer recycler_total_result_count, recycler_results_per_page_count;
    private Integer recycler_total_result_count_comment, recycler_results_per_page_count_comment, curr_recycler_visible_item_count_comment, first_visible_item_position_comment;
    private String recycler_next_page_token_comment = "";
    private String YOUTUBE_RECYCLER_MAX_LIMIT, YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY, YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY, LAYOUT_SETTING_PREV, LAYOUT_SETTING_CURR;
    private LinearLayoutManager layoutManager_list, layoutManager_comment, layoutManager_horizontal;
    private GridLayoutManager layoutManager_grid;

    private Boolean isLoadingMoreComments = false, isNewCommentLoaded = false, isEditCommentLoaded = false, sourceIntentflag = false;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        show_login_required_alertDialog = new AlertDialog.Builder(Video_play.this).create();
        net_connection_alertDialog = new AlertDialog.Builder(Video_play.this).create();
        invalid_id_alertDialog = new AlertDialog.Builder(Video_play.this).create();
        comment_without_login = new AlertDialog.Builder(Video_play.this).create();
        goolePlusRequired = new AlertDialog.Builder(Video_play.this).create();
        loginRequiredListener = new LoginRequiredListener();
        netConnectListener = new NetConnectListener();
        invalidVIDListener = new InvalidVIDListener();

        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(Video_play.this);
        //swipeRefreshLayout.setColorSchemeColors(getColor(R.color.colorPrimaryDark), getColor(R.color.colorAccent), getColor(R.color.facebook_deep_blue), getColor(R.color.textBlack));

        sharedPreferences = this.getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        sharedPreferences_settings = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(Video_play.this));
        editor = sharedPreferences.edit();

        get_previous_settings();
        settings_equalizer();

        if (LAYOUT_SETTING_CURR.equals("list")){
            layoutManager_list = new LinearLayoutManager(Video_play.this);
        } else if (LAYOUT_SETTING_CURR.equals("grid")){
            layoutManager_grid = new GridLayoutManager(Video_play.this, 2);
        } else if (LAYOUT_SETTING_CURR.equals("gridHorizontal")){
            layoutManager_horizontal = new LinearLayoutManager(Video_play.this, LinearLayoutManager.HORIZONTAL, false);
        }
        layoutManager_comment = new LinearLayoutManager(Video_play.this);

        mProgress = new ProgressDialog(this);
        activity_main = findViewById(R.id.mainContainer);
        mCommentProgressbar = findViewById(R.id.commentLoadingbar);

        mNestedScrollView = findViewById(R.id.videoPlay_nested_scroll_view);

        mVideo_like_btn = findViewById(R.id.video_like);
        mVideo_disLike_btn = findViewById(R.id.video_dislike);
        mVideo_share_btn = findViewById(R.id.video_share);
        mVideo_watch_later_btn = findViewById(R.id.video_watchLater);
        mVideo_favourite_btn = findViewById(R.id.video_favourite);

        mVideoTitle = findViewById(R.id.video_title);
        mVideoViewCount = findViewById(R.id.video_view_count);
        mVideoLikeCount = findViewById(R.id.video_like_count);
        mVideoDisLikeCount = findViewById(R.id.video_dislike_count);
        mVideoShareCount = findViewById(R.id.video_share_count);
        mChannelname = findViewById(R.id.channel_name);
        mVideoPublishDate = findViewById(R.id.video_publish_date);
        mVideoDescription = findViewById(R.id.videoDescription);
        mVideoChannelImage = findViewById(R.id.video_channel_image);
        mVideoCommentCount = findViewById(R.id.video_comment_count);
        mVideoCommentCountCurr = findViewById(R.id.video_comment_current);

        mCircleImageComment = findViewById(R.id.comment_insert_image);
        mCommentText = findViewById(R.id.comment_insert_text);
        mComment_update_section = findViewById(R.id.comment_update_section);
        mComment_inserter_btn = findViewById(R.id.comment_publish_btn);
        mComment_inserter_btn.setEnabled(false);
        mComment_update_btn = findViewById(R.id.comment_update_btn);
        mComment_update_cancel_btn = findViewById(R.id.comment_update_cancel_btn);
        m_firstCommenter = findViewById(R.id.bethefirstcommenter);

        mCommentText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (mComment_inserter_btn.getVisibility() == View.VISIBLE){
                    mComment_inserter_btn.setEnabled(false);
                } else {
                    mComment_update_section.setVisibility(View.GONE);
                    mComment_update_btn.setEnabled(false);
                    mComment_update_cancel_btn.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mComment_inserter_btn.getVisibility() == View.VISIBLE){
                    mComment_inserter_btn.setEnabled(true);
                } else {
                    mComment_update_section.setVisibility(View.VISIBLE);
                    mComment_update_btn.setEnabled(true);
                    mComment_update_cancel_btn.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mCommentText.getText().toString().equals("")) {
                    if (mComment_inserter_btn.getVisibility() == View.VISIBLE){
                        mComment_inserter_btn.setEnabled(false);
                    } else {
                        mComment_update_section.setVisibility(View.GONE);
                        mComment_update_btn.setEnabled(false);
                        mComment_update_cancel_btn.setEnabled(false);
                    }
                } else {
                    if (mComment_inserter_btn.getVisibility() == View.VISIBLE){
                        mComment_inserter_btn.setEnabled(true);
                    } else {
                        mComment_update_section.setVisibility(View.VISIBLE);
                        mComment_update_btn.setEnabled(true);
                        mComment_update_cancel_btn.setEnabled(true);
                    }
                }
            }
        });

        getIntentExtras();

        if (isNetworkAvailable()) {
            if (getLoginStatus()) {
                gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestServerAuthCode(getString(R.string.PeopleApiClientId))
                        .requestEmail()
                        .requestProfile()
                        .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.PLUS_ME))
                        .build();

                googleApiClient = new GoogleApiClient.Builder(this)
                        .enableAutoManage(this, this)
                        .addOnConnectionFailedListener(this)
                        .addConnectionCallbacks(this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();

                firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user != null) {

                        } else {
                            //logoutFromGoogle();
                            goLogInScreen();
                        }
                    }
                };
                firebaseAuth.addAuthStateListener(firebaseAuthListener);

            } else {

            }
        } else {
            makeNoInternetAlert();
        }

        if (getLoginStatus()) {
            mVideo_watch_later_btn.setEnabled(true);
            mVideo_favourite_btn.setEnabled(true);
        } else {
            mVideo_watch_later_btn.setEnabled(false);
            mVideo_favourite_btn.setEnabled(false);
        }

        mVideo_like_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getLoginStatus()) {
                    if (!isThisVideoLiked){

                        PerformRateOperations performRateOperations = new PerformRateOperations(vid, "like");
                        curr_perform_rate_operation = performRateOperations;
                        curr_perform_rate_operation.m_fragment = Video_play.this;
                        curr_perform_rate_operation.execute();
                    } else {
                        getSnackbar(getString(R.string.alreadyLikeRemoveMsg), Snackbar.LENGTH_LONG).show();

                        PerformRateOperations performRateOperations = new PerformRateOperations(vid, "none");
                        curr_perform_rate_operation = performRateOperations;
                        curr_perform_rate_operation.m_fragment = Video_play.this;
                        curr_perform_rate_operation.execute();

                        isThisVideoLiked = false;
                        updateUI_like(false);
                    }
                } else {
                    showLoginRequiredDialog();
                }
            }
        });
//        mVideo_like_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (getLoginStatus()){
//
//                }
//            }
//        });

        mVideo_disLike_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getLoginStatus()) {
                    if (!isThisVideoDisliked){

                        PerformRateOperations performRateOperations = new PerformRateOperations(vid, "dislike");
                        curr_perform_rate_operation = performRateOperations;
                        curr_perform_rate_operation.m_fragment = Video_play.this;
                        curr_perform_rate_operation.execute();
                    } else {
                        getSnackbar(getString(R.string.alreadydisLikeRemoveMsg), Snackbar.LENGTH_LONG).show();

                        PerformRateOperations performRateOperations = new PerformRateOperations(vid, "none");
                        curr_perform_rate_operation = performRateOperations;
                        curr_perform_rate_operation.m_fragment = Video_play.this;
                        curr_perform_rate_operation.execute();

                        isThisVideoDisliked = false;
                        updateUI_dislike(false);
                    }
                } else {
                    showLoginRequiredDialog();
                }
            }
        });

        mVideo_share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getLoginStatus()) {
                    //updateUI_share(true);
                    share_video_btn_click();
                } else {
                    showLoginRequiredDialog();
                }
            }
        });

        mVideo_watch_later_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (getLoginStatus()) {
                    add_to_watchLater(isChecked);
                } else {
                    showLoginRequiredDialog();
                }
            }
        });

        mVideo_favourite_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (getLoginStatus()) {
                    add_to_favourite(isChecked);
                } else {
                    showLoginRequiredDialog();
                }
            }
        });

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ImageView logoIcon = toolbar.findViewById(R.id.toolbar_logo);
        logoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goMainScreen();
            }
        });

        YouTubePlayerSupportFragment youTubePlayerSupportFragment = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_player_view);
        youTubePlayerSupportFragment.initialize(Player_config.YOUTUBE_PLAYER_API_KEY, this);

        thumb_adapter = new Related_thumbnail_adapter();
        videoTitle_adapter = new Related_videotitle_adapter();
        channelName_adapter = new Related_channelName_adapter();
        videoView_adapter = new Related_videoView_adapter();

        youTubePlayerView = findViewById(R.id.youtube_player_view);

        //gather_data_recycle_all_upload();
        //init_player();
    }

    @Override
    public void onAttachFragment(android.app.Fragment fragment) {
        super.onAttachFragment(fragment);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (isNetworkAvailable()) {

            /*INITIATING THE SILENT LOGIN TO GOOGLE FOR CREATING THE ACCESSING CREDENTIAL FOR API CALLING*/
            initSilentLogin();

        } else {
            makeNoInternetAlert();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (sourceIntentflag){
            goMainScreen();
        } else {
            onBackPressed();
        }
        return true;
    }

    //    private void init_player(){
//        youTubePlayerView.initialize(Player_config.YOUTUBE_PLAYER_API_KEY, onInitializedListener);
//    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
        mYouTubeplayer = youTubePlayer;
        mYtwasRestored = wasRestored;
        startYouTubePlayer();
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Log.d(TAG, "onInitializationFailure: " + youTubeInitializationResult);

        switch (youTubeInitializationResult){
            case SUCCESS:
                break;
            case INTERNAL_ERROR:
                InvalidVideoIdError("INTERNAL_ERROR");
                break;
            case UNKNOWN_ERROR:
                InvalidVideoIdError("INTERNAL_ERROR");
                break;
            case SERVICE_MISSING:
                InvalidVideoIdError("SERVICE_MISSING");
                break;
            case SERVICE_VERSION_UPDATE_REQUIRED:
                InvalidVideoIdError("SERVICE_VERSION_UPDATE_REQUIRED");
                break;
            case SERVICE_DISABLED:
                InvalidVideoIdError("SERVICE_DISABLED");
                break;
            case SERVICE_INVALID:
                InvalidVideoIdError("SERVICE_INVALID");
                break;
            case ERROR_CONNECTING_TO_SERVICE:
                InvalidVideoIdError("SERVICE_VERSION_UPDATE_REQUIRED");
                break;
            case CLIENT_LIBRARY_UPDATE_REQUIRED:
                InvalidVideoIdError("INTERNAL_ERROR");
                break;
            case NETWORK_ERROR:
                InvalidVideoIdError("NETWORK_ERROR");
                break;
            case DEVELOPER_KEY_INVALID:
                InvalidVideoIdError("INTERNAL_ERROR");
                break;
            case INVALID_APPLICATION_SIGNATURE:
                InvalidVideoIdError("INTERNAL_ERROR");
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("connection", "msg: " + connectionResult.getErrorMessage());
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        final Fragment profile_fragment = new Profilepage_drawer();
        final Fragment settings_fragment = new Settings_drawer();

        switch (id) {
            case R.id.action_settings:
                Intent settingIntent = new Intent(this, SettingsActivity.class);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                startActivity(settingIntent);
                break;
            case R.id.action_signout:
                showProgress(getString(R.string.pregress_msg_logout));
                logOut();
                break;

            case R.id.action_signin:
                goLogInScreen();
                break;

            case R.id.action_refresh:
                updateUI();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);

        if (getLoginStatus()) {
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_profile).setVisible(true);
            menu.findItem(R.id.action_settings).setVisible(true);
            menu.findItem(R.id.action_signout).setVisible(true);
            menu.findItem(R.id.action_signin).setVisible(false);
        } else {
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_profile).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            menu.findItem(R.id.action_signout).setVisible(false);
            menu.findItem(R.id.action_signin).setVisible(true);
        }

        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onRefresh() {
        updateUI();
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
//
//        } if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();

        get_current_settings();

        /*COMPARING THE RECYCLER VIEW MAX LIMIT SETTINGS*/
        if (YOUTUBE_RECYCLER_MAX_LIMIT != null && recycler_results_per_page_count != null){
            if ( Integer.parseInt(YOUTUBE_RECYCLER_MAX_LIMIT) != recycler_results_per_page_count ){
                Toast.makeText(Video_play.this, "Settings changed. Please Swipe To Refresh.", Toast.LENGTH_LONG).show();
            }
        }

        /*COMPARING YOUTUBE THUMBNAIL QUALITY SETTINGS*/
        if (YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY != null && YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY != null){
            if (!YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals(YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY)){
                Toast.makeText(Video_play.this, "Settings changed. Please Swipe To Refresh.", Toast.LENGTH_LONG).show();
            }
        }

        /*COMPARING LAYOUT CHANGE SETTTINGS*/
        if (LAYOUT_SETTING_CURR != null && LAYOUT_SETTING_PREV != null){
            if (!LAYOUT_SETTING_CURR.equals(LAYOUT_SETTING_PREV)){
                makeAppRestartDialog();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        recycler_next_page_token_comment = "";
    }

    @Override
    protected void onStop() {
        super.onStop();
        editor.putString(getString(R.string.lastActivity), getClass().getName());
        editor.putString("lastVid", vid);
        editor.putString("lastVTitle", title);
        editor.commit();
    }

    private String lastActivity;
    @Override
    protected void onRestart() {
        super.onRestart();
        String lastActivity = sharedPreferences.getString(getString(R.string.lastActivity), "");
        String lastVid = sharedPreferences.getString("lastVid", "");
        String lastVTitle = sharedPreferences.getString("lastVTitle", "");

        lastActivity = lastActivity;
        vid = lastVid;
        title = lastVTitle;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgress();
        recycler_next_page_token_comment = "";
        isLoadingMoreComments = false;
        isNewCommentLoaded= false;
        if (getLoginStatus()){
            if (curr_fetch_comments != null){
                curr_fetch_comments.cancel(true);
            }

            if (curr_fetch_my_comments != null){
                curr_fetch_my_comments.cancel(true);
            }

            if (curr_edit_comments != null){
                curr_edit_comments.cancel(true);
            }

            if (curr_delete_comment != null){
                curr_delete_comment.cancel(true);
            }

            if (curr_mark_As_spam != null){
                curr_mark_As_spam.cancel(true);
            }
        }

        if (curr_process_comment_response != null){
            curr_process_comment_response.cancel(true);
        }

        if (curr_publis_public_comment != null){
            curr_publis_public_comment.cancel(true);
        }

        if (curr_fetch_video_rating_count != null){
            curr_fetch_video_rating_count.cancel(true);
        }

        if (curr_fetch_video_info != null){
            curr_fetch_video_info.cancel(true);
        }

        if (curr_fetch_related_videos != null){
            curr_fetch_related_videos.cancel(true);
        }

        if (curr_perform_rate_operation != null){
            curr_perform_rate_operation.cancel(true);
        }

        if (curr_check_rating_status != null){
            curr_check_rating_status.cancel(true);
        }

        Button b1 = show_login_required_alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (b1 != null){
            if (b1.hasOnClickListeners()){
                b1.setOnClickListener(null);
                loginRequiredListener = null;
            }
        }

        Button b2 = net_connection_alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (b2 != null){
            if (b2.hasOnClickListeners()){
                b2.setOnClickListener(null);
                netConnectListener = null;
            }
        }

        Button b3 = invalid_id_alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (b3 != null){
            if (b3.hasOnClickListeners()){
                b3.setOnClickListener(null);
                invalidVIDListener = null;
            }
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        //DO NOTHING
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        //DO NOTHING
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast.makeText(this, "This app requires Google Play Services. Please install \" + \"Google Play Services on your device and relaunch this app.", Toast.LENGTH_SHORT).show();
                } else {
                    if (!CURR_TOP_LEVEL_COMMENT_TEXT.equals("")){
                        publish_top_comment();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK){
                    Toast.makeText(this, "Request Accepted", Toast.LENGTH_SHORT).show();
                    if (!CURR_TOP_LEVEL_COMMENT_TEXT.equals("")){
                        publish_top_comment();
                    } else if (COMMENT_FETCH_PERMISSION_FLAG){
                        if (curr_fetch_comments != null){
                            curr_fetch_comments.cancel(true);
                        }

                        fetch_comments_login();
                    }
                } else {
                    Toast.makeText(this, "Without this permission granted you can not publish comment. Please grant this permission request", Toast.LENGTH_SHORT).show();
                }
        }
    }































    /*
     * ALL METHODS ..............................................................................................
     * */
    private void initSilentLogin() {
        if (getLoginStatus()) {
            googleApiClient.connect();
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
            if (opr.isDone()) {
                GoogleSignInResult result = opr.get();
                handleSignInResult(result);
            } else {
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                        handleSignInResult(googleSignInResult);
                    }
                });
            }
        } else {
            if (vid != null) {
                fetch_video_details();
            }

            mCommentText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasfocus) {
                    if (hasfocus){
                        comment_without_login.setTitle(getString(R.string.commentWithoutLogin));
                        comment_without_login.setMessage(getString(R.string.commentWithoutLoginMsg));
                        comment_without_login.setCancelable(true);
                        comment_without_login.setButton(AlertDialog.BUTTON_POSITIVE, "Go To Login", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                goLogInScreen();
                            }
                        });
                        comment_without_login.setButton(AlertDialog.BUTTON_NEGATIVE, "Dismiss", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        comment_without_login.show();
                        mCommentText.setText("");
                    }
                }
            });

            mComment_inserter_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    comment_without_login.setTitle(getString(R.string.commentWithoutLogin));
                    comment_without_login.setMessage(getString(R.string.commentWithoutLoginMsg));
                    comment_without_login.setCancelable(true);
                    comment_without_login.setButton(AlertDialog.BUTTON_POSITIVE, "Go To Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            goLogInScreen();
                        }
                    });
                    comment_without_login.setButton(AlertDialog.BUTTON_NEGATIVE, "Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    comment_without_login.show();
                    mCommentText.setText("");
                }
            });
        }
    }

    private void startYouTubePlayer(){
        if (!mYtwasRestored) {
            if (vid != null && !vid.equals("")) {
                mYouTubeplayer.loadVideo(vid);
            } else {
                mYouTubeplayer.loadVideo("V8NAWrKlsAU");
            }
        }
    }

    private Snackbar getSnackbar(String message, int durationCode) {
        return Snackbar.make(activity_main, message, durationCode);
    }

    private void getIntentExtras() {
        if (getIntent().hasExtra(getString(R.string.passvid)) && getIntent().hasExtra(getString(R.string.passTitle))) {
            vid = getIntent().getStringExtra(getString(R.string.passvid));
            title = getIntent().getStringExtra(getString(R.string.passTitle));
            sourceIntentflag = getIntent().getBooleanExtra(getString(R.string.videoplayIntentSourceFlag), false);
            setIntentData();
        }
    }

    private void setIntentData() {
        mVideoTitle.setText(title);
    }

//    private void gather_data_recycle_comment(CommentThreadListResponse response) {
//        clearList(this.mCommVideoId);
//        clearList(this.mCommentId);
//        clearList(this.mCommAuthorName);
//        clearList(this.mCommAuthorChannelUrl);
//        clearList(this.mCommAuthorProfileImage);
//        clearList(this.mCommText);
//        clearList(this.mCommRatting);
//        clearList(this.mCommLikeCount);
//        clearList(this.mCommDateTime);
//        clearList(this.mCommCanRate);
//
//        List<CommentThread> commentThreads = response.getItems();
//        if (commentThreads == null || commentThreads.isEmpty()){
//            Toast.makeText(this, getString(R.string.noComment), Toast.LENGTH_SHORT).show();
//        } else {
//            for (int i = 0; i < commentThreads.size(); i++) {
//
//                mCommVideoId.add(commentThreads.get(i).getSnippet().getVideoId());
//                mCommentId.add(commentThreads.get(i).getSnippet().getTopLevelComment().getId());
//                mCommAuthorName.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getAuthorDisplayName());
//                mCommAuthorChannelUrl.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getAuthorChannelId().toString());
//                mCommAuthorProfileImage.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getAuthorProfileImageUrl());
//                mCommText.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getTextDisplay());
//                mCommRatting.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getViewerRating());
//                mCommLikeCount.add(String.valueOf(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getLikeCount()));
//                mCommDateTime.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getPublishedAt());
//                mCommCanRate.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getCanRate());
//
//            }
//            initCommentRecycleView();
//        }
//    }

//    private void gather_data_recycle_comment_php(JSONArray array) throws JSONException {
//        if (array != null && array.length() > 0){
//            for (int i = 0; i < array.length(); i++) {
//                JSONObject comment = array.getJSONObject(i);
//                JSONObject snippet = comment.getJSONObject("snippet");
//                JSONObject topLevelComment = snippet.getJSONObject("topLevelComment");
//                JSONObject toplevelcommentSnippet = topLevelComment.getJSONObject("snippet");
//
//                mCommVideoId.add(snippet.getString("videoId"));
//                mCommentId.add(topLevelComment.getString("id"));
//                mCommAuthorName.add(toplevelcommentSnippet.getString("authorDisplayName"));
//                mCommAuthorChannelUrl.add(toplevelcommentSnippet.getString("authorChannelUrl"));
//                mCommAuthorProfileImage.add(toplevelcommentSnippet.getString("authorProfileImageUrl"));
//                mCommText.add(toplevelcommentSnippet.getString("textDisplay"));
//                mCommRatting.add(toplevelcommentSnippet.getString("viewerRating"));
//                mCommLikeCount.add(String.valueOf(toplevelcommentSnippet.get("likeCount")));
//                mCommCanRate.add(toplevelcommentSnippet.getBoolean("canRate"));
//
//                String dt = String.valueOf(toplevelcommentSnippet.get("publishedAt"));
//                Instant comm_instant = Instant.parse(dt);
//                Instant now_instant = Instant.now();
//                if ( comm_instant.isBeforeNow() ){
//                    long difference = now_instant.getMillis() - comm_instant.getMillis();
//                    long seconds = difference / 1000;
//                    long minutes = seconds / 60;
//                    long hours = minutes / 60;
//                    long days = hours / 24;
//
//                    Log.d(TAG, "gather_data_recycle_comment_php: "+ days + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60);
//                }
//                //mCommDateTime.add( );
//            }
//        }
//        initCommentRecycleView();
//    }

    private Date stringToDate(String date, String format) {
        if (date != null) {
            ParsePosition pos = new ParsePosition(0);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            Date stringDate = simpleDateFormat.parse(date, pos);
            return stringDate;
        }
        return null;
    }

    private void gather_data_recycle_all_upload(SearchListResponse response) {
        clearList(this.mVideo);
        clearList(this.mThumbnail);
        clearList(this.mTitle);
        clearList(this.mChannel);
        clearList(this.mDescripies);

        recycler_total_result_count = response.getPageInfo().getTotalResults();
        recycler_results_per_page_count = response.getPageInfo().getResultsPerPage();

        List<SearchResult> searchResults = response.getItems();
        if (searchResults == null || searchResults.isEmpty()) {
            Toast.makeText(this, getString(R.string.norelatedVideoFound), Toast.LENGTH_SHORT).show();
        } else {
            for (int i = 0; i < searchResults.size(); i++) {
                mVideo.add(searchResults.get(i).getId().getVideoId());

                String vThumbnail = getString(R.string.youtubeThumbnailDefaultRes);
                switch (YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY){
                    case "default":
                        vThumbnail = searchResults.get(i).getSnippet().getThumbnails().getDefault().getUrl();
                        break;
                    case "medium":
                        vThumbnail = searchResults.get(i).getSnippet().getThumbnails().getMedium().getUrl();
                        break;
                    case "high":
                        vThumbnail = searchResults.get(i).getSnippet().getThumbnails().getHigh().getUrl();
                        break;
                    case "standard":
                        vThumbnail = searchResults.get(i).getSnippet().getThumbnails().getStandard().getUrl();
                        break;
                    case "maxres":
                        vThumbnail = searchResults.get(i).getSnippet().getThumbnails().getMaxres().getUrl();
                        break;
                }
                mThumbnail.add(vThumbnail);

                mTitle.add(searchResults.get(i).getSnippet().getTitle());
                mChannel.add(searchResults.get(i).getSnippet().getChannelTitle());
                mDescripies.add(searchResults.get(i).getSnippet().getDescription());

                //if ( response.getItems().get(i).getSnippet().getChannelId().equals(channelId) ){ }
            }

//        mThumbnail = thumb_adapter.get_list();
//        mTitle = videoTitle_adapter.get_list();
//        mChannel = channelName_adapter.get_list();
//        mViews = videoView_adapter.get_list();

            initRecyclerView();
        }
    }

    private void clearList(ArrayList list) {
        list.clear();
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView_related);
        recycler_adapter = new Recycler_adapter_related(LAYOUT_SETTING_CURR,this, mVideo, mThumbnail, mTitle, mChannel, mDescripies);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(recycler_adapter);

        /*LAYOUT MANAGER IS THE SWITCH TO SWITCH BETWEEN LINEAR LAYOUT MANAGER AND GRID LAYOUT MANAGER*/
        switch (LAYOUT_SETTING_CURR){
            case "list":
                recyclerView.setLayoutManager(layoutManager_list);
                break;
            case "grid":
                recyclerView.setLayoutManager(layoutManager_grid);
                break;
            case "gridHorizontal":
                recyclerView.setLayoutManager(layoutManager_horizontal);
                break;
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
    }

    private void initCommentRecycleView() {
        commentRecyclerCallbacks = new Recycler_adapter_comment.CommentRElatedCallback() {

            @Override
            public void onCommentEdit(final String commentId, final int position) {
                String original_comment = mCommText.get(position);
                mCommentText.setText(original_comment);

                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(activity_main.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);

                if (commentrecyclerView != null){
                    commentrecyclerView.smoothScrollToPosition(0);
                }

                if ( mComment_inserter_btn.getVisibility() == View.VISIBLE ){
                    mComment_inserter_btn.setVisibility(View.GONE);
                    //mComment_inserter_btn.setOnClickListener(null);
                }

                if (mComment_update_section.getVisibility() == View.GONE){
                    mComment_update_section.setVisibility(View.VISIBLE);
                    mComment_update_btn.setVisibility(View.VISIBLE);
                    mComment_update_cancel_btn.setVisibility(View.VISIBLE);
                }

                mComment_update_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String e_c_s = mCommentText.getText().toString();
                        if (!e_c_s.equals("")) {
                            CURR_TOP_LEVEL_COMMENT_EDITED_TEXT = e_c_s;
                            CURR_TOP_LEVEL_EDITED_COMMENT_ID = commentId;
                            CURR_TOP_LEVEL_COMMENT_EDIT_POSITION = position;
                            publish_edited_top_comment();
                        }
                    }
                });

                mComment_update_cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mComment_update_section.getVisibility() == View.VISIBLE){
                            mComment_update_section.setVisibility(View.GONE);
                            mComment_update_btn.setVisibility(View.GONE);
                            mComment_update_cancel_btn.setVisibility(View.GONE);

                            mComment_inserter_btn.setVisibility(View.VISIBLE);
                            mCommentText.setText("");
                        }
                    }
                });
            }

            @Override
            public void onCommentDelet(String commentId, int position) {
                showProgress("Deleting the comment . . .");
                Delete_comments delete_comments = new Delete_comments(commentId, position);
                curr_delete_comment = delete_comments;
                curr_delete_comment.m_fragment = Video_play.this;
                curr_delete_comment.execute();
            }

            @Override
            public void onCommentReporting(String commentId, int position) {
                Toast.makeText(Video_play.this, "This user is reported to Google!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCommentBanning(String commentID, int position) {
                Toast.makeText(Video_play.this, "This user Banning request has sent to Google for approval", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCommentSpam(String commentID, int position) {
                Mark_Spam_comments mark_spam_comments = new Mark_Spam_comments(commentID, position);
                curr_mark_As_spam = mark_spam_comments;
                curr_mark_As_spam.m_fragment = Video_play.this;
                curr_mark_As_spam.execute();
            }
        };

        commentrecyclerView = findViewById(R.id.comment_section_recycler);
        comment_recycler_adapter = new Recycler_adapter_comment(this, mCommVideoId, mCommentId, mCommAuthorName, mCommAuthorChannelUrl, mCommAuthorProfileImage, mCommText, mCommRatting, mCommLikeCount, mCommDateTime, mCommCanRate, mMyCommCommentId, commentRecyclerCallbacks);
        commentrecyclerView.setNestedScrollingEnabled(false);
        commentrecyclerView.setAdapter(comment_recycler_adapter);
        commentrecyclerView.setLayoutManager(layoutManager_comment);
        commentrecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        if (isLoadingMoreComments && !recycler_next_page_token_comment.equals("")) {
            comment_recycler_adapter.notifyItemInserted(mCommVideoId.size() - 1);
        }

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null){
                    if (scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight()) && scrollY > oldScrollY){

                        if (getLoginStatus()){
                            curr_recycler_visible_item_count_comment = layoutManager_comment.getChildCount();
                            first_visible_item_position_comment = layoutManager_comment.findFirstVisibleItemPosition();

                            if ( (curr_recycler_visible_item_count_comment + first_visible_item_position_comment) < commentCount.intValue() && first_visible_item_position_comment >= 0 ){
                                Toast.makeText(Video_play.this, "Loading More Comments . . ."+mCommVideoId.size(), Toast.LENGTH_SHORT).show();
                                loadMoreComments();
                            }
                        } else {
                            getSnackbar(getString(R.string.logintoloadmorecomment), Snackbar.LENGTH_LONG);
                        }
                    }
                }
            }
        });

        commentrecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //At this point the layout is complete and the
                //dimensions of recyclerView and any child views are known.
                //Remove listener after changed RecyclerView's height to prevent infinite loop
                mVideoCommentCountCurr.setText("Now Showing - "+comment_recycler_adapter.getItemCount()+" - comments");
                Log.d(TAG, "onGlobalLayout: "+comment_recycler_adapter.getItemCount());

                if (isLoadingMoreComments){
                    //commentrecyclerView.smoothScrollToPosition(mCommVideoId.size() - 1);
                    isLoadingMoreComments = false;
                }

                if (isNewCommentLoaded){
                    commentrecyclerView.smoothScrollToPosition(0);
                    isNewCommentLoaded = false;
                }

                commentrecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void loadMoreComments(){
        isLoadingMoreComments = true;
        if (getLoginStatus()){
            fetch_comments_login();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
            if (account != null) {
                mAccount = account.getAccount();

                credential = GoogleAccountCredential.usingOAuth2(getApplicationContext(), Arrays.asList(SCOPES)).setBackOff(new ExponentialBackOff());
                credentialYT = GoogleAccountCredential.usingOAuth2(getApplicationContext(), Arrays.asList(YOUTUBE_SCOPE)).setBackOff(new ExponentialBackOff());

                credential.setSelectedAccount(mAccount);
                credentialYT.setSelectedAccount(mAccount);

                transport = AndroidHttp.newCompatibleTransport();
                jasonfactory = JacksonFactory.getDefaultInstance();

                mYoutube = new com.google.api.services.youtube.YouTube.Builder(transport, jasonfactory, credential)
                        .setApplicationName(getString(R.string.youtubeAppname))
                        .build();
                mYoutube_comment = new YouTube.Builder(transport, jasonfactory, credentialYT)
                        .setApplicationName(getString(R.string.youtubeAppname))
                        .build();

                if (vid != null) {
                    //new Fetch_video_info().execute();

                    CheckRatingStatus checkRatingStatus = new CheckRatingStatus(true, "initial");
                    curr_check_rating_status = checkRatingStatus;
                    curr_check_rating_status.m_fragment = Video_play.this;
                    curr_check_rating_status.execute();
                }

                String imgUri = getProfilePicUrl();
                if (imgUri != "") {
                    Glide.with(this).load(imgUri).into(mCircleImageComment);
                }


                if ( mComment_inserter_btn.getVisibility() == View.GONE ){
                    mComment_inserter_btn.setVisibility(View.VISIBLE);
                }

                if (mComment_update_section.getVisibility() == View.VISIBLE){
                    mComment_update_section.setVisibility(View.GONE);
                    mComment_update_btn.setVisibility(View.GONE);
                    mComment_update_cancel_btn.setVisibility(View.GONE);
                    //mComment_update_btn.setOnClickListener(null);
                }

                mComment_inserter_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String publicComment = mCommentText.getText().toString();
                        if (!publicComment.equals("")) {
                            CURR_TOP_LEVEL_COMMENT_TEXT = publicComment;
                            publish_top_comment();
                        }
                    }
                });


            } else {
                getSnackbar(getString(R.string.youtubeLoginError), Snackbar.LENGTH_LONG).show();
                initSilentLogin();
            }
        } else {
            getSnackbar(getString(R.string.youtubeLoginError), Snackbar.LENGTH_LONG).show();
            initSilentLogin();
        }
    }

    private void parseVideoDetailsfromResponse(VideoListResponse response) {
        List<Video> videoList = response.getItems();
        if (videoList == null || videoList.isEmpty()) {
            Toast.makeText(this, getString(R.string.videoNotFound), Toast.LENGTH_SHORT).show();
        } else {
            for (int i = 0; i < videoList.size(); i++) {
                thumbnail = videoList.get(i).getSnippet().getThumbnails().getMedium().getUrl();
                publishdate = videoList.get(i).getSnippet().getPublishedAt();
                description = videoList.get(i).getSnippet().getDescription();
                channelId = videoList.get(i).getSnippet().getChannelId();
                channelTitle = videoList.get(i).getSnippet().getChannelTitle();
                duration = videoList.get(i).getContentDetails().getDuration();
                viewCount = videoList.get(i).getStatistics().getViewCount();
                likeCount = videoList.get(i).getStatistics().getLikeCount();
                dislikeCount = videoList.get(i).getStatistics().getDislikeCount();
                favoriteCount = videoList.get(i).getStatistics().getFavoriteCount();
                commentCount = videoList.get(i).getStatistics().getCommentCount();
            }

            Fetch_related_vides fetch_related_vides = new Fetch_related_vides();
            curr_fetch_related_videos = fetch_related_vides;
            curr_fetch_related_videos.m_fragment = Video_play.this;
            curr_fetch_related_videos.execute();

            //fetch_comments_PHP();
            COMMENT_FETCH_PERMISSION_FLAG = false;
            fetch_comments_login();

            historysendtoPHPServer();
            setYoutubeData();
        }
    }

    private void setYoutubeData() {
        mVideoChannelImage.setImageResource(R.drawable.applogo_s);
        mVideoDescription.setText(description);
        mChannelname.setText(channelTitle);

        if (getLoginStatus()) {
            mVideoViewCount.setText(viewCount.toString() + " views");
            mVideoLikeCount.setText(likeCount.toString());
            mVideoDisLikeCount.setText(dislikeCount.toString());
            mVideoShareCount.setText("Youtube share  "+favoriteCount.toString());
            mVideoCommentCount.setText(commentCount.toString()+ "  "+ getString(R.string.commentintotal));
            try {
                mVideoPublishDate.setText("Published on " + parseDateTimetousableString(String.valueOf(publishdate)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            //Logged in without Google
            mVideoViewCount.setText(viewCount_nologin.toString() + " views");
            mVideoLikeCount.setText(likeCount_nologin.toString());
            mVideoDisLikeCount.setText(dislikeCount_nologin.toString());
            mVideoShareCount.setText("Youtube share  "+favoriteCount_nologin.toString());
            mVideoPublishDate.setText("Published on " + publishDate_nologin);
            mVideoCommentCount.setText(commentCount_nologin + "   " + getString(R.string.commentintotal));
        }
    }

    public void setRowId(int id) {
        editor.putInt(getString(R.string.rowid), id);
        editor.commit();
    }

    public int getRowId() {
        return sharedPreferences.getInt(getString(R.string.rowid), 0);
    }

    private String getProfilePicUrl() {
        return sharedPreferences.getString("profilePicUrl", "");
    }

    private void updateRAtingUI(VideoGetRatingResponse response, Boolean flag) {
        if (response != null && !response.isEmpty()) {
            if (!response.getItems().isEmpty()){
                String rating = response.getItems().get(0).getRating();
                if (rating.equals("like") && flag) {
                    updateUI_like(true);
                    updateUI_dislike(false);
                    isThisVideoRated = true;
                    isThisVideoLiked = true;
                    isThisVideoDisliked = false;
                } else if (rating.equals("dislike") && flag) {
                    isThisVideoLiked = false;
                    isThisVideoDisliked = true;
                    updateUI_like(false);
                    updateUI_dislike(true);
                    isThisVideoRated = true;
                } else if (rating.equals("none") && flag) {
                    updateUI_like(false);
                    updateUI_dislike(false);
                    isThisVideoRated = false;
                    isThisVideoLiked = false;
                    isThisVideoDisliked = false;
                } else if (rating.equals("unspecified") && flag) {
                    updateUI_like(false);
                    updateUI_dislike(false);
                    isThisVideoRated = false;
                    isThisVideoLiked = false;
                    isThisVideoDisliked = false;
                }
            } else {
                InvalidVideoIdError("");
            }
        }
    }

    private void InvalidVideoIdError(String reason){
        switch (reason){
            case "NETWORK_ERROR":
                invalid_id_alertDialog.setTitle(getString(R.string.youtubeNetError));
                invalid_id_alertDialog.setMessage(getString(R.string.youtubeNetErrorMsg));
                break;

            case "SERVICE_DISABLED":
                invalid_id_alertDialog.setTitle(getString(R.string.youtubeDisabled));
                invalid_id_alertDialog.setMessage(getString(R.string.youtubeDisabledMsg));
                break;

            case "SERVICE_INVALID":
                invalid_id_alertDialog.setTitle(getString(R.string.youtubeInvalid));
                invalid_id_alertDialog.setMessage(getString(R.string.youtubeInvalidMsg));
                break;

            case "SERVICE_MISSING":
                invalid_id_alertDialog.setTitle(getString(R.string.youtubenotfound));
                invalid_id_alertDialog.setMessage(getString(R.string.youtubenotfoundMsg));
                break;

            case "SERVICE_VERSION_UPDATE_REQUIRED":
                invalid_id_alertDialog.setTitle(getString(R.string.youtubeoutoddate));
                invalid_id_alertDialog.setMessage(getString(R.string.youtubeoutofdateMsg));
                break;
            case "INTERNAL_ERROR":
                invalid_id_alertDialog.setTitle(getString(R.string.youtubeinternalerror));
                invalid_id_alertDialog.setMessage(getString(R.string.youtubeinternalerrorMsg));
                break;

            default:
                invalid_id_alertDialog.setTitle(getString(R.string.invalidVid));
                invalid_id_alertDialog.setMessage(getString(R.string.invalidVidMsg));
                break;
        }

        invalid_id_alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Go Back", invalidVIDListener);
        invalid_id_alertDialog.setCancelable(false);
        invalid_id_alertDialog.show();
    }

    private void updateUI_like(Boolean flag) {
        Drawable d;
        if (flag) {
            d = getResources().getDrawable(R.drawable.ic_like_user);
        } else {
            d = getResources().getDrawable(R.drawable.ic_like);
        }
        mVideo_like_btn.setBackground(d);
    }

    private void updateUI_dislike(Boolean flag) {
        Drawable d;
        if (flag) {
            d = getResources().getDrawable(R.drawable.ic_dislike_user);
        } else {
            d = getResources().getDrawable(R.drawable.ic_dislike);
        }
        mVideo_disLike_btn.setBackground(d);
    }

//    private void updateUI_share(Boolean flag) {
//        Drawable d;
//        if (flag) {
//            d = getResources().getDrawable(R.drawable.ic_share_user);
//        } else {
//            d = getResources().getDrawable(R.drawable.ic_share);
//        }
//        mVideo_share_btn.setBackground(d);
//    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    private void makeNoInternetAlert() {
        net_connection_alertDialog.setTitle(getString(R.string.noInternetMsg));
        net_connection_alertDialog.setMessage(getString(R.string.noInternetDetailMsg));
        net_connection_alertDialog.setButton(AlertDialog.BUTTON_POSITIVE ,"OK", netConnectListener);
        net_connection_alertDialog.show();
    }

    private void updateUI_watchLater(Boolean flag) {
        Drawable d;
        if (flag) {
            d = getResources().getDrawable(R.drawable.ic_clock_user);
        } else {
            d = getResources().getDrawable(R.drawable.ic_clock);
        }
        mVideo_watch_later_btn.setBackground(d);
    }

    private void updateUI_favourite(Boolean flag) {
        Drawable d;
        if (flag) {
            d = getResources().getDrawable(R.drawable.ic_favourite_user);
        } else {
            d = getResources().getDrawable(R.drawable.ic_favourite_local);
        }
        mVideo_favourite_btn.setBackground(d);
    }

    public void logOut() {
        if (isSignedIn()) {
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    if (status.isSuccess()) {
                        Toast.makeText(getBaseContext(), "Successfully Logged out from Google", Toast.LENGTH_SHORT).show();
                        logoutFromPHP();
                    } else {
                        getSnackbar(getString(R.string.not_close_session), Snackbar.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void showProgress(String msg) {
        mProgress.setMessage(msg);
        mProgress.show();
    }

    private void hideProgress() {
        if (mProgress.isShowing()){
            mProgress.setMessage("");
            mProgress.dismiss();
        }
    }

    private boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(this) != null;
    }

    private void logoutFromPHP() {
        String uri = getString(R.string.host_url) + "/" + getString(R.string.app_logout) + "/" + getRowId();
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        getSnackbar(getString(R.string.phpLogoutSuccess), Snackbar.LENGTH_SHORT).show();
                        logoutFromFirebase();
                    } else {
                        hideProgress();
                        getSnackbar(getString(R.string.phpLogoutError), Snackbar.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    hideProgress();
                    e.printStackTrace();
                    getSnackbar(String.valueOf(e), Snackbar.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress();
            }
        });
        queue.add(postRequest);
    }

    private void logoutFromFirebase() {
        firebaseAuth.getInstance().signOut();
        hideProgress();
        goLogInScreen();
    }


    private void historysendtoPHPServer() {
        String uri = getString(R.string.host_url) + "/" + getString(R.string.history_token);
        RequestQueue queue = Volley.newRequestQueue(Video_play.this);

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("videoId", vid);
        jsonParams.put("userId", String.valueOf(getRowId()));
        jsonParams.put("thumbnail", thumbnail);
        jsonParams.put("title", title);
        jsonParams.put("description", description);
        jsonParams.put("duration", duration);

        Log.d(TAG, "historysendtoPHPServer: " + description);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getBoolean("status")) {

                        Log.d(TAG, "historyResponse: " + response);

                        isVideo_wl = response.getBoolean("wl_flag");
                        isVideo_fav = response.getBoolean("fav_flag");

                        mVideo_watch_later_btn.setChecked(isVideo_wl);
                        mVideo_favourite_btn.setChecked(isVideo_fav);

                        //getSnackbar(response.getString("message"), Snackbar.LENGTH_SHORT).show();
                    } else {
                        //getSnackbar(response.getString("message"), Snackbar.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //getSnackbar(String.valueOf(e), Snackbar.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(getRequest);
    }

    private void add_to_watchLater(final Boolean flag) {
        RequestQueue queue = Volley.newRequestQueue(Video_play.this);

        if (flag && !isVideo_wl) {
            String uri = getString(R.string.host_url) + "/" + getString(R.string.watchLater_token);
            Map<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("videoId", vid);
            jsonParams.put("userId", String.valueOf(getRowId()));
            jsonParams.put("thumbnail", thumbnail);
            jsonParams.put("title", title);
            jsonParams.put("description", description);
            jsonParams.put("duration", duration);

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.getBoolean("status")) {
                            isVideo_wl = true;
                            updateUI_watchLater(flag);
                            Toast.makeText(Video_play.this, getString(R.string.addWatchLater_yes), Toast.LENGTH_SHORT).show();
                        } else {
                            updateUI_watchLater(isVideo_wl);
                            Toast.makeText(Video_play.this, getString(R.string.addWatchLater_no), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        updateUI_watchLater(isVideo_wl);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    updateUI_watchLater(isVideo_wl);
                    Toast.makeText(Video_play.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            queue.add(postRequest);
        } else if (!flag && isVideo_wl) {
            String uri = getString(R.string.host_url) + "/" + getString(R.string.watchLater_remove_token) + "/" + vid + "/" + getRowId();
            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getBoolean("status")) {
                            isVideo_wl = false;
                            updateUI_watchLater(flag);
                            Toast.makeText(Video_play.this, getString(R.string.removeWatchLater_yes), Toast.LENGTH_SHORT).show();
                        } else {
                            updateUI_watchLater(isVideo_wl);
                            Toast.makeText(Video_play.this, getString(R.string.addWatchLater_no), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        updateUI_watchLater(isVideo_wl);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    updateUI_watchLater(isVideo_wl);
                    Toast.makeText(Video_play.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            queue.add(getRequest);
        }
    }

    private void add_to_favourite(final Boolean flag) {
        RequestQueue queue = Volley.newRequestQueue(Video_play.this);

        if (flag && !isVideo_fav) {
            String uri = getString(R.string.host_url) + "/" + getString(R.string.favourite_token);
            Map<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("videoId", vid);
            jsonParams.put("userId", String.valueOf(getRowId()));
            jsonParams.put("thumbnail", thumbnail);
            jsonParams.put("title", title);
            jsonParams.put("description", description);
            jsonParams.put("duration", duration);

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        if (response.getBoolean("status")) {
                            isVideo_fav = true;
                            updateUI_favourite(flag);
                            Toast.makeText(Video_play.this, getString(R.string.addFavourite_yes), Toast.LENGTH_SHORT).show();
                        } else {
                            updateUI_favourite(isVideo_fav);
                            Toast.makeText(Video_play.this, getString(R.string.favourite_no), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        updateUI_favourite(isVideo_fav);
                        getSnackbar(String.valueOf(e), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    updateUI_favourite(isVideo_fav);
                    Toast.makeText(Video_play.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            queue.add(postRequest);
        } else if (!flag && isVideo_fav) {
            String uri = getString(R.string.host_url) + "/" + getString(R.string.favourite_remove_token) + "/" + vid + "/" + getRowId();
            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getBoolean("status")) {
                            isVideo_fav = false;
                            updateUI_favourite(flag);
                            Toast.makeText(Video_play.this, getString(R.string.removeFavourite_yes), Toast.LENGTH_SHORT).show();
                        } else {
                            updateUI_favourite(isVideo_fav);
                            Toast.makeText(Video_play.this, getString(R.string.favourite_no), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        updateUI_favourite(isVideo_fav);
                        getSnackbar(String.valueOf(e), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    updateUI_favourite(isVideo_fav);
                    Toast.makeText(Video_play.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            queue.add(getRequest);
        }
    }

    private void fetch_comments_PHP() {
        show_comment_loader();
        RequestQueue queue = Volley.newRequestQueue(Video_play.this);
        String uri = getString(R.string.host_url) + "/" + getString(R.string.fetch_comment_uri) + "/" + vid;

//        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, uri, null, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//                try {
//                    gather_data_recycle_comment_php(response);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        });

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hide_comment_loader();
                try {
                    //gather_data_recycle_comment_php(response.getJSONArray("data"));
                    if (response.getBoolean("status")) {
                        if (response.get("data") != null) {
                            m_firstCommenter.setVisibility(View.GONE);

                            ProcessCommentResponse newprocessCommentResponse = new ProcessCommentResponse(false);
                            curr_process_comment_response = newprocessCommentResponse;
                            curr_process_comment_response.m_fragment = Video_play.this;
                            curr_process_comment_response.localArray = response.getJSONArray("data");
                            curr_process_comment_response.execute();

                        } else {
                            m_firstCommenter.setVisibility(View.VISIBLE);
                            Toast.makeText(Video_play.this, getString(R.string.noComment), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        m_firstCommenter.setVisibility(View.VISIBLE);
                        Toast.makeText(Video_play.this, getString(R.string.noComment), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hide_comment_loader();
            }
        });

        queue.add(getRequest);

        if (swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private Boolean getLoginStatus() {
        return sharedPreferences.getBoolean(getString(R.string.loginStatus), true);
    }


    private void showLoginRequiredDialog() {
        show_login_required_alertDialog.setTitle(getString(R.string.loginRequiredTitle));
        show_login_required_alertDialog.setMessage(getString(R.string.LoginRequiredMsg));
        show_login_required_alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Login", loginRequiredListener);
        show_login_required_alertDialog.show();
    }

    private void fetch_video_details() {
        String uri = getString(R.string.youtube_base_address) +
                "/" + getString(R.string.yt_vedioDetails) +
                "?part=snippet%2CcontentDetails%2Cstatistics&id=" +
                vid +
                "&key=" + getString(R.string.YOUTUBE_API_KEY_WEB);

        RequestQueue yt_queue = Volley.newRequestQueue(this);
        JsonObjectRequest youtube_get_request = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    try {
                        parseVideoDetailsfromResponse_nologin(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Youtube Data not came. Promote to try again
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "youtubeApiReq: " + error);
            }
        });
        yt_queue.add(youtube_get_request);
    }

    private String parseDateTimetousableString(String dateTime) throws ParseException {
        String publishDate;
        String dt1 = String.valueOf(dateTime);
        String[] splits = dt1.split("T");
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df2 = new SimpleDateFormat("EEE, MMM dd, ''yyyy");

        Date result = df1.parse(splits[0]);
        publishDate = df2.format(result);
        return publishDate;
    }

    private void parseVideoDetailsfromResponse_nologin(JSONObject response) throws JSONException {
        JSONArray videoList = response.getJSONArray("items");
        if (videoList == null) {
            InvalidVideoIdError("");
            Toast.makeText(this, getString(R.string.videoNotFound), Toast.LENGTH_SHORT).show();
        } else if (videoList.length() <= 0){
            InvalidVideoIdError("");
        } else {
            for (int i = 0; i < videoList.length(); i++) {
                thumbnail = videoList.getJSONObject(i).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url");
                try {
                    publishDate_nologin = parseDateTimetousableString(videoList.getJSONObject(i).getJSONObject("snippet").getString("publishedAt"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    publishDate_nologin = videoList.getJSONObject(i).getJSONObject("snippet").getString("publishedAt");
                }
                description = videoList.getJSONObject(i).getJSONObject("snippet").getString("description");
                channelId = videoList.getJSONObject(i).getJSONObject("snippet").getString("channelId");
                channelTitle = videoList.getJSONObject(i).getJSONObject("snippet").getString("channelTitle");
                duration = videoList.getJSONObject(i).getJSONObject("contentDetails").getString("duration");
                viewCount_nologin = videoList.getJSONObject(i).getJSONObject("statistics").getString("viewCount");
                likeCount_nologin = videoList.getJSONObject(i).getJSONObject("statistics").getString("likeCount");
                dislikeCount_nologin = videoList.getJSONObject(i).getJSONObject("statistics").getString("dislikeCount");
                favoriteCount_nologin = videoList.getJSONObject(i).getJSONObject("statistics").getString("favoriteCount");
                commentCount_nologin = videoList.getJSONObject(i).getJSONObject("statistics").getString("commentCount");
            }

            fetch_related_videos();
            fetch_comments_PHP();
            //historysendtoPHPServer();
            setYoutubeData();
        }
    }

    private void fetch_related_videos() {
        String uri = getString(R.string.youtube_base_address) +
                "/" + getString(R.string.yt_search) +
                "?part=snippet&channelId=" +
                channelId +
                "&maxResults=" + YOUTUBE_RECYCLER_MAX_LIMIT + "&type=video&key="
                + getString(R.string.YOUTUBE_API_KEY_WEB);

        RequestQueue yt_queue = Volley.newRequestQueue(this);
        JsonObjectRequest youtube_get_request = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    try {
                        gather_data_recycle_all_upload_nologin(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Youtube Search Data no come. Promote to try again
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "youtubeApiReq: " + error);
            }
        });
        yt_queue.add(youtube_get_request);
    }

    private void gather_data_recycle_all_upload_nologin(JSONObject response) throws JSONException {
        clearList(this.mVideo);
        clearList(this.mThumbnail);
        clearList(this.mTitle);
        clearList(this.mChannel);
        clearList(this.mDescripies);

        recycler_total_result_count = response.getJSONObject("pageInfo").getInt("totalResults");
        recycler_results_per_page_count = response.getJSONObject("pageInfo").getInt("resultsPerPage");

        JSONArray jsonArray = response.getJSONArray("items");
        if (jsonArray == null) {
            Toast.makeText(this, getString(R.string.norelatedVideoFound), Toast.LENGTH_SHORT).show();
        } else {
            for (int i = 0; i < jsonArray.length(); i++) {
                mVideo.add(jsonArray.getJSONObject(i).getJSONObject("id").getString("videoId"));

                if(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals("maxres")){
                    this.mThumbnail.add( jsonArray.getJSONObject(i).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url") );
                } else {
                    this.mThumbnail.add( jsonArray.getJSONObject(i).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject(YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY).getString("url") );
                }

                mTitle.add(jsonArray.getJSONObject(i).getJSONObject("snippet").getString("title"));
                mChannel.add(jsonArray.getJSONObject(i).getJSONObject("snippet").getString("channelTitle"));
                mDescripies.add(jsonArray.getJSONObject(i).getJSONObject("snippet").getString("description"));
            }
            initRecyclerView();
        }
    }

    private void goLogInScreen() {
        Intent intent = new Intent(Video_play.this, Login.class);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void goMainScreen(){
        Intent mainIntent = new Intent(Video_play.this, MainActivity.class);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainIntent);
        finish();
    }

    private void update_comment_recycle(CommentThread commentThread) {
        String leftDateTimeString;

        mCommVideoId.add(0, commentThread.getSnippet().getVideoId());
        mCommentId.add(0, commentThread.getSnippet().getTopLevelComment().getId());
        mCommAuthorName.add(0, commentThread.getSnippet().getTopLevelComment().getSnippet().getAuthorDisplayName());
        mCommAuthorChannelUrl.add(0, commentThread.getSnippet().getTopLevelComment().getSnippet().getAuthorChannelId().toString());
        mCommAuthorProfileImage.add(0, commentThread.getSnippet().getTopLevelComment().getSnippet().getAuthorProfileImageUrl());
        mCommText.add(0, commentThread.getSnippet().getTopLevelComment().getSnippet().getTextOriginal());
        mCommRatting.add(0, commentThread.getSnippet().getTopLevelComment().getSnippet().getViewerRating());
        mCommLikeCount.add(0, String.valueOf(commentThread.getSnippet().getTopLevelComment().getSnippet().getLikeCount()));
        mCommCanRate.add(0, commentThread.getSnippet().getTopLevelComment().getSnippet().getCanRate());

        mMyCommCommentId.add(commentThread.getSnippet().getTopLevelComment().getId());

        String dt = String.valueOf(commentThread.getSnippet().getTopLevelComment().getSnippet().getPublishedAt());
        Instant comm_instant = Instant.parse(dt);
        Instant now_instant = Instant.now();
        if (comm_instant.isBeforeNow()) {
            long difference = now_instant.getMillis() - comm_instant.getMillis();
            long seconds = difference / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            leftDateTimeString = makeDate(seconds, minutes, hours, days);
        } else {
            leftDateTimeString = "11 minutes";
        }
        mCommDateTime.add(0, leftDateTimeString);
        initCommentRecycleView();
    }

    private void editing_comment_recycle(Comment responseComment, int position){
        if (responseComment != null){
            delete_Item_comment_recycle(position);

            String v_id = mCommVideoId.get(position);
            String comm_id = responseComment.getId();
            String auth_name = responseComment.getSnippet().getAuthorDisplayName();
            String auth_chanUrl = responseComment.getSnippet().getAuthorChannelUrl();
            String auth_prof_img = responseComment.getSnippet().getAuthorProfileImageUrl();
            String comm_text = responseComment.getSnippet().getTextOriginal();
            String comm_rate = responseComment.getSnippet().getViewerRating();
            String comm_like_cnt = String.valueOf(responseComment.getSnippet().getLikeCount());
            Boolean comm_rate_abl = responseComment.getSnippet().getCanRate();

            String leftDateTimeString;
            String dt = String.valueOf(responseComment.getSnippet().getPublishedAt());
            Instant comm_instant = Instant.parse(dt);
            Instant now_instant = Instant.now();
            if (comm_instant.isBeforeNow()) {
                long difference = now_instant.getMillis() - comm_instant.getMillis();
                long seconds = difference / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;

                leftDateTimeString = makeDate(seconds, minutes, hours, days);
            } else {
                leftDateTimeString = "1 minutes";
            }

            mCommVideoId.add(position, v_id);
            mCommentId.add(position, comm_id);
            mCommAuthorName.add(position, auth_name);
            mCommAuthorChannelUrl.add(position, auth_chanUrl);
            mCommAuthorProfileImage.add(position, auth_prof_img);
            mCommText.add(position, comm_text);
            mCommRatting.add(position, comm_rate);
            mCommLikeCount.add(position, comm_like_cnt);
            mCommCanRate.add(position, comm_rate_abl);
            mCommDateTime.add(position, leftDateTimeString);

            comment_recycler_adapter.notifyItemInserted(position);
            comment_recycler_adapter.notifyDataSetChanged();
        }

    }

    private void delete_Item_comment_recycle(int position){
        if (commentrecyclerView != null){
            mCommVideoId.remove(position);
            mCommentId.remove(position);
            mCommAuthorName.remove(position);
            mCommAuthorChannelUrl.remove(position);
            mCommAuthorProfileImage.remove(position);
            mCommText.remove(position);
            mCommRatting.remove(position);
            mCommLikeCount.remove(position);
            mCommCanRate.remove(position);
            mCommDateTime.remove(position);
            comment_recycler_adapter.notifyItemMoved(position, position);
            comment_recycler_adapter.notifyDataSetChanged();
        }
    }

    private void updateRattingCountUI(VideoListResponse response){
        viewCount = response.getItems().get(0).getStatistics().getViewCount();
        likeCount = response.getItems().get(0).getStatistics().getLikeCount();
        dislikeCount = response.getItems().get(0).getStatistics().getDislikeCount();
        favoriteCount = response.getItems().get(0).getStatistics().getFavoriteCount();
        commentCount = response.getItems().get(0).getStatistics().getCommentCount();

        mVideoViewCount.setText(viewCount.toString() + " views");
        mVideoLikeCount.setText(likeCount.toString());
        mVideoDisLikeCount.setText(dislikeCount.toString());
        mVideoShareCount.setText(favoriteCount.toString());
        mVideoCommentCount.setText(commentCount+getString(R.string.commentintotal));
    }

    private void updateUI() {
        recycler_next_page_token_comment = "";
        startYouTubePlayer();
        if (getLoginStatus()){

            CheckRatingStatus checkRatingStatus = new CheckRatingStatus(true, "initial");
            curr_check_rating_status = checkRatingStatus;
            curr_check_rating_status.m_fragment = Video_play.this;
            curr_check_rating_status.execute();
        } else {
            if (vid != null) {
                fetch_video_details();
            }
        }
    }

    private void get_current_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_RECYCLER_MAX_LIMIT = sharedPreferences_settings.getString(getString(R.string.pref_youtubePageSize_key), getString(R.string.All_upload_max_result));
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            LAYOUT_SETTING_CURR = sharedPreferences_settings.getString(getString(R.string.videoPlayLayoutKey), "list");
        } else {
            YOUTUBE_RECYCLER_MAX_LIMIT = getString(R.string.All_upload_max_result);
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            LAYOUT_SETTING_CURR = "list";
        }
    }

    private void get_previous_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_RECYCLER_MAX_LIMIT = sharedPreferences_settings.getString(getString(R.string.pref_youtubePageSize_key), getString(R.string.All_upload_max_result));
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            LAYOUT_SETTING_PREV = sharedPreferences_settings.getString(getString(R.string.videoPlayLayoutKey), "list");
        } else {
            YOUTUBE_RECYCLER_MAX_LIMIT = getString(R.string.All_upload_max_result);
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            LAYOUT_SETTING_PREV = "list";
        }
    }

    private void settings_equalizer(){
        YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY;
        LAYOUT_SETTING_CURR = LAYOUT_SETTING_PREV;
    }

    private void makeAppRestartDialog(){
        AlertDialog.Builder nonCancelableDialog = new AlertDialog.Builder(Video_play.this);
        //nonCancelableDialog.setCancelable(false);
        nonCancelableDialog.setTitle(getString(R.string.restartAppdialogTitle));
        nonCancelableDialog.setMessage(getString(R.string.restartAppdialogMessage));
        nonCancelableDialog.setPositiveButton("Restart Application", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Restart the app
                restartApp();
            }
        });
        nonCancelableDialog.setNegativeButton("Restart Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Dissmiss the dialog
            }
        });

        AlertDialog dialog = nonCancelableDialog.create();
        dialog.show();
    }

    private void restartApp(){
        Intent restartIntent = getPackageManager().getLaunchIntentForPackage( getPackageName() );
        assert restartIntent != null;
        restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(restartIntent);
    }

    private void publish_top_comment(){
        Publish_public_comment publish_public_comment = new Publish_public_comment(CURR_TOP_LEVEL_COMMENT_TEXT);
        curr_publis_public_comment = publish_public_comment;
        curr_publis_public_comment.m_fragment = Video_play.this;
        curr_publis_public_comment.execute();
    }

    private void publish_edited_top_comment(){
        showProgress("Updating the comment . . .");
        Edit_comment edit_comment = new Edit_comment();
        curr_edit_comments = edit_comment;
        curr_edit_comments.m_fragment = Video_play.this;
        curr_edit_comments.execute();
    }

    private void fetch_comments_login(){
        Fetch_my_comments fetch_my_comments = new Fetch_my_comments();
        curr_fetch_my_comments = fetch_my_comments;
        curr_fetch_my_comments.m_fragment = Video_play.this;
        curr_fetch_my_comments.execute();
    }

    private void commentingDone(CommentThread commentThread){
        isNewCommentLoaded = true;
        hideProgress();
        if (m_firstCommenter.getVisibility() == View.VISIBLE){
            m_firstCommenter.setVisibility(View.GONE);
        }
        update_comment_recycle(commentThread);
        mCommentText.setText("");
    }

    private void commentEditingDone(Comment response){
        isEditCommentLoaded = true;
        hideProgress();
        editing_comment_recycle(response, CURR_TOP_LEVEL_COMMENT_EDIT_POSITION);
        if (mComment_update_section.getVisibility() == View.VISIBLE){
            mComment_update_section.setVisibility(View.GONE);

            mComment_update_btn.setVisibility(View.GONE);
            mComment_update_cancel_btn.setVisibility(View.GONE);
            mComment_inserter_btn.setVisibility(View.VISIBLE);
        }
        mCommentText.setText("");
        closeSoftKeyboard();
    }

    private String makeDate(long sec, long min, long hour, long day) {
        String tempTime;

        if (sec % 60 <= 0) {
            tempTime = "right now";
        } else if (min % 60 <= 0) {
            tempTime = sec % 60 + " seconds ago";
        } else if (hour % 24 <= 0) {
            tempTime = min % 60 + " minutes ago";
        } else if (day <= 0) {
            tempTime = hour % 24 + " hours ago";
        } else {
            tempTime = day + " days ago";
        }

        return tempTime;
    }

    private void process_my_comments(CommentThreadListResponse mycommentonthisvideo){
        //commentThreadListResponse --> id
        //match with commentID arrayList

        if (mycommentonthisvideo != null){
            if(mycommentonthisvideo.getItems().isEmpty()){
                Toast.makeText(this, "You have not commented on this video before . . .", Toast.LENGTH_SHORT).show();
            } else {
                for (int i = 0; i < mycommentonthisvideo.getItems().size(); i++) {
                    String my_comment_id = mycommentonthisvideo.getItems().get(i).getId();
                    mMyCommCommentId.add(my_comment_id);
                }
            }
        }
    }

    private void show_comment_loader(){
        if (mCommentProgressbar != null){
            mCommentProgressbar.setVisibility(View.VISIBLE);
        }
    }
    private void hide_comment_loader(){
        if (mCommentProgressbar != null){
            mCommentProgressbar.setVisibility(View.GONE);
        }
    }

    private void closeSoftKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = Video_play.this.getCurrentFocus();
        if (view == null){
            view = new View(Video_play.this);
        }
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void goToBrowser(String uri){
        if (uri != null){
            Uri ad_uri = Uri.parse(uri);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, ad_uri);

            PackageManager packageManager = getPackageManager();
            List<ResolveInfo> activities = packageManager.queryIntentActivities(browserIntent, PackageManager.MATCH_DEFAULT_ONLY);
            boolean isIntentSafe = activities.size() > 0;

            if (isIntentSafe){
                String intentTitle = getString(R.string.browserChooserText);
                Intent chooser = Intent.createChooser(browserIntent, intentTitle);
                startActivity(chooser);
            } else {
                //THERE IS NOT ANY APP INSTALLED IN YOUR PHONE TO OPEN THIS LINK
                Toast.makeText(this, getString(R.string.noBrowserText), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void share_video_btn_click(){
        if (vid != null){
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.youutbeSharelink)+vid);

            String vid_shrare_title = getString(R.string.vid_share_title);
            Intent intentChooser = Intent.createChooser(shareIntent, vid_shrare_title);
            startActivity(intentChooser);
        } else {
            Toast.makeText(this, "No compatible video found to Share", Toast.LENGTH_SHORT).show();
        }
    }





















    /*
     * ALL CLASSES .........................................................................................
     * */
    private static class ProcessCommentResponse extends AsyncTask<Void, Void, Void> {
        public JSONArray localArray = new JSONArray();
        public CommentThreadListResponse commentThreadListResponse = null;

        private Boolean authFlag = true;

        private ArrayList<String> videoId = new ArrayList<>();
        private ArrayList<String> commentId = new ArrayList<>();
        private ArrayList<String> id = new ArrayList<>();
        private ArrayList<String> authorName = new ArrayList<>();
        private ArrayList<String> channelUrl = new ArrayList<>();
        private ArrayList<String> authorProfileImage = new ArrayList<>();
        private ArrayList<String> text = new ArrayList<>();
        private ArrayList<String> ratting = new ArrayList<>();
        private ArrayList<String> likeCount = new ArrayList<>();
        private ArrayList<String> dateTime = new ArrayList<String>();
        private ArrayList<Boolean> canRate = new ArrayList<>();

        public Video_play m_fragment = null;

        Exception mException;

        ProcessCommentResponse(Boolean authFlag) {
            this.authFlag = authFlag;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if (!isCancelled()){
                    if (authFlag){
                        process_authenticated();
                    } else {
                        process(this.localArray);
                    }
                }
            } catch (JSONException e) {
                mException = e;
                e.printStackTrace();
            }
            return null;
        }

        private void process_authenticated(){
            String leftDateTimeString;
            if (commentThreadListResponse != null){
                if (!m_fragment.isLoadingMoreComments){
                    m_fragment.clearList(m_fragment.mCommVideoId);
                    m_fragment.clearList(m_fragment.mCommentId);
                    m_fragment.clearList(m_fragment.mCommAuthorName);
                    m_fragment.clearList(m_fragment.mCommAuthorChannelUrl);
                    m_fragment.clearList(m_fragment.mCommAuthorProfileImage);
                    m_fragment.clearList(m_fragment.mCommText);
                    m_fragment.clearList(m_fragment.mCommRatting);
                    m_fragment.clearList(m_fragment.mCommLikeCount);
                    m_fragment.clearList(m_fragment.mCommDateTime);
                    m_fragment.clearList(m_fragment.mCommCanRate);
                }

                m_fragment.recycler_total_result_count_comment = commentThreadListResponse.getPageInfo().getTotalResults();
                m_fragment.recycler_results_per_page_count_comment = commentThreadListResponse.getPageInfo().getResultsPerPage();
                if (commentThreadListResponse.getNextPageToken() != null){
                    m_fragment.recycler_next_page_token_comment = commentThreadListResponse.getNextPageToken();
                } else {
                    m_fragment.recycler_next_page_token_comment = "";
                }

                List<CommentThread> commentThreads = commentThreadListResponse.getItems();
                if (commentThreads == null || commentThreads.isEmpty()){
                    if (!isCancelled()){
                        if (!m_fragment.sourceIntentflag){
                            Toast.makeText(m_fragment, m_fragment.getString(R.string.noComment), Toast.LENGTH_SHORT).show();
                        }
                        m_fragment.m_firstCommenter.setVisibility(View.VISIBLE);
                    }
                } else {
                    m_fragment.m_firstCommenter.setVisibility(View.GONE);
                    for (int i = 0; i < commentThreads.size(); i++) {
                        if (isCancelled()){
                            break;
                        }

                        m_fragment.mCommVideoId.add(commentThreads.get(i).getSnippet().getVideoId());
                        m_fragment.mCommentId.add(commentThreads.get(i).getSnippet().getTopLevelComment().getId());
                        m_fragment.mCommAuthorName.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getAuthorDisplayName());
                        m_fragment.mCommAuthorChannelUrl.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getAuthorChannelId().toString());
                        m_fragment.mCommAuthorProfileImage.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getAuthorProfileImageUrl());
                        m_fragment.mCommText.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getTextOriginal());
                        m_fragment.mCommRatting.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getViewerRating());
                        m_fragment.mCommLikeCount.add(String.valueOf(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getLikeCount()));
                        m_fragment.mCommCanRate.add(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getCanRate());

                        String dt = String.valueOf(commentThreads.get(i).getSnippet().getTopLevelComment().getSnippet().getPublishedAt());
                        Instant comm_instant = Instant.parse(dt);
                        Instant now_instant = Instant.now();
                        if (comm_instant.isBeforeNow()) {
                            long difference = now_instant.getMillis() - comm_instant.getMillis();
                            long seconds = difference / 1000;
                            long minutes = seconds / 60;
                            long hours = minutes / 60;
                            long days = hours / 24;

                            leftDateTimeString = m_fragment.makeDate(seconds, minutes, hours, days);
                            //Log.d(TAG, "gather_data_recycle_comment_php: "+ days + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60);
                        } else {
                            leftDateTimeString = "11 minutes";
                        }
                        m_fragment.mCommDateTime.add(leftDateTimeString);
                    }
                }
            } else {
                Toast.makeText(m_fragment, m_fragment.getString(R.string.noComment), Toast.LENGTH_SHORT).show();
            }
        }

        private void process(JSONArray array) throws JSONException {
            String leftDateTimeString;
            if (array != null && array.length() > 0) {

                for (int i = 0; i < array.length(); i++) {
                    if (isCancelled()){
                        break;
                    }

                    JSONObject comment = array.getJSONObject(i);
                    JSONObject snippet = comment.getJSONObject("snippet");
                    JSONObject topLevelComment = snippet.getJSONObject("topLevelComment");
                    JSONObject toplevelcommentSnippet = topLevelComment.getJSONObject("snippet");

                    videoId.add(snippet.getString("videoId"));
                    id.add(topLevelComment.getString("id"));
                    authorName.add(toplevelcommentSnippet.getString("authorDisplayName"));
                    channelUrl.add(toplevelcommentSnippet.getString("authorChannelUrl"));
                    authorProfileImage.add(toplevelcommentSnippet.getString("authorProfileImageUrl"));
                    text.add(toplevelcommentSnippet.getString("textOriginal"));
                    ratting.add(toplevelcommentSnippet.getString("viewerRating"));
                    likeCount.add(String.valueOf(toplevelcommentSnippet.get("likeCount")));
                    canRate.add(toplevelcommentSnippet.getBoolean("canRate"));

                    String dt = String.valueOf(toplevelcommentSnippet.get("publishedAt"));
                    Instant comm_instant = Instant.parse(dt);
                    Instant now_instant = Instant.now();
                    if (comm_instant.isBeforeNow()) {
                        long difference = now_instant.getMillis() - comm_instant.getMillis();
                        long seconds = difference / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        long days = hours / 24;

                        leftDateTimeString = m_fragment.makeDate(seconds, minutes, hours, days);
                        //Log.d(TAG, "gather_data_recycle_comment_php: "+ days + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60);
                    } else {
                        leftDateTimeString = "11 minutes";
                    }
                    dateTime.add(leftDateTimeString);
                }
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (localArray != null) {
                if (mException == null) {
                    if (!m_fragment.getLoginStatus()){
                        if (!m_fragment.isLoadingMoreComments){
                            m_fragment.mCommVideoId = this.videoId;
                            m_fragment.mCommentId = this.id;
                            m_fragment.mCommAuthorName = this.authorName;
                            m_fragment.mCommAuthorChannelUrl = this.channelUrl;
                            m_fragment.mCommAuthorProfileImage = this.authorProfileImage;
                            m_fragment.mCommText = this.text;
                            m_fragment.mCommRatting = this.ratting;
                            m_fragment.mCommLikeCount = this.likeCount;
                            m_fragment.mCommDateTime = this.dateTime;
                            m_fragment.mCommCanRate = this.canRate;
                        }
                    }

                    m_fragment.initCommentRecycleView();

                    /*m_fragment.mComment_inserter_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String publicComment = m_fragment.mCommentText.getText().toString();
                            if (!publicComment.equals("")) {
                                Toast.makeText(m_fragment, publicComment, Toast.LENGTH_SHORT).show();

                                Publish_public_comment publish_public_comment = new Publish_public_comment(publicComment);
                                m_fragment.curr_publis_public_comment = publish_public_comment;
                                m_fragment.curr_publis_public_comment.m_fragment = m_fragment;
                                m_fragment.curr_publis_public_comment.execute();
                            }
                        }
                    });*/

                } else {
                    /*Comment View Exception Happened. Handle*/
                }
            } else {
                /*No Comment Data Recieved*/
            }
        }
    }

    private static class Fetch_video_ratting_Count extends AsyncTask<Void, Void, VideoListResponse>{
        public Video_play m_fragment;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected VideoListResponse doInBackground(Void... voids) {
            VideoListResponse ratingResponse = new VideoListResponse();
            if (m_fragment.vid != null){
                try {
                    if (!isCancelled()){
                        ratingResponse = fetchRattingCount();
                    } else {
                        return null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return ratingResponse;
        }

        private VideoListResponse fetchRattingCount() throws IOException {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "statistics");
            parameters.put("id", m_fragment.vid);

            YouTube.Videos.List videosListByIdRequest = m_fragment.mYoutube.videos().list(parameters.get("part").toString());
            if (parameters.containsKey("id") && parameters.get("id") != "") {
                videosListByIdRequest.setId(parameters.get("id").toString());
            }
            VideoListResponse response = null;
            if (!isCancelled()){
                response = videosListByIdRequest.execute();
            }
            return response;
        }

        @Override
        protected void onPostExecute(VideoListResponse videoListResponse) {
            super.onPostExecute(videoListResponse);
            if (videoListResponse != null){
                m_fragment.updateRattingCountUI(videoListResponse);
            } else {
                //This Query has failed
            }
        }
    }

    private static class Fetch_video_info extends AsyncTask<Void, Void, VideoListResponse> {
        public Video_play m_fragment;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected VideoListResponse doInBackground(Void... voids) {
            VideoListResponse response = new VideoListResponse();
            try {
                if (!isCancelled()){
                    response = fetchVideoDetails();
                } else {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        private VideoListResponse fetchVideoDetails() throws IOException {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet,contentDetails,statistics");
            parameters.put("id", m_fragment.vid);

            YouTube.Videos.List videosListByIdRequest = m_fragment.mYoutube.videos().list(parameters.get("part").toString());
            if (parameters.containsKey("id") && parameters.get("id") != "") {
                videosListByIdRequest.setId(parameters.get("id").toString());
            }
            VideoListResponse response = null;
            if (!isCancelled()){
                response = videosListByIdRequest.execute();
            }
            return response;
        }

        @Override
        protected void onPostExecute(VideoListResponse response) {
            if (response != null){
                m_fragment.parseVideoDetailsfromResponse(response);
            }
        }
    }

    private static class Fetch_related_vides extends AsyncTask<Void, Void, SearchListResponse> {
        public Video_play m_fragment;

        @Override
        protected SearchListResponse doInBackground(Void... voids) {
            SearchListResponse response = new SearchListResponse();
            try {
                if (m_fragment.vid != null) {
                    if (!isCancelled()){
                        response = fetchRelatedVideos();
                    } else {
                        return null;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        private SearchListResponse fetchRelatedVideos() throws IOException {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet");
            //parameters.put("relatedToVideoId", vid);
            parameters.put("type", "video");
            parameters.put("channelId", m_fragment.channelId);

            YouTube.Search.List searchListRelatedVideosRequest = m_fragment.mYoutube.search().list(parameters.get("part").toString());
            if (parameters.containsKey("relatedToVideoId") && parameters.get("relatedToVideoId") != "") {
                searchListRelatedVideosRequest.setRelatedToVideoId(parameters.get("relatedToVideoId").toString());
            }

            if (parameters.containsKey("type") && parameters.get("type") != "") {
                searchListRelatedVideosRequest.setType(parameters.get("type").toString());
            }

            if (parameters.containsKey("channelId") && parameters.get("channelId") != "") {
                searchListRelatedVideosRequest.setChannelId(parameters.get("channelId").toString());
            }

            searchListRelatedVideosRequest.setMaxResults(Long.valueOf(m_fragment.YOUTUBE_RECYCLER_MAX_LIMIT));
            SearchListResponse response = null;
            if (!isCancelled()){
                response = searchListRelatedVideosRequest.execute();
            }
            return response;
        }

        @Override
        protected void onPostExecute(SearchListResponse searchListResponse) {
            if (searchListResponse != null) {
                m_fragment.gather_data_recycle_all_upload(searchListResponse);
            }
        }
    }

    private static class PerformRateOperations extends AsyncTask<Void, Void, Void> {
        private String vid;
        private String rateType;
        private Exception exception = null;
        public Video_play m_fragment = null;

        public PerformRateOperations(String vid, String rateType) {
            this.vid = vid;
            this.rateType = rateType;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (vid != null) {
                if (!isCancelled()){
                    rateVideos();
                } else {

                }
            }
            return null;
        }

        private void rateVideos() {
            try {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("id", vid);
                parameters.put("rating", rateType);

                YouTube.Videos.Rate videosRateRequest = null;
                if (!isCancelled()){
                    videosRateRequest = m_fragment.mYoutube.videos().rate(parameters.get("id").toString(), parameters.get("rating").toString());
                }
                if (videosRateRequest != null) {
                    videosRateRequest.execute();
                } else {
                    //Show error Massage
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                this.exception = e;
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (this.rateType.equals("like")) {
                m_fragment.isThisVideoLiked = true;
                m_fragment.isThisVideoDisliked = false;
                m_fragment.updateUI_like(true);
                m_fragment.updateUI_dislike(false);
            } else if (this.rateType.equals("dislike")){
                m_fragment.isThisVideoLiked = false;
                m_fragment.isThisVideoDisliked = true;
                m_fragment.updateUI_like(false);
                m_fragment.updateUI_dislike(true);
            }

            CheckRatingStatus checkRatingStatus = new CheckRatingStatus(false, "commit");
            m_fragment.curr_check_rating_status = checkRatingStatus;
            m_fragment.curr_check_rating_status.m_fragment = m_fragment;
            m_fragment.curr_check_rating_status.execute();

//            if (this.exception != null){
//            } else {
//                AlertDialog alertDialog = new AlertDialog.Builder(Video_play.this).create();
//                alertDialog.setTitle(getString(R.string.videoratingfailed));
//                alertDialog.setMessage( getString(R.string.videoratingfailedmsg) + "Exception: " + exception );
//                alertDialog.setButton("OK", new RateRetryListener(true, this.vid, this.rateType));
//                alertDialog.show();
//            }
        }
    }

    private static class CheckRatingStatus extends AsyncTask<Void, Void, VideoGetRatingResponse> {
        private Boolean initFlag;
        private String position;
        private Exception exception;
        public Video_play m_fragment = null;

        public CheckRatingStatus(Boolean flag, String position) {
            this.initFlag = flag;
            this.position = position;
        }

        @Override
        protected VideoGetRatingResponse doInBackground(Void... voids) {
            VideoGetRatingResponse videoGetRatingResponse = null;
            if (!isCancelled()){
                videoGetRatingResponse = getRatings();
            } else {
                return null;
            }
            return getRatings();
        }

        private VideoGetRatingResponse getRatings() {
            VideoGetRatingResponse response = null;
            try {
                if (m_fragment.vid != null) {
                    HashMap<String, String> parameters = new HashMap<>();
                    parameters.put("id", m_fragment.vid);
                    parameters.put("onBehalfOfContentOwner", "");

                    YouTube.Videos.GetRating videosGetRatingRequest = m_fragment.mYoutube.videos().getRating(parameters.get("id").toString());
                    if (parameters.containsKey("onBehalfOfContentOwner") && parameters.get("onBehalfOfContentOwner") != "") {
                        videosGetRatingRequest.setOnBehalfOfContentOwner(parameters.get("onBehalfOfContentOwner").toString());
                    }

                    if (!isCancelled()){
                        response = videosGetRatingRequest.execute();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                exception = e;
                //new CheckRatingStatus(this.initFlag).execute();
            }
            return response;
        }

        @Override
        protected void onPostExecute(VideoGetRatingResponse videoGetRatingResponse) {
            super.onPostExecute(videoGetRatingResponse);
            if (videoGetRatingResponse != null){
                m_fragment.updateRAtingUI(videoGetRatingResponse, this.initFlag);
                if (this.position.equals("initial")){
                    Fetch_video_info fetch_video_info = new Fetch_video_info();
                    m_fragment.curr_fetch_video_info = fetch_video_info;
                    m_fragment.curr_fetch_video_info.m_fragment = m_fragment;
                    m_fragment.curr_fetch_video_info.execute();

                } else if(this.position.equals("commit")){
                    Fetch_video_ratting_Count fetch_video_ratting_count = new Fetch_video_ratting_Count();
                    m_fragment.curr_fetch_video_rating_count = fetch_video_ratting_count;
                    m_fragment.curr_fetch_video_rating_count.m_fragment = m_fragment;
                    m_fragment.curr_fetch_video_rating_count.execute();
                }
            }
        }
    }

    private class LoginRequiredListener implements View.OnClickListener, DialogInterface.OnClickListener {

        @Override
        public void onClick(View view) {
            goLogInScreen();
        }

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            goLogInScreen();
        }
    }

    private class NetConnectListener implements View.OnClickListener, DialogInterface.OnClickListener {

        private void startWifi() {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        }

        @Override
        public void onClick(View view) {
            startWifi();
        }

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            startWifi();
        }
    }

    private class InvalidVIDListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            //onBackPressed();
            goMainScreen();
        }
    }

    private class RateRetryListener implements DialogInterface.OnClickListener {
        private Boolean flag;
        private String vid, type;

        RateRetryListener(Boolean flag, String vid, String type){
            this.flag = flag;
            this.vid = vid;
            this.type = type;
        }

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            if (this.flag){
                dialogInterface.dismiss();

                PerformRateOperations performRateOperations = new PerformRateOperations(this.vid, this.type);
                curr_perform_rate_operation = performRateOperations;
                curr_perform_rate_operation.m_fragment = Video_play.this;
                curr_perform_rate_operation.execute();

            } else {
                dialogInterface.dismiss();
            }
        }
    }

    /*THIS CLASS IS ABOUT TO PUBLISH NEW TOPLEVEL COMMENT ON THE SUPPLIED VIDEO*/
    private static class Publish_public_comment extends AsyncTask<Void, Void, CommentThread> {
        String comment_orign;
        Exception exception;

        public Video_play m_fragment = null;

        public Publish_public_comment(String comment_orign) {
            this.comment_orign = comment_orign;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isCancelled()){
                m_fragment.showProgress(m_fragment.getString(R.string.postingCommentTitle));
            } else {
                m_fragment.hideProgress();
            }
        }

        @Override
        protected CommentThread doInBackground(Void... voids) {
            CommentThread thread = new CommentThread();
            if (m_fragment.vid != null) {
                if (!isCancelled()){
                    try {
                        thread = publish();
                    } catch (Exception e){
                        //m_fragment.hideProgress();
                        this.exception = e;
                        cancel(true);
                        return null;
                    }
                } else {
                    m_fragment.hideProgress();
                    return null;
                }
            }
            return thread;
        }

        private CommentThread publish() throws IOException {
            /*HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet");

            CommentThread commentThread = new CommentThread();
            CommentThreadSnippet snippet = new CommentThreadSnippet();
            snippet.set("channelId", "UCMQc2TiuVg89rwnutcJM6HA");
            Comment topLevelComment = new Comment();
            CommentSnippet commentSnippet = new CommentSnippet();
            commentSnippet.set("textOriginal", "This is a awesome work from shazzad");
            commentSnippet.set("videoId", "gCREOfadcNQ");

            topLevelComment.setSnippet(commentSnippet);
            snippet.setTopLevelComment(topLevelComment);
            commentThread.setSnippet(snippet);

            YouTube.CommentThreads.Insert commentThreadsInsertRequest = m_fragment.mYoutube_comment.commentThreads().insert(parameters.get("part").toString(), commentThread);
            CommentThread comment_response = commentThreadsInsertRequest.execute();*/


            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet");


            CommentThread commentThread = new CommentThread();
            CommentThreadSnippet snippet = new CommentThreadSnippet();
            snippet.set("channelId", m_fragment.channelId);
            Comment topLevelComment = new Comment();
            CommentSnippet commentSnippet = new CommentSnippet();
            commentSnippet.set("textOriginal", this.comment_orign);
            commentSnippet.set("videoId", m_fragment.vid);

            topLevelComment.setSnippet(commentSnippet);
            snippet.setTopLevelComment(topLevelComment);
            commentThread.setSnippet(snippet);

            YouTube.CommentThreads.Insert commentThreadsInsertRequest = m_fragment.mYoutube_comment.commentThreads().insert(parameters.get("part").toString(), commentThread);
            CommentThread comment_response = null;
            if (!isCancelled()){
                comment_response = commentThreadsInsertRequest.execute();
            } else {
                m_fragment.hideProgress();
            }

            return comment_response;
        }

        @Override
        protected void onPostExecute(CommentThread commentThread) {
            super.onPostExecute(commentThread);
            m_fragment.hideProgress();
            if (exception == null && commentThread != null) {
                m_fragment.commentingDone(commentThread);
            }
        }

        @Override
        protected void onCancelled() {
            m_fragment.hideProgress();
            if (exception != null){
                if (exception instanceof UserRecoverableAuthIOException) {
                    m_fragment.startActivityForResult( ((UserRecoverableAuthIOException) exception).getIntent(), REQUEST_AUTHORIZATION );
                } else if (exception instanceof GoogleJsonResponseException){
                    int err_code = ((GoogleJsonResponseException) exception).getStatusCode();
                    if (err_code == 403){
                        m_fragment.goolePlusRequired.setTitle(m_fragment.getString(R.string.googleplusrequiredtitle));
                        m_fragment.goolePlusRequired.setMessage(m_fragment.getString(R.string.googleplusrequiredmsg));
                        m_fragment.goolePlusRequired.setCancelable(false);
                        m_fragment.goolePlusRequired.setButton(DialogInterface.BUTTON_NEGATIVE, "OK! Dissmiss Now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                m_fragment.mCommentText.setText("");
                            }
                        });
                        m_fragment.goolePlusRequired.setButton(DialogInterface.BUTTON_POSITIVE, "Take me to YouTube", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                m_fragment.goToBrowser(m_fragment.getString(R.string.youtubechannelCreate_url));
                            }
                        });
                        m_fragment.goolePlusRequired.show();
                    }
                }
            }
        }
    }

    private static class Fetch_comments extends AsyncTask<Void, Void, CommentThreadListResponse>{
        public Video_play m_fragment;
        private Exception exception;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isCancelled()){
                m_fragment.show_comment_loader();
            } else {
                m_fragment.hide_comment_loader();
            }
        }

        @Override
        protected CommentThreadListResponse doInBackground(Void... voids) {
            CommentThreadListResponse response = null;
            if (m_fragment.vid != null){
                try{
                    if (!isCancelled()){
                        response = fetchComments();
                    } else {
                        m_fragment.hide_comment_loader();
                    }
                } catch (Exception e){
                    if (m_fragment.swipeRefreshLayout.isRefreshing()){
                        m_fragment.swipeRefreshLayout.setRefreshing(false);
                    }
                    exception = e;
                    cancel(true);
                    return null;
                }
            }
            return response;
        }

        private CommentThreadListResponse fetchComments() throws IOException {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet,replies");
            parameters.put("videoId", m_fragment.vid);
            parameters.put("pageToken", m_fragment.recycler_next_page_token_comment);
            parameters.put("order", "time");

            YouTube.CommentThreads.List commentThreadsListByVideoIdRequest = m_fragment.mYoutube_comment.commentThreads().list(parameters.get("part").toString());
            if (parameters.containsKey("videoId") && parameters.get("videoId") != "") {
                commentThreadsListByVideoIdRequest.setVideoId(parameters.get("videoId").toString());
            }
            if (parameters.containsKey("pageToken") && parameters.get("pageToken") != null ){
                commentThreadsListByVideoIdRequest.setPageToken(parameters.get("pageToken").toString());
            }
            if (parameters.containsKey("order") && parameters.get("order") != null){
                commentThreadsListByVideoIdRequest.setOrder(parameters.get("order"));
            }

            CommentThreadListResponse response = null;
            if (!isCancelled()){
                response = commentThreadsListByVideoIdRequest.execute();
            } else {
                m_fragment.hide_comment_loader();
            }
            return response;
        }

        @Override
        protected void onPostExecute(CommentThreadListResponse commentThreadListResponse) {
            super.onPostExecute(commentThreadListResponse);
            m_fragment.hide_comment_loader();
            if (m_fragment.swipeRefreshLayout.isRefreshing()){
                m_fragment.swipeRefreshLayout.setRefreshing(false);
            }
            if (commentThreadListResponse != null){
                ProcessCommentResponse newprocessCommentResponse = new ProcessCommentResponse(true);
                m_fragment.curr_process_comment_response = newprocessCommentResponse;
                m_fragment.curr_process_comment_response.m_fragment = m_fragment;
                m_fragment.curr_process_comment_response.commentThreadListResponse = commentThreadListResponse;
                m_fragment.curr_process_comment_response.execute();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            m_fragment.hide_comment_loader();
            if (m_fragment.swipeRefreshLayout.isRefreshing()){
                m_fragment.swipeRefreshLayout.setRefreshing(false);
            }
            if (exception != null){
                if (exception instanceof UserRecoverableAuthIOException){
                    m_fragment.COMMENT_FETCH_PERMISSION_FLAG = true;
                    m_fragment.startActivityForResult( ((UserRecoverableAuthIOException) exception).getIntent(), REQUEST_AUTHORIZATION );
                }
            }
        }
    }

    private static class Fetch_my_comments extends AsyncTask<Void, Void, CommentThreadListResponse>{
        public Video_play m_fragment;
        private Exception exception;

        Fetch_my_comments(){

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected CommentThreadListResponse doInBackground(Void... voids) {
            CommentThreadListResponse response = null;
            if (m_fragment.vid != null){
                try {
                    if (!isCancelled()){
                        response = fetchmycomments();
                    }
                } catch (Exception e) {
                    exception = e;
                    cancel(true);
                    return null;
                }
            }
            return response;
        }

        private CommentThreadListResponse fetchmycomments() throws IOException {
            CommentThreadListResponse commentThreadListResponse = null;

            m_fragment.clearList(m_fragment.mMyCommCommentId);

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet,replies");
            parameters.put("videoId", m_fragment.vid);
            parameters.put("moderationStatus", "published");
            parameters.put("searchTerms", m_fragment.sharedPreferences.getString("username", ""));

            YouTube.CommentThreads.List commentThreadsListByVideoIdRequest = m_fragment.mYoutube_comment.commentThreads().list(parameters.get("part").toString());
            if (parameters.containsKey("videoId") && parameters.get("videoId") != "") {
                commentThreadsListByVideoIdRequest.setVideoId(parameters.get("videoId").toString());
            }
            if (parameters.containsKey("moderationStatus") && parameters.get("moderationStatus") != ""){
                commentThreadsListByVideoIdRequest.setModerationStatus(parameters.get("moderationStatus").toString());
            }
            if (parameters.containsKey("searchTerms") && parameters.get("searchTerms") != ""){
                commentThreadsListByVideoIdRequest.setSearchTerms(parameters.get("searchTerms").toString());
            }

            CommentThreadListResponse response = null;
            if (!isCancelled()){
                response = commentThreadsListByVideoIdRequest.execute();
            }

            return response;
        }

        @Override
        protected void onPostExecute(CommentThreadListResponse commentThreadListResponse) {
            super.onPostExecute(commentThreadListResponse);
            if (commentThreadListResponse != null){
                m_fragment.process_my_comments(commentThreadListResponse);

                Fetch_comments fetch_comments = new Fetch_comments();
                m_fragment.curr_fetch_comments = fetch_comments;
                m_fragment.curr_fetch_comments.m_fragment = m_fragment;
                m_fragment.curr_fetch_comments.execute();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (exception != null){
                if (exception instanceof UserRecoverableAuthIOException){
                    m_fragment.COMMENT_FETCH_PERMISSION_FLAG = true;
                    m_fragment.startActivityForResult( ((UserRecoverableAuthIOException) exception).getIntent(), REQUEST_AUTHORIZATION );
                } else {
                    Fetch_comments fetch_comments = new Fetch_comments();
                    m_fragment.curr_fetch_comments = fetch_comments;
                    m_fragment.curr_fetch_comments.m_fragment = m_fragment;
                    m_fragment.curr_fetch_comments.execute();
                }
            }
        }
    }

    private static class Edit_comment extends AsyncTask<Void, Void, Comment>{
        private Video_play m_fragment = null;
        private Exception exception;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Comment doInBackground(Void... voids) {
            Comment response = null;
            try {
                if (!isCancelled()){
                    if ( m_fragment.CURR_TOP_LEVEL_EDITED_COMMENT_ID != null && m_fragment.CURR_TOP_LEVEL_COMMENT_EDITED_TEXT != null ){
                        response = editComment();
                    } else {
                        m_fragment.hideProgress();
                    }
                } else {
                    m_fragment.hideProgress();
                }
            } catch (Exception e) {
                m_fragment.hideProgress();
                exception = e;
                cancel(true);
                return null;
            }

            return response;
        }

        private Comment editComment() throws IOException {
            Comment response = null;

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet");

            Comment comment = new Comment();
            comment.set("id", m_fragment.CURR_TOP_LEVEL_EDITED_COMMENT_ID);
            CommentSnippet snippet = new CommentSnippet();
            snippet.set("textOriginal", m_fragment.CURR_TOP_LEVEL_COMMENT_EDITED_TEXT);

            comment.setSnippet(snippet);
            YouTube.Comments.Update commentsUpdateRequest = m_fragment.mYoutube_comment.comments().update(parameters.get("part").toString(), comment);

            if (!isCancelled()){
                response = commentsUpdateRequest.execute();
            } else {
                m_fragment.hideProgress();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Comment comment) {
            super.onPostExecute(comment);
            m_fragment.hideProgress();
            if (comment != null){
                m_fragment.commentEditingDone(comment);
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            m_fragment.hideProgress();
            if (m_fragment.swipeRefreshLayout.isRefreshing()){
                m_fragment.swipeRefreshLayout.setRefreshing(false);
            }
            if (exception != null){
                if (exception instanceof UserRecoverableAuthIOException){
                    m_fragment.COMMENT_FETCH_PERMISSION_FLAG = true;
                    m_fragment.startActivityForResult( ((UserRecoverableAuthIOException) exception).getIntent(), REQUEST_AUTHORIZATION );
                }
            }
        }
    }

    private static class Delete_comments extends AsyncTask<Void, Void, Void>{
        private Video_play m_fragment = null;
        private Exception exception = null;
        private String commId;
        private int position;

        Delete_comments(String commId, int pos){
            this.commId = commId;
            this.position = pos;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (commId != null && !commId.equals("")){
                try {
                    if (!isCancelled()){
                        deleteComment();
                    } else {
                        m_fragment.hideProgress();
                    }
                } catch (Exception e) {
                    m_fragment.hideProgress();
                    exception = e;
                    cancel(true);
                    return null;
                }
            }
            return null;
        }

        private void deleteComment() throws IOException {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("id", commId);

            YouTube.Comments.Delete commentsDeleteRequest = m_fragment.mYoutube_comment.comments().delete(parameters.get("id").toString());
            if (!isCancelled()){
                commentsDeleteRequest.execute();
            } else {
                m_fragment.hideProgress();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            m_fragment.hideProgress();
            if (m_fragment.commentrecyclerView != null){
                m_fragment.delete_Item_comment_recycle(position);
                Toast.makeText(m_fragment, "Your Comment is Deleted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(m_fragment, "Illigal Operation!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            m_fragment.hideProgress();
            if (m_fragment.swipeRefreshLayout.isRefreshing()){
                m_fragment.swipeRefreshLayout.setRefreshing(false);
            }
            if (exception != null){
                if (exception instanceof UserRecoverableAuthIOException){
                    m_fragment.COMMENT_FETCH_PERMISSION_FLAG = true;
                    m_fragment.startActivityForResult( ((UserRecoverableAuthIOException) exception).getIntent(), REQUEST_AUTHORIZATION );
                }
            }
        }
    }

    private static class Mark_Spam_comments extends AsyncTask<Void, Void, Void>{
        private Video_play m_fragment = null;
        private Exception exception = null;
        private String commId;
        private int position;

        Mark_Spam_comments(String commId, int pos){
            this.commId = commId;
            this.position = pos;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (commId != null && !commId.equals("")){
                try {
                    if (!isCancelled()){
                        markasSpam();
                    }
                } catch (Exception e) {
                    exception = e;
                    cancel(true);
                    return null;
                }
            }
            return null;
        }

        private void markasSpam() throws IOException {
            YouTube.Comments.MarkAsSpam commentSpamRequest = m_fragment.mYoutube_comment.comments().markAsSpam(commId);
            if (!isCancelled()){
                commentSpamRequest.execute();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (m_fragment.commentrecyclerView != null){
                m_fragment.delete_Item_comment_recycle(position);
                Toast.makeText(m_fragment, "Comment is marked as spam", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(m_fragment, "Illigal Operation!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (m_fragment.swipeRefreshLayout.isRefreshing()){
                m_fragment.swipeRefreshLayout.setRefreshing(false);
            }
            if (exception != null){
                if (exception instanceof UserRecoverableAuthIOException){
                    m_fragment.COMMENT_FETCH_PERMISSION_FLAG = true;
                    m_fragment.startActivityForResult( ((UserRecoverableAuthIOException) exception).getIntent(), REQUEST_AUTHORIZATION );
                }
            }
        }
    }

}
