package biz.agvcorp.banichitra.notifications;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.database_classes.AppDatabase;
import biz.agvcorp.banichitra.database_classes.App_notification;
import biz.agvcorp.banichitra.recycler_adapters.Recycler_adapter_news;

public class News extends Fragment {
    private static final String TAG = "News";

    private AppDatabase appDatabase_news;

    private View mView;

    private Toolbar toolbar;
    private GetAllNotifications currAllNotifications = null;

    private RecyclerView recyclerView;
    private Recycler_adapter_news recycler_adapter_news;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private final static int NUM_COLUM = 3;
    private ArrayList<String> msgId = new ArrayList<>();
    private ArrayList<String> msgTitle = new ArrayList<>();
    private ArrayList<String> msgBody = new ArrayList<>();
    private ArrayList<String> msgImageUrl = new ArrayList<>();
    private ArrayList<String> msgPriority = new ArrayList<>();
    private ArrayList<String> msgVideoId = new ArrayList<>();

    private RelativeLayout mProgressbarContainer;
    private TextView mNonewsmsg;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appDatabase_news = AppDatabase.getAppDatabse(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.news_activity, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar = view.findViewById(R.id.news_toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(true);

        mProgressbarContainer = view.findViewById(R.id.news_progressbar_container);
        mNonewsmsg = view.findViewById(R.id.nonewsrecievemsg);

        GetAllNotifications getAllNotifications = new GetAllNotifications(News.this);
        currAllNotifications = getAllNotifications;
        currAllNotifications.execute();

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUM, LinearLayoutManager.VERTICAL);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (currAllNotifications != null){
            currAllNotifications.cancel(true);
        }
        AppDatabase.destroyInstance();
        hideNonews();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    public News() {

    }

    private void create_recycler_news(){
        recyclerView = mView.findViewById(R.id.news_recycler);
        recycler_adapter_news = new Recycler_adapter_news(msgId, msgTitle, msgBody, msgImageUrl, msgPriority, msgVideoId, mView.getContext());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(recycler_adapter_news);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void showNonews(){
        if (mNonewsmsg.getVisibility() == View.GONE){
            mNonewsmsg.setVisibility(View.VISIBLE);
        }
    }
    private void hideNonews(){
        if (mNonewsmsg.getVisibility() == View.VISIBLE){
            mNonewsmsg.setVisibility(View.GONE);
        }
    }

    private static class GetAllNotifications extends AsyncTask<Void, Void, List<App_notification>>{
        private News mContext = null;

        public GetAllNotifications(News mContext) {
            this.mContext = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mContext != null && !isCancelled()){
                mContext.mProgressbarContainer.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected List<App_notification> doInBackground(Void... voids) {
            List<App_notification> noti_list = null;

            if (!isCancelled()){
                noti_list = mContext.appDatabase_news.appDatabaseobject().readNotification();
            } else {
                mContext.mProgressbarContainer.setVisibility(View.GONE);
                return null;
            }

            return noti_list;
        }

        @Override
        protected void onPostExecute(List<App_notification> app_notifications) {
            super.onPostExecute(app_notifications);
            mContext.mProgressbarContainer.setVisibility(View.GONE);
            if (app_notifications.size() == 0){
                mContext.showNonews();
            } else {
                mContext.hideNonews();
            }

            if (app_notifications != null){
                mContext.hideNonews();
                for (int i = 0; i < app_notifications.size(); i++) {
                    String nid = app_notifications.get(i).getNotificationid();
                    String ntitle = app_notifications.get(i).getNotificationTitle();
                    String nbody = app_notifications.get(i).getNotificationBody();
                    String nimage = app_notifications.get(i).getNotificationImageUrl();
                    String nprio = app_notifications.get(i).getNotificationPriority();
                    String nvid = app_notifications.get(i).getVideoId();

                    if (nid != null){
                        mContext.msgId.add(nid);
                    } else {
                        mContext.msgId.add("");
                    }

                    if (ntitle != null){
                        mContext.msgTitle.add(ntitle);
                    } else {
                        mContext.msgTitle.add("");
                    }

                    if (nbody != null){
                        mContext.msgBody.add(nbody);
                    } else {
                        mContext.msgBody.add("");
                    }

                    if (nimage != null){
                        mContext.msgImageUrl.add(nimage);
                    } else {
                        mContext.msgImageUrl.add("");
                    }

                    if (nprio != null){
                        mContext.msgPriority.add(nprio);
                    } else {
                        mContext.msgPriority.add("");
                    }

                    if (nvid != null){
                        mContext.msgVideoId.add(nvid);
                    } else {
                        mContext.msgVideoId.add("");
                    }
                }
                mContext.create_recycler_news();
            } else {
                mContext.showNonews();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mContext.mProgressbarContainer.setVisibility(View.GONE);
            mContext.showNonews();
        }
    }
}
