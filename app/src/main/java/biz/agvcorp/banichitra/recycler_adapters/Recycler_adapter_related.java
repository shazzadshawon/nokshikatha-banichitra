package biz.agvcorp.banichitra.recycler_adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.elyeproj.loaderviewlibrary.LoaderImageView;
import com.elyeproj.loaderviewlibrary.LoaderTextView;

import java.util.ArrayList;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.Video_play;

public class Recycler_adapter_related extends RecyclerView.Adapter<Recycler_adapter_related.ViewHolder> {

    private static final String TAG = "Recycler_ad_related";

    private ArrayList<String> mVidieos = new ArrayList<>();
    private ArrayList<String> mThumblains = new ArrayList<>();
    private ArrayList<String> mTitle = new ArrayList<>();
    private ArrayList<String> mChannelName = new ArrayList<>();
    private ArrayList<String> mDesc = new ArrayList<>();
    private Context mContext;
    public String layoutFlag;

    public Recycler_adapter_related(String layoutFlag, Context mContext, ArrayList<String> mVidios, ArrayList<String> mThumblains, ArrayList<String> mTitle, ArrayList<String> mChannelName, ArrayList<String> mDesc) {
        clearAll();
        this.layoutFlag = layoutFlag;
        this.mVidieos = mVidios;
        this.mThumblains = mThumblains;
        this.mTitle = mTitle;
        this.mChannelName = mChannelName;
        this.mDesc = mDesc;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (layoutFlag){
            case "list":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_related, parent, false);
                break;
            case "grid":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_related_grid, parent, false);
                break;
            case "gridHorizontal":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclable_item_related_grid_horizontal, parent, false);
                break;
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    int lastPosition = -1;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final int holder_postion = position;
        Glide.with(mContext)
                .asBitmap()
                .load(mThumblains.get(position))
                .into(holder.thumbnail);
        holder.videoTitle.setText(mTitle.get(position));
        holder.channelName.setText(mChannelName.get(position));
        holder.videoDesc.setText(mDesc.get(position));

        Log.d(TAG, "onBindViewHolderRelated: "+mTitle.get(position));

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go_to_video_page(holder_postion);
            }
        });

        holder.recycle_item_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go_to_video_page(holder_postion);
            }
        });

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    @Override
    public int getItemCount() {
        return mThumblains.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LoaderImageView thumbnail;
        LoaderTextView videoTitle;
        LoaderTextView channelName;
        LoaderTextView videoDesc;
        RelativeLayout recycle_item_container;

        public ViewHolder(View itemView) {
            super(itemView);

            thumbnail = itemView.findViewById(R.id.imageView);
            videoTitle = itemView.findViewById(R.id.relatedvideoTitle);
            channelName = itemView.findViewById(R.id.relatedvideoChannelName);
            videoDesc = itemView.findViewById(R.id.relatedVideoDesc);
            recycle_item_container = itemView.findViewById(R.id.recycle_item_container);
        }
    }

    private void go_to_video_page(int position){
        String vid = mVidieos.get(position);
        Log.d(TAG, "go_to_video_page: VideoId" + vid);

        Intent videoIntent = new Intent(mContext, Video_play.class);
        videoIntent.putExtra(mContext.getString(R.string.passvid), vid);
        videoIntent.putExtra(mContext.getString(R.string.passTitle), mTitle.get(position));
        mContext.startActivity(videoIntent);


        Toast.makeText(mContext, "Clicked on the video named: "+mTitle.get(position), Toast.LENGTH_SHORT).show();
    }

    private void clearAll(){
        this.mVidieos.clear();
        this.mThumblains.clear();
        this.mTitle.clear();
        this.mChannelName.clear();
        this.mDesc.clear();
    }

}
