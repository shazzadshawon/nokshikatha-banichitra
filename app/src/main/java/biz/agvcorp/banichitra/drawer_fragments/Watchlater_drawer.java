package biz.agvcorp.banichitra.drawer_fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.recycler_adapters.Recycler_adapter;
import biz.agvcorp.banichitra.adapters.Watchlater_thumbnail_adapter;
import biz.agvcorp.banichitra.adapters.Watchlater_videoDescription_adapter;
import biz.agvcorp.banichitra.adapters.Watchlater_videoDuration_adapter;
import biz.agvcorp.banichitra.adapters.Watchlater_videotitle_adapter;
import biz.agvcorp.banichitra.controllers.SwipeController;
import biz.agvcorp.banichitra.controllers.SwipeControllerActions;

public class Watchlater_drawer extends Fragment implements ViewPagerEx.OnPageChangeListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "Watchlater_drawer";

    private View mView;
    private OnFragmentInteractionListener mListener;

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private Recycler_adapter recycler_adapter;
    private FrameLayout mMother;
    private SwipeRefreshLayout swipeRefreshLayout;

    private Watchlater_thumbnail_adapter thumb_adapter;
    private Watchlater_videotitle_adapter videoTitle_adapter;
    private Watchlater_videoDescription_adapter videoDescription_adapter;
    private Watchlater_videoDuration_adapter videoDuration_adapter;

    private ArrayList<String> mVideoId = new ArrayList<>();
    private ArrayList<String> mThumbnail = new ArrayList<>();
    private ArrayList<String> mTitle = new ArrayList<>();
    private ArrayList<String> mDescription = new ArrayList<>();
    private ArrayList<String> mDuration = new ArrayList<>();

    private TextView mNoResult;
    private ProgressDialog historyProgress;

    private LinearLayoutManager layoutManager_list;
    private GridLayoutManager layoutManager_grid;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private final static int NUM_COLUM = 2;

    String curr_removed_vid, curr_removed_thumbnail, curr_removed_title, curr_removed_description, curr_removed_duration;

    SwipeController swipeController = null;

    SharedPreferences sharedPreferences, sharedPreferences_settings;

    private String YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY, YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY, LAYOUT_SETTING_PREV, LAYOUT_SETTING_CURR;

    public Watchlater_drawer() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.drawer_watchlater, container, false);
        swipeRefreshLayout = mView.findViewById(R.id.swiperefreshWatchLater);
        swipeRefreshLayout.setOnRefreshListener(this);

        sharedPreferences = getContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        sharedPreferences_settings = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getActivity()));

        get_previous_settings();
        settings_equalizer();

        thumb_adapter = new Watchlater_thumbnail_adapter(TAG);
        videoTitle_adapter = new Watchlater_videotitle_adapter(TAG);
        videoDescription_adapter = new Watchlater_videoDescription_adapter(TAG);
        videoDuration_adapter = new Watchlater_videoDuration_adapter(TAG);

        historyProgress = new ProgressDialog(getContext());

        showProgress(getString(R.string.fetchingWatchLater));
        FetchWatchLaterData();
        return mView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMother = view.findViewById(R.id.wl_mother);
        mNoResult = view.findViewById(R.id.wl_no_result);

        if (LAYOUT_SETTING_CURR.equals("list")){
            layoutManager_list = new LinearLayoutManager(mView.getContext());
        } else if (LAYOUT_SETTING_CURR.equals("grid")){
            layoutManager_grid = new GridLayoutManager(mView.getContext(), 2);
        } else if (LAYOUT_SETTING_CURR.equals("staggered")){
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUM, LinearLayoutManager.VERTICAL);
        } else if (LAYOUT_SETTING_CURR.equals("onelineList")){
            layoutManager_list = new LinearLayoutManager(mView.getContext());
        }

        toolbar = view.findViewById(R.id.sceondery_toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        get_current_settings();

        /*COMPARING YOUTUBE THUMBNAIL QUALITY SETTINGS*/
        if (YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY != null && YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY != null){
            if (!YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY.equals(YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY)){
                Toast.makeText(getContext(), "This settings is not supported in History", Toast.LENGTH_LONG).show();
            }
        }

        /*COMPARING LAYOUT CHANGE SETTTINGS*/
        if (LAYOUT_SETTING_CURR != null && LAYOUT_SETTING_PREV != null){
            if (!LAYOUT_SETTING_CURR.equals(LAYOUT_SETTING_PREV)){
                makeAppRestartDialog();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try{
            mListener = (OnFragmentInteractionListener) getActivity();
        } catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mNoResult.setVisibility(View.GONE);
        if (historyProgress.isShowing()){
            historyProgress.dismiss();
        }
    }

    @Override
    public void onRefresh() {
        updateUI();
    }













    private void showProgress(String msg){
        if (historyProgress.isShowing()){
            historyProgress.dismiss();
        }
        historyProgress.setMessage(msg);
        historyProgress.show();
    }
    private void hideProgress(){
        if (historyProgress.isShowing()){
            historyProgress.dismiss();
        }
        historyProgress.setMessage("");
        historyProgress.hide();
    }

    private void FetchWatchLaterData(){
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String uri = getString(R.string.host_url)+"/"+getString(R.string.fetch_watchLater_uri)+"/"+getRowId();

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    //gather_data_recycle_comment_php(response.getJSONArray("data"));
                    Log.d(TAG, "onResponse: "+response);
                    if (response.getBoolean("status")){
                        if ( response.get("data") != null){
                            JSONArray arrayData = response.getJSONArray("data");
                            if (arrayData.length() <= 0){
                                hideProgress();
                                mMother.setBackgroundResource(R.color.colorPrimary);
                                mNoResult.setVisibility(View.VISIBLE);
                                mNoResult.setText("You have no record saved on Watch Later playlist");
                            } else {
                                new ProcessWatchLaterData( response.getJSONArray("data") ).execute();
                            }
                        } else {
                            Toast.makeText(getContext(), getString(R.string.noComment), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.noComment), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(getRequest);
        stopSwipeRefreshLoader();
    }

    private void stopSwipeRefreshLoader() {
        if (swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public int getRowId(){
        return sharedPreferences.getInt(getString(R.string.rowid), 0);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void clearList(ArrayList list){
        list.clear();
    }

    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return BitmapFactory.decodeResource(context.getResources(), drawableId);
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }

    private void get_curr_removed_items(int position){
        curr_removed_vid = mVideoId.get(position);
        curr_removed_thumbnail = mThumbnail.get(position);
        curr_removed_title = mTitle.get(position);
        curr_removed_description = mDescription.get(position);
        curr_removed_duration = mDuration.get(position);
    }

    private Snackbar getSnackbar(String message, int durationCode){
        return Snackbar.make(Objects.requireNonNull(getView()), message, durationCode);
    }

    private void initRecyclerView(){
        recyclerView = mView.findViewById(R.id.recyclerView);
        recycler_adapter = new Recycler_adapter(LAYOUT_SETTING_CURR, mView.getContext(), mVideoId, mThumbnail, mTitle, mDescription, mDuration);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(recycler_adapter);

        Bitmap bitmap = getBitmap(getContext(), R.drawable.ic_rubbishbin);
        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            //on click on the left button after swiping
            public void onLeftClicked(int position) {
                get_curr_removed_items(position);
                recycler_adapter.removeItem(position);

            }
            //on click on the right button after swiping
            @Override
            public void onRightClicked(int position) {
                get_curr_removed_items(position);
                recycler_adapter.removeItem(position);
                Snackbar snackbar = getSnackbar(getString(R.string.swipedeleteMsg), Snackbar.LENGTH_LONG).setAction("UNDO", new UndoListener(position, Watchlater_drawer.this));
                snackbar.addCallback(new Snackbar.Callback(){
                    @Override
                    public void onShown(Snackbar sb) {
                        super.onShown(sb);
                    }

                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        //super.onDismissed(transientBottomBar, event);
                        if (event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT){
                            remove_from_watchlater();
                        } else if (event == Snackbar.Callback.DISMISS_EVENT_ACTION){
                            Toast.makeText(getContext(), getString(R.string.swipeDeleteToastRestoreMsg), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                snackbar.show();
            }
        }, bitmap);

        /*LAYOUT MANAGER IS THE SWITCH TO SWITCH BETWEEN LINEAR LAYOUT MANAGER AND GRID LAYOUT MANAGER*/
        switch (LAYOUT_SETTING_CURR){
            case "list":
                recyclerView.setLayoutManager(layoutManager_list);
                break;
            case "grid":
                recyclerView.setLayoutManager(layoutManager_grid);
                break;
            case "staggered":
                recyclerView.setLayoutManager(staggeredGridLayoutManager);
                break;
            case "onelineList":
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
                itemTouchHelper.attachToRecyclerView(recyclerView);
                recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                    @Override
                    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                        swipeController.onDraw(c);
                    }
                });

                recyclerView.setLayoutManager(layoutManager_list);
                break;
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void updateUI(){
        FetchWatchLaterData();
    }

    private void get_current_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            LAYOUT_SETTING_CURR = sharedPreferences_settings.getString(getString(R.string.recycler_layout_key), "onelineList");
        } else {
            YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            LAYOUT_SETTING_CURR = "onelineList";
        }
    }

    private void get_previous_settings(){
        if (sharedPreferences_settings != null){
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = sharedPreferences_settings.getString(getString(R.string.youtube_thumbnail_res_key), getString(R.string.youtubeThumbnailDefaultRes));
            LAYOUT_SETTING_PREV = sharedPreferences_settings.getString(getString(R.string.recycler_layout_key), "onelineList");
        } else {
            YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY = getString(R.string.youtubeThumbnailDefaultRes);
            LAYOUT_SETTING_PREV = "onelineList";
        }
    }

    private void settings_equalizer(){
        YOUTUBE_VIDEO_THUMBNAIL_CURR_QUALITY = YOUTUBE_VIDEO_THUMBNAIL_PREV_QUALITY;
        LAYOUT_SETTING_CURR = LAYOUT_SETTING_PREV;
    }

    private void makeAppRestartDialog(){
        AlertDialog.Builder nonCancelableDialog = new AlertDialog.Builder(getContext());
        //nonCancelableDialog.setCancelable(false);
        nonCancelableDialog.setTitle(getString(R.string.restartAppdialogTitle));
        nonCancelableDialog.setMessage(getString(R.string.restartAppdialogMessage));
        nonCancelableDialog.setPositiveButton("Restart Application", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Restart the app
                Intent restartIntent = Objects.requireNonNull(getContext()).getPackageManager().getLaunchIntentForPackage( getContext().getPackageName() );
                assert restartIntent != null;
                restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(restartIntent);
            }
        });
        nonCancelableDialog.setNegativeButton("Restart Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Dissmiss the dialog
            }
        });

        AlertDialog dialog = nonCancelableDialog.create();
        dialog.show();
    }

    private void remove_from_watchlater(){
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String uri = getString(R.string.host_url) + "/" + getString(R.string.watchLater_remove_token) + "/" + curr_removed_vid + "/" + getRowId();
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        Toast.makeText(getContext(), getString(R.string.swipeDeleteToastMsg), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), getString(R.string.swipeDeleteFailMsg), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), getString(R.string.swipeDeleteFailMsg), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.swipeDeleteFailMsg), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(getRequest);
    }












    private class ProcessWatchLaterData extends AsyncTask<Void, Void, Void> {
        private JSONArray localArray = new JSONArray();

        private ArrayList<String> videoId = new ArrayList<>();
        private ArrayList<String> thumbnail = new ArrayList<>();
        private ArrayList<String> title = new ArrayList<>();
        private ArrayList<String> description = new ArrayList<>();
        private ArrayList<String> duration = new ArrayList<>();

        Exception mException;

        ProcessWatchLaterData(JSONArray array){
            clearList(mThumbnail);
            clearList(mTitle);
            clearList(mDescription);
            clearList(mDuration);

            this.localArray = array;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                process(this.localArray);
            } catch (JSONException e) {
                mException = e;
                e.printStackTrace();
            }
            return null;
        }

        private void process(JSONArray array) throws JSONException {
            if (array != null && array.length() > 0){
                for (int i = 0; i < array.length(); i++) {
                    JSONObject historyVideo = array.getJSONObject(i);

                    this.videoId.add(historyVideo.getString("video_id"));
                    this.thumbnail.add(historyVideo.getString("thumbnail"));
                    this.title.add(historyVideo.getString("title"));
                    if (historyVideo.isNull("description")){
                        this.description.add( " " );
                    } else {
                        this.description.add(historyVideo.getString("description"));
                    }

                    if (historyVideo.isNull("duration")){
                        this.duration.add(" ");
                    } else {
                        String dur = historyVideo.getString("duration");
                        String[] durs = dur.split("T");
                        String dur_PT = durs[1];
                        this.duration.add(dur_PT);
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgress();
            if (localArray != null){
                if (mException == null){

                    mVideoId = this.videoId;
                    mThumbnail = this.thumbnail;
                    mTitle = this.title;
                    mDescription = this.description;
                    mDuration = this.duration;

                    initRecyclerView();
                } else {
                    /*Comment View Exception Happened. Handle*/
                }
            } else {
                /*No Comment Data Recieved*/
            }
        }
    }

    private static class UndoListener implements View.OnClickListener{
        private int position;
        private Watchlater_drawer mFragment = null;

        UndoListener(int pos, Watchlater_drawer concent){
            this.position = pos;
            this.mFragment = concent;
        }

        @Override
        public void onClick(View view) {
            mFragment.recycler_adapter.restoreItem(position, mFragment.curr_removed_vid, mFragment.curr_removed_thumbnail, mFragment.curr_removed_title, mFragment.curr_removed_description, mFragment.curr_removed_duration);
        }
    }
}
