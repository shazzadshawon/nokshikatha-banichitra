package biz.agvcorp.banichitra.notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import biz.agvcorp.banichitra.R;
import biz.agvcorp.banichitra.asset_library.Singleton;


public class FcmInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FcmInstanceIDService";

    @Override
    public void onTokenRefresh() {

        String recentToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG, "onTokenRefresh: "+recentToken);

        SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.FCM_TOKEN), recentToken);
        editor.commit();

        /*Will be Enabled After the Proper API is Created*/
        //sendTokenToServer(recentToken);
    }

    private void sendTokenToServer(String recentToken) {
        String url_fcmToken = getString(R.string.host_url)+"/"+getString(R.string.fcmTokenuri);

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("fcmToken",recentToken);

        JsonObjectRequest tokenRequest = new JsonObjectRequest(Request.Method.POST, url_fcmToken, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Singleton.getmInstance(FcmInstanceIDService.this).addtoRequestQueue(tokenRequest);
    }
}