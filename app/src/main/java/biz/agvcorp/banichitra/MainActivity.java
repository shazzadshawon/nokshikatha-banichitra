package biz.agvcorp.banichitra;

/**
 * Created By - Shazzadur Rahaman on 8/29/2018.
 * Company - Asian Global Ventures BD Limited
 * Company Email - shazzadur.r@agvcorp.com
 * Personal Email - shazzadurrahaman@gmail.com
 */

import android.accounts.Account;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.elyeproj.loaderviewlibrary.LoaderImageView;
import com.facebook.AccessToken;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import biz.agvcorp.banichitra.adapters.SectionPageAdapter;
import biz.agvcorp.banichitra.asset_library.FetchAddEventListener;
import biz.agvcorp.banichitra.asset_library.Fetch_ad_list;
import biz.agvcorp.banichitra.asset_library.ProgressBarAnimation;
import biz.agvcorp.banichitra.drawer_fragments.AboutBanichitra_drawer;
import biz.agvcorp.banichitra.drawer_fragments.History_drawer;
import biz.agvcorp.banichitra.drawer_fragments.Home_page_drawer;
import biz.agvcorp.banichitra.drawer_fragments.Likedvideo_drawer;
import biz.agvcorp.banichitra.drawer_fragments.Profilepage_drawer;
import biz.agvcorp.banichitra.drawer_fragments.Settings_drawer;
import biz.agvcorp.banichitra.drawer_fragments.Watchlater_drawer;
import biz.agvcorp.banichitra.notifications.News;
import biz.agvcorp.banichitra.tab_fragments.Tab_allupload;
import biz.agvcorp.banichitra.tab_fragments.Tab_favourite;
import biz.agvcorp.banichitra.tab_fragments.Tab_newuploads;
import biz.agvcorp.banichitra.tab_fragments.Tab_recentupload;
import biz.agvcorp.banichitra.tab_fragments.Tab_topviewed;
import biz.agvcorp.banichitra.tab_fragments.Tab_trending;
import de.hdodenhof.circleimageview.CircleImageView;
import me.leolin.shortcutbadger.ShortcutBadger;
import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        Home_page_drawer.OnFragmentInteractionListener,
        AboutBanichitra_drawer.OnFragmentInteractionListener,
        Settings_drawer.OnFragmentInteractionListener,
        History_drawer.OnFragmentInteractionListener,
        Watchlater_drawer.OnFragmentInteractionListener,
        Likedvideo_drawer.OnFragmentInteractionListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{

    private static final String TAG = "MainActivity";
    private DrawerLayout activity_main;

    final int RC_API_CHECK = 100;
    public static final int REQUEST_AUTHORIZATION = 1001;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    public static GoogleApiClient googleApiClient;
    public static GoogleSignInOptions gso;
    public static Account mAccount;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private String profilepicurl, displayname, googleid, useremail;

    private NavigationView navigationView;
    private CircleImageView circularProfileImage_nav;
    private TextView username_nav;
    private TextView emailaddress_nav;
    public LinearLayout nav_header;

    private SectionPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;

    private TabLayout tabLayout;
    private SectionPageAdapter adapter;

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;

    private ProgressDialog mProgress;
    private SharedPreferences sharedPreferences, sharedPreferences_count;
    private SharedPreferences.Editor editor, editor_count;

    private ArrayList<JSONObject> ads;
    private Integer CURRENT_AD_INDEX = 0;

    private LinearLayout mBottomSheet;
    private BottomSheetBehavior mBottomSheetBehavior;
    private LoaderImageView mAddImage;
    private HtmlTextView mAddvertisementDescription;
    private GifImageView mAddGif;
    private ImageView mAddCloseBtn;
    private TextView mAddPlaceHolder;
    private ProgressBar mAddProgressBar;

    //private LoginButton mFacebook_login_btn;
    //private CallbackManager mFacebook_callback_manager;

    public static boolean isAppRunning, isAlreadyLoggedIn = false, doubleBackToExitPressedOnce = false;

    private static boolean active = false;

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*ADMOB TEST*/
        MobileAds.initialize(this, getString(R.string.googleAdMobAppId));
        mAdView = findViewById(R.id.adView);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interestitialAdUnitId));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
//        AdRequest adRequest = new AdRequest.Builder().build();
//        AdRequest adRequest = new AdRequest.Builder()
//                .build();
//        mAdView.loadAd(adRequest);



        /*FACEBOOK LOGIN PROCESS*/
        AccessToken fb_accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = fb_accessToken != null && !fb_accessToken.isExpired();

        //mFacebook_login_btn = findViewById(R.id.facebook_login_button);
        //mFacebook_callback_manager = CallbackManager.Factory.create();
        /*mFacebook_login_btn.registerCallback(mFacebook_callback_manager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(MainActivity.this, "Logged In With Facebook", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(MainActivity.this, "User Canceled login process with Facebook", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MainActivity.this, "Facebook Login Error", Toast.LENGTH_SHORT).show();
            }
        });*/

        //printHashKey(getApplicationContext());
        /*Facebook is now auto initialized*/

        if (getIntent().getBooleanExtra("LOGOUT", false)){
            finish();
        }

        PreferenceManager.setDefaultValues(this, R.xml.app_prefferences, false);

        isAppRunning = true;

        sharedPreferences = getBaseContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        sharedPreferences_count = PreferenceManager.getDefaultSharedPreferences(this);
        editor_count = sharedPreferences_count.edit();

        mProgress = new ProgressDialog(this);

        activity_main = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);
        circularProfileImage_nav = hView.findViewById(R.id.profile_image);
        username_nav = hView.findViewById(R.id.profile_name);
        emailaddress_nav = hView.findViewById(R.id.profile_email);
        nav_header = hView.findViewById(R.id.nav_header);

        mBottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(mBottomSheet);
        mAddImage = mBottomSheet.findViewById(R.id.addvertisement_image);
        mAddGif = mBottomSheet.findViewById(R.id.addvertisement_gif);
        mAddCloseBtn = mBottomSheet.findViewById(R.id.add_close_btn);
        mAddPlaceHolder = mBottomSheet.findViewById(R.id.addPlaceHolder);
        mAddProgressBar = mBottomSheet.findViewById(R.id.addStreamProgressBar);
        mAddvertisementDescription = (HtmlTextView) mBottomSheet.findViewById(R.id.addvertisement_description);

        //creating a new section page adapter
        adapter = new SectionPageAdapter(getSupportFragmentManager());
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.drawer_contnet_main, new Home_page_drawer()).commit();

        //TABs
        mSectionsPageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(4);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        /*custome menu toggoler icon*/
//        toggle.setDrawerIndicatorEnabled(false);
//        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                drawer.openDrawer(GravityCompat.START);
//            }
//        });
//        toggle.setHomeAsUpIndicator(R.drawable.ic_menucustom2);
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.nav_quit).setVisible(true);

        if (isNetworkAvailable()){
            /*CHECK FOR USER USING GOOGLE LOGIN OR VIEWING WITHOUT LOGIN*/


            if (getLoginStatus()){
                /*This is for Google Silent Login*/
                menu.findItem(R.id.nav_History).setEnabled(true);
                menu.findItem(R.id.nav_watch_later).setEnabled(true);
                menu.findItem(R.id.nav_liked_videos).setEnabled(true);
                menu.findItem(R.id.nav_new_uploads).setVisible(true);
                menu.findItem(R.id.nav_trending).setVisible(true);
                menu.findItem(R.id.nav_login).setVisible(false);

                gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestServerAuthCode(getString(R.string.PeopleApiClientId))
                        .requestEmail()
                        .requestProfile()
                        .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.PLUS_ME))
                        .build();

                googleApiClient = new GoogleApiClient.Builder(this)
                        .enableAutoManage(this, this)
                        .addOnConnectionFailedListener(this)
                        .addConnectionCallbacks(this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();

                firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user != null){
                            isAlreadyLoggedIn = true;
                            setuserData_firebase(user);
                            setupViewPager(mViewPager);
                            //initLogin();
                        } else {
                            //logoutFromGoogle();
                            goLogInScreen();
                        }
                    }
                };
                firebaseAuth.addAuthStateListener(firebaseAuthListener);
            } else {
                //Logged in without Google
                gso = null;
                googleApiClient = null;
                firebaseAuth = null;

                menu.findItem(R.id.nav_History).setEnabled(false);
                menu.findItem(R.id.nav_watch_later).setEnabled(false);
                menu.findItem(R.id.nav_liked_videos).setEnabled(false);

                menu.findItem(R.id.nav_new_uploads).setVisible(false);
                menu.findItem(R.id.nav_trending).setVisible(false);

                menu.findItem(R.id.nav_login).setVisible(true);

                invalidateOptionsMenu();
                setupViewPager(mViewPager);
            }
        } else {
            makeNoInternetAlert();
            getSnackbar( getString(R.string.noInternetMsg), Snackbar.LENGTH_INDEFINITE ).setAction(R.string.netconnect, new NetConnectListener()).show();
        }

        mAddCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {

                }
            }
        });

        mAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject current_ad = ads.get(CURRENT_AD_INDEX);
                try {
                    String ad_url = current_ad.getString("url");
                    goToBrowser(ad_url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;

        /*INITIATE THE ADDVERTISEMENT */
        if (isNetworkAvailable()){
            initiateAdds();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        resetNotificationCount();
        int badgeCount = getNotificationCount();
        ShortcutBadger.applyCount(MainActivity.this, badgeCount);

        //tabLayout.removeAllTabs();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce){
                super.onBackPressed();
                return;
            }

            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);

        if (getLoginStatus()){
            menu.findItem(R.id.action_profile).setEnabled(true);
            menu.findItem(R.id.action_signout).setVisible(true);
            menu.findItem(R.id.action_signin).setVisible(false);
        } else {
            menu.findItem(R.id.action_profile).setEnabled(false);
            menu.findItem(R.id.action_signout).setVisible(false);
            menu.findItem(R.id.action_signin).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        final FragmentManager fragmentManager = getSupportFragmentManager();
        final Fragment profile_fragment = new Profilepage_drawer();
        final Fragment settings_fragment = new Settings_drawer();
        //final Fragment settings_prefference_fragment = new SettingsFragment();

        switch (id){
            case R.id.action_settings:
                checkTabSanity();
                /*fragmentManager
                        .beginTransaction()
                        .replace(R.id.drawer_contnet_main, settings_fragment)
                        .addToBackStack(TAG)
                        .commit();
                tabLayout.setTabMode(TabLayout.MODE_FIXED);*/

                Intent settingIntent = new Intent(this, SettingsActivity.class);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                startActivity(settingIntent);

//                fragmentManager
//                        .beginTransaction()
//                        .replace(R.id.drawer_contnet_main, settings_prefference_fragment)
//                        .addToBackStack(TAG)
//                        .commit();
//                tabLayout.setTabMode(TabLayout.MODE_FIXED);

                break;

            case R.id.action_profile:
                checkTabSanity();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.drawer_contnet_main, profile_fragment)
                        .addToBackStack(TAG)
                        .commit();
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                break;

            case R.id.action_refresh:
                updateUI();
                break;

            case R.id.action_signout:
                showProgress(getString(R.string.pregress_msg_logout));
                logOut();
                break;

            case R.id.action_signin:
                goLogInScreen();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    public void clearBackStackInclusive(String tag) {
        //getSupportFragmentManager().popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        final int id = item.getItemId();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        final Fragment home_fragment = new Home_page_drawer();
        final Fragment history_fragment = new History_drawer();
        final Fragment watchlater_fragment = new Watchlater_drawer();
        final Fragment likedvideos_fragment = new Likedvideo_drawer();
        final Fragment about_fragment = new AboutBanichitra_drawer();
        final Fragment news_fragment = new News();


        new Handler().post(new Runnable() {
            @Override
            public void run() {
                switch (id){
                    case R.id.nav_home:
                        checkTabSanity();
                        mViewPager.setCurrentItem(0);
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                        //mViewPager.destroyDrawingCache();
                        break;

                    case R.id.nav_new_uploads:
                        checkTabSanity();
                        adapter.addFragment(new Tab_newuploads(), "New");
                        mViewPager.setCurrentItem( adapter.getPositionbyName("New") );
                        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                        adapter.notifyDataSetChanged();
                        //mViewPager.destroyDrawingCache();
                        break;

                    case R.id.nav_trending:
                        checkTabSanity();
                        adapter.addFragment(new Tab_trending(), "Trending");
                        mViewPager.setCurrentItem( adapter.getPositionbyName("Trending") );
                        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                        adapter.notifyDataSetChanged();
                        //mViewPager.destroyDrawingCache();
                        break;

                    case R.id.nav_about_banicitra:
                        checkTabSanity();
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.drawer_contnet_main, about_fragment)
                                .addToBackStack(TAG)
                                .commit();
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                        //mViewPager.destroyDrawingCache();
                        break;

                    case R.id.nav_History:
                        checkTabSanity();
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.drawer_contnet_main, history_fragment)
                                .addToBackStack(TAG)
                                .commit();
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                        break;

                    case R.id.nav_watch_later:
                        checkTabSanity();
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.drawer_contnet_main, watchlater_fragment)
                                .addToBackStack(TAG)
                                .commit();
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                        break;

                    case R.id.nav_liked_videos:
                        checkTabSanity();
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.drawer_contnet_main, likedvideos_fragment)
                                .addToBackStack(TAG)
                                .commit();
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                        break;

//                    case R.id.nav_Profile:
//                        checkTabSanity();
//                        fragmentManager
//                                .beginTransaction()
//                                .replace(R.id.drawer_contnet_main, profile_fragment)
//                                .addToBackStack(TAG)
//                                .commit();
//                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
//                        break;
//
//                    case R.id.nav_logout:
//                        Intent androidIntent = new Intent(Intent.ACTION_MAIN);
//                        androidIntent.addCategory(Intent.CATEGORY_HOME);
//                        androidIntent.setFlags(androidIntent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(androidIntent);
//                        moveTaskToBack(true);
//                        break;

                    case R.id.nav_news:
                        checkTabSanity();
                        fragmentManager
                                .beginTransaction()
                                .replace(R.id.drawer_contnet_main, news_fragment)
                                .addToBackStack(TAG)
                                .commit();
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                        break;

                    case R.id.nav_login:
                        goLogInScreen();
                        break;

                    case R.id.nav_quit:
                        MainActivity.super.onBackPressed();
                        break;

                    case R.id.nav_restart:
                        restartApp();
                        break;
                }
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //mFacebook_callback_manager.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK){

                } else {

                }
            case REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK){

                } else {

                }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
        Glide.with(getApplicationContext()).pauseRequests();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("connection", "msg: " + connectionResult.getErrorMessage());
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        editor.putString(getString(R.string.lastActivity), getClass().getName());
        editor.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
        if (mProgress.isShowing()){
            mProgress.dismiss();
        }
        if (firebaseAuthListener != null){
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }

        editor.putString(getString(R.string.lastActivity), getClass().getName());
        editor.commit();
    }






















    /*
    * ALL METHODS ..........................................................................................
    * */
//    private void handleSignInResult(GoogleSignInResult result) {
//        hideProgress();
//        int current_retry = 0;
//
//        if (result.isSuccess()) {
//            //GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
//            GoogleSignInAccount account = result.getSignInAccount();
//            if (account != null){
//                mAccount = account.getAccount();
//
//                /*INITIATE THE TABS*/
//                setupViewPager(mViewPager);
//
//                //setupViewPager(mViewPager);
//                //tabLayout.setupWithViewPager(mViewPager);
//
////                credential = GoogleAccountCredential.usingOAuth2(MainActivity.this, Collections.singleton(YOUTUBE_SCOPE));
////                credential.setSelectedAccount(mAccount);
////
////                transport = AndroidHttp.newCompatibleTransport();
////                jasonfactory = JacksonFactory.getDefaultInstance();
////                mYoutube = new com.google.api.services.youtube.YouTube.Builder(transport, jasonfactory, credential)
////                        .setApplicationName("Banichitra")
////                        .build();
//
//                //new MakeYoutubeRequests().execute();
//                //new Fetch_channel_info().execute();
//                //new PeopleAsync().execute(account.getServerAuthCode());
//            } else {
//                Log.d(TAG, "handleSignInResult: account "+account);
//                getSnackbar( getString(R.string.googlesignInErrMsg), Snackbar.LENGTH_INDEFINITE ).setAction(R.string.googlesigninretry, new LoginretryListener()).show();
//            }
//
//        } else {
//            Log.d(TAG, "handleSignInResult: result : "+result);
//            getSnackbar( getString(R.string.googlesignInErrMsg), Snackbar.LENGTH_INDEFINITE ).setAction(R.string.googlesigninretry, new LoginretryListener()).show();
//        }
//    }

//    private void initLogin(){
//        if (isNetworkAvailable()){
//            if (getLoginStatus()){
//                googleApiClient.connect();
//                OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
//                if (opr.isDone()){
//                    GoogleSignInResult result = opr.get();
//                    handleSignInResult(result);
//                } else {
//                    opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                        @Override
//                        public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
//                            handleSignInResult(googleSignInResult);
//                        }
//                    });
//                }
//            } else {
//                //Logged in without Google
//
//            }
//        } else {
//            hideProgress();
//
//            makeNoInternetAlert();
//            getSnackbar( getString(R.string.noInternetMsg), Snackbar.LENGTH_INDEFINITE ).setAction(R.string.netconnect, new NetConnectListener()).show();
//        }
//    }

    private void goToBrowser(String uri){
        if (uri != null){
            Uri ad_uri = Uri.parse(uri);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, ad_uri);

            PackageManager packageManager = getPackageManager();
            List<ResolveInfo> activities = packageManager.queryIntentActivities(browserIntent, PackageManager.MATCH_DEFAULT_ONLY);
            boolean isIntentSafe = activities.size() > 0;

            if (isIntentSafe){
                String intentTitle = getString(R.string.browserChooserText);
                Intent chooser = Intent.createChooser(browserIntent, intentTitle);
                startActivity(chooser);
            } else {
                //THERE IS NOT ANY APP INSTALLED IN YOUR PHONE TO OPEN THIS LINK
                Toast.makeText(this, getString(R.string.noBrowserText), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initiateAdds(){
        Fetch_ad_list fetch_ad_list = new Fetch_ad_list(
                this,
                new FetchAddEventListener<ArrayList<JSONObject>>() {
                    @Override
                    public void onFetchingAddSuccess(ArrayList<JSONObject> object) {
                        if (active){
                            ads = object;
                            ad_perser(0);
                        }
                    }
                    @Override
                    public void onFetchingAddFailure(Exception e) {
                        Toast.makeText(MainActivity.this, getString(R.string.add_fetchFailed)+" Retrying . . .", Toast.LENGTH_SHORT).show();
                        initiateAdds();
                    }
                    @Override
                    public void onFetchingStart() { }
                    @Override
                    public void onNoAd(){
                        Toast.makeText(MainActivity.this, "No Ads Found on Server", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        fetch_ad_list.execute();
    }

    private void loadAdd(JSONObject ad, final int index){
        CURRENT_AD_INDEX = index;
        try {
            int ad_id = ad.getInt("ad_id");
            //String ad_title = ad.getString("title");
            //String ad_org_name = ad.getString("org_name");
            final int ad_preview_time = Integer.parseInt(ad.getString("preview_time"));
            String ad_url = ad.getString("url");
            String ad_image_type = ad.getString("imagetype");
            String ad_description = ad.getString("description");

            if (active){
                String add_image_url = getString(R.string.host_url_add_image)+ad.getString("image");
                int resId = getResources().getIdentifier(add_image_url, "drawable", "android.demo");

                if (checkActivityAlive()){
                    if (ad_image_type != null && !ad_image_type.equals("")){

                        showAddPlaceholder();
                        showAddImage();
                        if (!MainActivity.this.isFinishing()){
                            Glide.with(getApplicationContext())
                                    .load(add_image_url)
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            hideAddPlaceholder();
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            hideAddPlaceholder();
                                            showAddCloseBtn();
                                            ad_timer(ad_preview_time, index);
                                            return false;
                                        }
                                    })
                                    .transition(GenericTransitionOptions.with(R.anim.zoomin))
                                    .into(mAddImage);
                            mAddvertisementDescription.setHtml(ad_description, new HtmlHttpImageGetter(mAddvertisementDescription, null, true));
                        }

//                        if (ad_image_type.equals("1")){
//                            //showAddPlaceholder();
//                            showAddImage();
//                            if (!MainActivity.this.isFinishing()){
//                                Glide.with(getApplicationContext())
//                                        .load(add_image_url)
//                                        .listener(new RequestListener<Drawable>() {
//                                            @Override
//                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                                //hideAddPlaceholder();
//                                                return false;
//                                            }
//
//                                            @Override
//                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                                //hideAddPlaceholder();
//                                                showAddCloseBtn();
//                                                ad_timer(ad_preview_time, index);
//                                                return false;
//                                            }
//                                        })
//                                        .transition(GenericTransitionOptions.with(R.anim.zoomin))
//                                        .into(mAddImage);
//                            }
//                        } else if (ad_image_type.equals("0")){
//                            showAddGif();
//                            Glide.with(getApplicationContext())
//                                    .load(add_image_url)
//                                    .listener(new RequestListener<Drawable>() {
//                                        @Override
//                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                            return false;
//                                        }
//
//                                        @Override
//                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                            showAddCloseBtn();
//                                            ad_timer(ad_preview_time, index);
//                                            return false;
//                                        }
//                                    })
//                                    .into(mAddGif);
//                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ad_timer(int time, final int i){
        final int timeLeft = time * 1000;
        if (active){
            showAddProgress();

            //mAddProgressBar.setProgress(progressInt[0]);

            new CountDownTimer(timeLeft, 1000){
                int progressInt = 0;
                @Override
                public void onTick(long l) {
                    progressInt++;
                    int progress = progressInt*100/(timeLeft/1000);

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        mAddProgressBar.setProgress(progress,true);
//                    } else {
//                        mAddProgressBar.setProgress(progress);
//                    }
                    ProgressBarAnimation anim = new ProgressBarAnimation(mAddProgressBar, timeLeft);
                    anim.setProgress(progress);
                    //Toast.makeText(MainActivity.this,"seconds remaining: " + timeLeft / 1000, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFinish() {
                    if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN){
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }

                    int new_index = i + 1;
                    //anim.resetProgress();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        mAddProgressBar.setProgress(0, true);
                    } else {
                        mAddProgressBar.setProgress(0);
                    }
                    hideAddProgressBar();

                    ad_perser(new_index);
                }
            }.start();
        }
    }

    private void ad_perser(int index){
        if (checkActivityAlive()){
            if ( index >= ads.size()){
                ad_perser(0);
            } else {
                JSONObject ad = ads.get(index);
                loadAdd(ad, index);
            }
        }
    }

    private void setuserData_firebase(FirebaseUser user){
        displayname = user.getDisplayName();
        googleid = user.getUid();
        profilepicurl = String.valueOf(user.getPhotoUrl());
        useremail = user.getEmail();

        //Log.d(TAG, "setuserData_firebase: " + getCoverPhoto());
        Glide.with(this).load( user.getPhotoUrl() ).into(circularProfileImage_nav);
        Glide.with(this).load(getCoverPhoto()).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                nav_header.setBackground(resource);
            }
        });
        username_nav.setText(displayname);
        emailaddress_nav.setText(useremail);

        setImageUrl(profilepicurl);
        setUserName(displayname);
        setUserEmail(useremail);
    }

    private void setupViewPager(ViewPager viewPager){

        //adding fragments to the adapter. Just like adding items to the list adapter
        adapter.addFragment(new Tab_allupload(), "All");     //0
        adapter.addFragment(new Tab_recentupload(), "Recent");      //1
        adapter.addFragment(new Tab_favourite(), "Fav");      //2
        adapter.addFragment(new Tab_topviewed() , "Top");      //3

        viewPager.setAdapter(adapter);
    }

    private void checkTabSanity(){
        if (adapter.getCount() > 4){
            int extra = adapter.getCount() - 4;

            for (int i = 1; i <= extra; i++) {
                adapter.removePage(3+i);
            }
        }
    }

    private void goLogInScreen() {
        Intent intent = new Intent(this, Login.class);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(this) != null;
    }

    public void logOut() {
        if (isSignedIn()){
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    if (status.isSuccess()) {
                        Toast.makeText(MainActivity.this, "Successfully Logged out from Google", Toast.LENGTH_SHORT).show();
                        logoutFromPHP();
                    } else {
                        getSnackbar(getString(R.string.not_close_session), Snackbar.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void logoutFromPHP(){
        String uri = getString(R.string.host_url)+"/"+getString(R.string.app_logout)+"/"+ getRowId();
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        getSnackbar(getString(R.string.phpLogoutSuccess), Snackbar.LENGTH_SHORT).show();
                        logoutFromFirebase();
                    } else {
                        hideProgress();
                        getSnackbar(getString(R.string.phpLogoutError), Snackbar.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    hideProgress();
                    e.printStackTrace();
                    getSnackbar(String.valueOf(e), Snackbar.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress();
            }
        });
        queue.add(postRequest);
    }

    private void logoutFromFirebase(){
        firebaseAuth.getInstance().signOut();
        hideProgress();
        goLogInScreen();
    }

    private Snackbar getSnackbar(String message, int durationCode){
        return Snackbar.make(activity_main, message, durationCode);
    }

    private void showProgress(String msg){
        mProgress.setMessage(msg);
        mProgress.show();
    }
    private void hideProgress(){
        mProgress.setMessage("");
        mProgress.hide();
    }



    private String getFcmToken(){
        return sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
    }
    public void setRowId(int id){
        editor.putInt( getString(R.string.rowid),  id);
        editor.commit();
    }
    private void setImageUrl(String url){
        editor.putString(getString(R.string.setProfileImageurl), url);
        editor.commit();
    }
    public void setUserName(String name){
        editor.putString(getString(R.string.setUserNaem), name);
        editor.commit();
    }
    public void setUserEmail(String email){
        editor.putString(getString(R.string.setUserEmail), email);
        editor.commit();
    }
    public int getRowId(){
        return sharedPreferences.getInt(getString(R.string.rowid), 0);
    }
    private Boolean getLoginStatus(){
        return sharedPreferences.getBoolean(getString(R.string.loginStatus), true);
    }

    private Boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return ( netInfo != null && netInfo.isConnected() );
    }

    private void makeNoInternetAlert(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.noInternetMsg));
        alertDialog.setMessage( getString(R.string.noInternetDetailMsg) );
        alertDialog.setButton("OK", new NetConnectListener());
        alertDialog.show();
    }

    private String getCoverPhoto(){
        return sharedPreferences.getString(getString(R.string.coverphoto), "");
    }

    private Boolean checkActivityAlive(){
        AppCompatActivity activity = MainActivity.this;
        if (activity != null){
            return true;
        } return false;
    }

    private void showAddImage(){
        mAddGif.setVisibility(View.GONE);
        mAddImage.setVisibility(View.VISIBLE);
    }
    private void showAddGif(){
        mAddGif.setVisibility(View.VISIBLE);
        mAddImage.setVisibility(View.GONE);
    }
    private void showAddCloseBtn(){
        mAddCloseBtn.setVisibility(View.VISIBLE);
    }
    private void hideAddCloseBtn(){
        mAddCloseBtn.setVisibility(View.GONE);
    }
    private void showAddPlaceholder(){
        mAddPlaceHolder.setVisibility(View.VISIBLE);
    }
    private void hideAddPlaceholder(){
        mAddPlaceHolder.setVisibility(View.GONE);
    }
    private void showAddProgress(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mAddProgressBar.setProgress(0, true);
        } else {
            mAddProgressBar.setProgress(0);
        }
        mAddProgressBar.setVisibility(View.VISIBLE);
    }
    private void hideAddProgressBar(){
        mAddProgressBar.setVisibility(View.GONE);
    }

    private void updateUI(){
        if (isNetworkAvailable()){
            tabLayout.setupWithViewPager(mViewPager);
        } else {
            makeNoInternetAlert();
            getSnackbar( getString(R.string.noInternetMsg), Snackbar.LENGTH_INDEFINITE ).setAction(R.string.netconnect, new NetConnectListener()).show();
        }
    }

    private void restartApp(){
        Intent restartIntent = getPackageManager().getLaunchIntentForPackage( getPackageName() );
        assert restartIntent != null;
        restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(restartIntent);
    }

    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

    public void updateNotificationCount(int count){
        int p = sharedPreferences_count.getInt(getString(R.string.iconBadgeCount), 0);
        int f = p+count;
        editor_count.putInt(getString(R.string.iconBadgeCount), f);
        editor_count.commit();
    }
    public int getNotificationCount(){
        return sharedPreferences_count.getInt(getString(R.string.iconBadgeCount), 0);
    }
    public void resetNotificationCount(){
        editor_count.putInt(getString(R.string.iconBadgeCount), 0);
        editor_count.commit();
    }




















    /*
    * ALL CLASSES ......................................................................................
    * */
    private class NetConnectListener implements View.OnClickListener, DialogInterface.OnClickListener{

        private void startWifi(){
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        }

        @Override
        public void onClick(View view) {
            startWifi();
        }

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            startWifi();
        }
    }

//    private class LoginretryListener implements View.OnClickListener{
//
//        @Override
//        public void onClick(View view) {
//            initLogin();
//        }
//    }
}
