package biz.agvcorp.banichitra.asset_library;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import biz.agvcorp.banichitra.R;

public class Fetch_ad_list extends AsyncTask<Void, Void, Void> {
    private FetchAddEventListener<ArrayList<JSONObject>> mCallback;
    private Context mContext;
    public Exception mException;

    public Fetch_ad_list(Context context, FetchAddEventListener callback){
        this.mContext = context;
        this.mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mCallback != null){
            mCallback.onFetchingStart();
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        //final ArrayList<JSONObject> ad_array = new ArrayList<>();

        String uri = mContext.getString(R.string.host_url)+"/"+mContext.getString(R.string.add_image_url_array);
        RequestQueue queue = Volley.newRequestQueue(mContext);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {



            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")){
                        ArrayList<JSONObject> ad_array = new ArrayList<>();
                        for (int i = 0; i < response.getJSONArray("ads").length(); i++) {
                            ad_array.add( response.getJSONArray("ads").getJSONObject(i) );
                        }

                        if (mCallback != null){
                            if (mException == null){
                                mCallback.onFetchingAddSuccess(ad_array);
                            } else {
                                mCallback.onFetchingAddFailure(mException);
                            }
                        }

                    } else {
                        mCallback.onNoAd();
                    }
                } catch (JSONException e) {
                    mException = e;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(getRequest);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
