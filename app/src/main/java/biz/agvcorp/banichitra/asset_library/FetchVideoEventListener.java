package biz.agvcorp.banichitra.asset_library;

public interface FetchVideoEventListener<T> {
    public void onSuccess(T object);
    public void onFailure(Exception e);
    public void onFetchingStart();
}
